`timescale 1ns / 1ps

module LedCheck(
	input [11:0] L,
	input [11:0] R,
	output reg LED0,
	output reg LED1,
	output reg LED2,
	output reg LED3,
	
	input clk
    );


initial begin
	LED0 <= 1;
	LED1 <= 0;
	LED2 <= 0;
	LED3 <= 0;
end

always @(posedge clk) begin
	
	if(L > 20 || R > 20) begin
		LED1 <= 1;
	end
	else begin
		LED1 <= 0;
	end
	
	if(L > 228 || R > 228 ) begin
		LED2 <= 1;
	end
	else begin
		LED2 <= 0;
	end
	if(L > 2001 || R > 2001) begin
		LED3 <= 1;
	end
	else begin
		LED3 <= 0;
	end
end



endmodule
