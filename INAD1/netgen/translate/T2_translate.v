////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.20131013
//  \   \         Application: netgen
//  /   /         Filename: T2_translate.v
// /___/   /\     Timestamp: Wed Jan 15 18:50:32 2020
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -intstyle ise -insert_glbl true -w -dir netgen/translate -ofmt verilog -sim T2.ngd T2_translate.v 
// Device	: 3s500efg320-5
// Input file	: T2.ngd
// Output file	: C:\Users\Kristers\Desktop\INAD1\netgen\translate\T2_translate.v
// # of Modules	: 1
// Design Name	: T2
// Xilinx        : E:\Xilinx\14.7\ISE_DS\ISE\
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module T2 (
  MISO_L, MISO_R, CS, L0, L1, L2, L3, SCLK
);
  input MISO_L;
  input MISO_R;
  output CS;
  output L0;
  output L1;
  output L2;
  output L3;
  output SCLK;
  wire N0;
  wire N1;
  wire SCLK_OBUF_8;
  wire MISO_L_IBUF_11;
  wire MISO_R_IBUF_12;
  X_ZERO   XST_GND (
    .O(N0)
  );
  X_ONE   XST_VCC (
    .O(N1)
  );
  X_INV   \XLXI_3/SCLK_INV_0  (
    .I(SCLK_OBUF_8),
    .O(SCLK_OBUF_8)
  );
  X_OPAD #(
    .LOC ( "B4" ))
  CS_10 (
    .PAD(CS)
  );
  X_OPAD #(
    .LOC ( "F12" ))
  L0_11 (
    .PAD(L0)
  );
  X_OPAD #(
    .LOC ( "E12" ))
  L1_12 (
    .PAD(L1)
  );
  X_OPAD #(
    .LOC ( "E11" ))
  L2_13 (
    .PAD(L2)
  );
  X_OPAD #(
    .LOC ( "F11" ))
  L3_14 (
    .PAD(L3)
  );
  X_OPAD #(
    .LOC ( "C5" ))
  SCLK_15 (
    .PAD(SCLK)
  );
  X_IPAD #(
    .LOC ( "D5" ))
  MISO_L_16 (
    .PAD(MISO_L)
  );
  X_BUF   MISO_L_IBUF (
    .I(MISO_L),
    .O(MISO_L_IBUF_11)
  );
  X_IPAD #(
    .LOC ( "A4" ))
  MISO_R_18 (
    .PAD(MISO_R)
  );
  X_BUF   MISO_R_IBUF (
    .I(MISO_R),
    .O(MISO_R_IBUF_12)
  );
  X_OBUF   CS_OBUF (
    .I(N0),
    .O(CS)
  );
  X_OBUF   L0_OBUF (
    .I(N1),
    .O(L0)
  );
  X_OBUF   L1_OBUF (
    .I(N0),
    .O(L1)
  );
  X_OBUF   L2_OBUF (
    .I(N0),
    .O(L2)
  );
  X_OBUF   L3_OBUF (
    .I(N0),
    .O(L3)
  );
  X_OBUF   SCLK_OBUF (
    .I(SCLK_OBUF_8),
    .O(SCLK)
  );
endmodule


`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

