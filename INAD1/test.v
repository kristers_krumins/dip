`timescale 1ns / 1ps

module test;

	// Inputs
	reg clk;

	// Outputs
	wire [15:0] filter;
	wire [11:0] addr;
	wire setF;

	// Instantiate the Unit Under Test (UUT)
	FiltGen uut (
		.clk(clk), 
		.filter(filter), 
		.addr(addr), 
		.setF(setF)
	);
	
	always #20 clk = ~clk;

	initial begin
		// Initialize Inputs
		clk = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

