`timescale 1ns / 1ps

module multTemp( NumbIn, NumbIn2,clk, NumbOut);
	 
	parameter WIDTH = 8;
	parameter MANTISSA = 4;
	
	 input [WIDTH + MANTISSA -1:0] NumbIn;
	 input [WIDTH + MANTISSA -1:0] NumbIn2;
	 input clk;
	 output reg [WIDTH + MANTISSA -1:0] NumbOut;
	
	 reg [((WIDTH + MANTISSA)*2)-1:0] tmp;

	
	always@(posedge clk) begin 
		tmp = NumbIn * NumbIn2;
		NumbOut <= tmp[WIDTH + (MANTISSA *2) -1:MANTISSA];
	end

endmodule 

