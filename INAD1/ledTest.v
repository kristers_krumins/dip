`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:39:57 01/16/2020
// Design Name:   LedCheck
// Module Name:   /home/raimonds/Desktop/repos/dip/INAD1/ledTest.v
// Project Name:  INAD1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: LedCheck
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module ledTest;

	// Inputs
	reg [11:0] L;
	reg clk;

	// Outputs
	wire LED0;
	wire LED1;
	wire LED2;
	wire LED3;

	// Instantiate the Unit Under Test (UUT)
	LedCheck uut (
		.L(L), 
		.LED0(LED0), 
		.LED1(LED1), 
		.LED2(LED2), 
		.LED3(LED3), 
		.clk(clk)
	);

	initial begin
		// Initialize Inputs
		L = 0;
		clk = 0;

		// Wait 100 ns for global reset to finish
		#100;
        L <= 100;
		  clk <= 1;
		#50
			clk <=0;
		#50
		        L <= 1000;
		  clk <= 1;
		#50
			clk <=0;
		#50
		        L <= 4000;
		  clk <= 1;
		#50
			clk <=0;
		#50
		        L <= 15;
		  clk <= 1;
		#50
			clk <=0;

		// Add stimulus here

	end
      
endmodule

