<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="BUTTON" />
        <signal name="CL">
        </signal>
        <signal name="L0" />
        <signal name="L1" />
        <signal name="L2" />
        <signal name="L3" />
        <signal name="XLXN_18" />
        <signal name="XLXN_19(11:0)" />
        <signal name="CL1" />
        <signal name="XLXN_21(15:0)" />
        <port polarity="Input" name="BUTTON" />
        <port polarity="Output" name="L0" />
        <port polarity="Output" name="L1" />
        <port polarity="Output" name="L2" />
        <port polarity="Output" name="L3" />
        <port polarity="Input" name="CL1" />
        <blockdef name="kernelBlock">
            <timestamp>2020-1-16T11:47:45</timestamp>
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="352" y="-364" height="24" />
            <line x2="416" y1="-352" y2="-352" x1="352" />
            <rect width="288" x="64" y="-384" height="448" />
        </blockdef>
        <blockdef name="DataGen">
            <timestamp>2020-1-16T9:26:4</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="LedCheck">
            <timestamp>2020-1-16T12:5:45</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-140" height="24" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="slowClock">
            <timestamp>2020-1-16T11:50:1</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <block symbolname="kernelBlock" name="XLXI_1">
            <blockpin signalname="CL" name="clk" />
            <blockpin signalname="BUTTON" name="setFilter" />
            <blockpin signalname="XLXN_19(11:0)" name="data(11:0)" />
            <blockpin name="ldata(15:0)" />
            <blockpin name="filterData(15:0)" />
            <blockpin signalname="XLXN_21(15:0)" name="value(15:0)" />
        </block>
        <block symbolname="LedCheck" name="XLXI_4">
            <blockpin signalname="CL" name="clk" />
            <blockpin signalname="XLXN_21(15:0)" name="L(15:0)" />
            <blockpin signalname="L0" name="LED0" />
            <blockpin signalname="L1" name="LED1" />
            <blockpin signalname="L2" name="LED2" />
            <blockpin signalname="L3" name="LED3" />
        </block>
        <block symbolname="DataGen" name="XLXI_8">
            <blockpin signalname="CL" name="clk" />
            <blockpin signalname="XLXN_19(11:0)" name="Numb(11:0)" />
        </block>
        <block symbolname="slowClock" name="XLXI_9">
            <blockpin signalname="CL1" name="clk" />
            <blockpin signalname="CL" name="o_clk" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="BUTTON">
            <wire x2="1296" y1="832" y2="832" x1="1280" />
            <wire x2="1312" y1="832" y2="832" x1="1296" />
        </branch>
        <iomarker fontsize="28" x="1280" y="832" name="BUTTON" orien="R180" />
        <instance x="1312" y="1056" name="XLXI_1" orien="R0">
        </instance>
        <branch name="L0">
            <wire x2="2832" y1="544" y2="544" x1="2816" />
            <wire x2="2848" y1="544" y2="544" x1="2832" />
        </branch>
        <branch name="L1">
            <wire x2="2832" y1="608" y2="608" x1="2816" />
            <wire x2="2848" y1="608" y2="608" x1="2832" />
        </branch>
        <branch name="L2">
            <wire x2="2832" y1="672" y2="672" x1="2816" />
            <wire x2="2848" y1="672" y2="672" x1="2832" />
        </branch>
        <branch name="L3">
            <wire x2="2832" y1="736" y2="736" x1="2816" />
            <wire x2="2848" y1="736" y2="736" x1="2832" />
        </branch>
        <instance x="2432" y="768" name="XLXI_4" orien="R0">
        </instance>
        <iomarker fontsize="28" x="2848" y="544" name="L0" orien="R0" />
        <iomarker fontsize="28" x="2848" y="608" name="L1" orien="R0" />
        <iomarker fontsize="28" x="2848" y="672" name="L2" orien="R0" />
        <iomarker fontsize="28" x="2848" y="736" name="L3" orien="R0" />
        <branch name="CL">
            <wire x2="752" y1="752" y2="752" x1="720" />
            <wire x2="720" y1="752" y2="1136" x1="720" />
            <wire x2="1280" y1="1136" y2="1136" x1="720" />
            <wire x2="1296" y1="1136" y2="1136" x1="1280" />
            <wire x2="1280" y1="1136" y2="1216" x1="1280" />
            <wire x2="2992" y1="1216" y2="1216" x1="1280" />
            <wire x2="864" y1="736" y2="736" x1="752" />
            <wire x2="752" y1="736" y2="752" x1="752" />
            <wire x2="1312" y1="1088" y2="1088" x1="1296" />
            <wire x2="1296" y1="1088" y2="1136" x1="1296" />
            <wire x2="1824" y1="448" y2="448" x1="1792" />
            <wire x2="2400" y1="448" y2="448" x1="1824" />
            <wire x2="2400" y1="448" y2="544" x1="2400" />
            <wire x2="2416" y1="544" y2="544" x1="2400" />
            <wire x2="2432" y1="544" y2="544" x1="2416" />
            <wire x2="2992" y1="448" y2="448" x1="2400" />
            <wire x2="2992" y1="448" y2="1216" x1="2992" />
        </branch>
        <instance x="864" y="768" name="XLXI_8" orien="R0">
        </instance>
        <branch name="XLXN_19(11:0)">
            <wire x2="1296" y1="624" y2="624" x1="784" />
            <wire x2="1296" y1="624" y2="736" x1="1296" />
            <wire x2="784" y1="624" y2="896" x1="784" />
            <wire x2="1312" y1="896" y2="896" x1="784" />
            <wire x2="1296" y1="736" y2="736" x1="1248" />
        </branch>
        <instance x="1408" y="480" name="XLXI_9" orien="R0">
        </instance>
        <branch name="CL1">
            <wire x2="1408" y1="448" y2="448" x1="1376" />
        </branch>
        <iomarker fontsize="28" x="1376" y="448" name="CL1" orien="R180" />
        <branch name="XLXN_21(15:0)">
            <wire x2="2080" y1="704" y2="704" x1="1728" />
            <wire x2="2080" y1="640" y2="704" x1="2080" />
            <wire x2="2432" y1="640" y2="640" x1="2080" />
        </branch>
    </sheet>
</drawing>