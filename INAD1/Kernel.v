`timescale 1ns / 1ps


module Kernel(dataIn, clk, dataOut, setF_clk, addr, fVal);

	parameter WIDTH = 12;
	parameter LENGTH = 10;
	parameter MANTISA = 4;
	
	//---------
	
	input [WIDTH-1:0] dataIn;
	input clk;
	input setF_clk;
	input [11:0] addr;
	input [WIDTH + MANTISA -1: 0] fVal;

	
	output reg [WIDTH-1:0] dataOut;
	 
	 //---------
	 
	reg [WIDTH + MANTISA -1:0] filters [LENGTH:0];
	wire [WIDTH + MANTISA -1:0] wires [LENGTH+1:0];

	assign wires[0] = 0;
	
	reg [WIDTH + MANTISA -1:0] tmp; //tmp for bitshift before sending out
	
	integer j;

	
	initial begin

		for ( j = 0; j <= LENGTH; j = j+1) begin
			filters[j] = 0;
		end
		
		filters[LENGTH] = 'b10000;
		filters[0] = 'b1000;
	
		
	end
	
	always@(posedge setF_clk) begin
		filters[addr] = fVal;
	end
	
	
	always@(negedge clk) begin
		dataOut = wires[LENGTH+1] >> 4;
	end
	
		generate
		genvar i;
		for(i=0; i <=LENGTH; i=i+1) begin : KernelBL
		
			kernelBlock #(.WIDTH(WIDTH), .MANTISA(MANTISA)) 
						 KB ( .data(dataIn), 
								.ldata(wires[i]),
								.filter(filters[i]),
								.clk(clk),
								.value(wires[i+1]));				
		end
	endgenerate

endmodule
