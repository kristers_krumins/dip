`timescale 1ns / 1ps

module multGen(clk, Numb1, Numb2);
	 
	parameter WIDTH = 8;
	parameter MANTISSA = 4;
	
	 output reg [WIDTH + MANTISSA -1:0] Numb1;
	 output reg [WIDTH + MANTISSA -1:0] Numb2;
	 input clk;
	
	initial begin 
		Numb1 = 0;
		Numb2 = 0;
	end
	
	always@(negedge clk)begin
		Numb1 <= Numb1 + 1;
		Numb2 <= Numb2 + 2;
	end 

endmodule 

