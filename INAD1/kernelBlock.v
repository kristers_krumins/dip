`timescale 1ns / 1ps

module kernelBlock(
		data,
		ldata,
		filter,
		clk,
		setFilter,
		value
    );
	 parameter WIDTH = 12;
	 parameter MANTISA = 4;
	
	
	input [WIDTH -1:0] data;
	input [WIDTH + MANTISA -1:0] ldata;
	//input [WIDTH + MANTISA -1:0] filterData;
	
	input clk;
	input setFilter;
	
	output reg [WIDTH + MANTISA -1:0] value;
	 
	input [WIDTH + MANTISA -1 : 0] filter;
	reg [WIDTH + MANTISA -1: 0] temp;
	reg [((WIDTH + MANTISA)*2)-1:0] tmp;
	reg [WIDTH + MANTISA -1 : 0] test;

initial begin
		value = 0;
		temp = 0;
		test = 0;
		
end
	
	always@(negedge clk) begin 
		test = ldata;
	end

always@(posedge clk) begin 
		value = test;
		temp = data << 4;
		
		tmp = filter * temp;
		value = tmp[WIDTH + (MANTISA *2) -1:MANTISA] + value;

	end
	

endmodule
