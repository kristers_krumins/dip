`timescale 1ns / 1ps

module kernBL;

	// Inputs
	reg [7:0] data;
	reg [11:0] ldata;
	reg add_clk;
	reg write_clk;
	reg set;

	wire [11:0] fl;
	wire [11:0] t;
	// Outputs
	wire [11:0] value;


	// Instantiate the Unit Under Test (UUT)
	kernelBlock uut (
		.data(data), 
		.ldata(ldata), 
		.filterData(ldata),
		.add_clk(add_clk),
		.write_clk(write_clk), 
		.setFilter(set),
		.value(value)
	);

	initial begin
		// Initialize Inputs
		data = 0;
		add_clk = 0;
		write_clk = 0;
		set = 0;
      ldata = 'b1100;

		// Wait 100 ns for global reset to finish
		#100;
		  set = 1;
		#10
		  set = 0;
		  data = 'b10;
		  add_clk = 1;
		#10
		  add_clk = 0;
		#10
		  data = 'b111;
		  add_clk = 1;
  		#10
		  add_clk = 0;


	end
      
endmodule

