/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;



int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    work_m_09186810022330879541_3161391933_init();
    work_m_14261734771079828239_1673056715_init();
    work_m_09413517182449967122_0814259009_init();
    work_m_08696398369077611122_4275449226_init();
    work_m_05896177934100398076_3696334355_init();
    work_m_14876125985842548296_4294909412_init();
    work_m_16541823861846354283_2073120511_init();


    xsi_register_tops("work_m_14876125985842548296_4294909412");
    xsi_register_tops("work_m_16541823861846354283_2073120511");


    return xsi_run_simulation(argc, argv);

}
