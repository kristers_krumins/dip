<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="SCLK" />
        <signal name="CS" />
        <signal name="L0" />
        <signal name="L1" />
        <signal name="L2" />
        <signal name="L3" />
        <signal name="XLXN_10(11:0)" />
        <signal name="XLXN_11" />
        <signal name="MISO_LP" />
        <signal name="MISO_RP" />
        <signal name="CLK_IN" />
        <signal name="XLXN_12(11:0)" />
        <port polarity="Output" name="SCLK" />
        <port polarity="Output" name="CS" />
        <port polarity="Output" name="L0" />
        <port polarity="Output" name="L1" />
        <port polarity="Output" name="L2" />
        <port polarity="Output" name="L3" />
        <port polarity="Input" name="MISO_LP" />
        <port polarity="Input" name="MISO_RP" />
        <port polarity="Input" name="CLK_IN" />
        <blockdef name="SPIAD">
            <timestamp>2020-1-16T21:7:43</timestamp>
            <line x2="0" y1="96" y2="96" x1="64" />
            <line x2="384" y1="32" y2="32" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-256" height="448" />
        </blockdef>
        <blockdef name="LedCheck">
            <timestamp>2020-1-16T20:48:43</timestamp>
            <rect width="64" x="0" y="20" height="24" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-140" height="24" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-256" height="320" />
        </blockdef>
        <block symbolname="LedCheck" name="XLXI_2">
            <blockpin signalname="XLXN_11" name="clk" />
            <blockpin signalname="XLXN_10(11:0)" name="L(11:0)" />
            <blockpin signalname="XLXN_12(11:0)" name="R(11:0)" />
            <blockpin signalname="L0" name="LED0" />
            <blockpin signalname="L1" name="LED1" />
            <blockpin signalname="L2" name="LED2" />
            <blockpin signalname="L3" name="LED3" />
        </block>
        <block symbolname="SPIAD" name="XLXI_3">
            <blockpin signalname="CLK_IN" name="clkIN" />
            <blockpin signalname="MISO_LP" name="MISO_L" />
            <blockpin signalname="MISO_RP" name="MISO_R" />
            <blockpin signalname="SCLK" name="SCLK" />
            <blockpin signalname="CS" name="CS" />
            <blockpin signalname="XLXN_11" name="CLK" />
            <blockpin signalname="XLXN_12(11:0)" name="dataOut_R(11:0)" />
            <blockpin signalname="XLXN_10(11:0)" name="dataOut_L(11:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1360" y="768" name="XLXI_2" orien="R0">
        </instance>
        <branch name="SCLK">
            <wire x2="1184" y1="528" y2="528" x1="1136" />
        </branch>
        <iomarker fontsize="28" x="1184" y="528" name="SCLK" orien="R0" />
        <branch name="CS">
            <wire x2="1184" y1="592" y2="592" x1="1136" />
        </branch>
        <iomarker fontsize="28" x="1184" y="592" name="CS" orien="R0" />
        <branch name="L0">
            <wire x2="1776" y1="544" y2="544" x1="1744" />
        </branch>
        <iomarker fontsize="28" x="1776" y="544" name="L0" orien="R0" />
        <branch name="L1">
            <wire x2="1776" y1="608" y2="608" x1="1744" />
        </branch>
        <branch name="L2">
            <wire x2="1776" y1="672" y2="672" x1="1744" />
        </branch>
        <branch name="L3">
            <wire x2="1776" y1="736" y2="736" x1="1744" />
        </branch>
        <iomarker fontsize="28" x="1776" y="608" name="L1" orien="R0" />
        <branch name="XLXN_10(11:0)">
            <wire x2="1264" y1="720" y2="720" x1="1136" />
            <wire x2="1264" y1="640" y2="720" x1="1264" />
            <wire x2="1360" y1="640" y2="640" x1="1264" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="1328" y1="784" y2="784" x1="1136" />
            <wire x2="1328" y1="544" y2="784" x1="1328" />
            <wire x2="1360" y1="544" y2="544" x1="1328" />
        </branch>
        <instance x="752" y="752" name="XLXI_3" orien="R0">
        </instance>
        <branch name="MISO_LP">
            <wire x2="736" y1="592" y2="592" x1="656" />
            <wire x2="736" y1="592" y2="624" x1="736" />
            <wire x2="752" y1="624" y2="624" x1="736" />
        </branch>
        <branch name="MISO_RP">
            <wire x2="736" y1="752" y2="752" x1="656" />
            <wire x2="752" y1="720" y2="720" x1="736" />
            <wire x2="736" y1="720" y2="752" x1="736" />
        </branch>
        <iomarker fontsize="28" x="1776" y="736" name="L3" orien="R0" />
        <iomarker fontsize="28" x="1776" y="672" name="L2" orien="R0" />
        <iomarker fontsize="28" x="656" y="592" name="MISO_LP" orien="R180" />
        <iomarker fontsize="28" x="656" y="752" name="MISO_RP" orien="R180" />
        <branch name="CLK_IN">
            <wire x2="752" y1="848" y2="848" x1="720" />
        </branch>
        <iomarker fontsize="28" x="720" y="848" name="CLK_IN" orien="R180" />
        <branch name="XLXN_12(11:0)">
            <wire x2="1344" y1="656" y2="656" x1="1136" />
            <wire x2="1344" y1="656" y2="800" x1="1344" />
            <wire x2="1360" y1="800" y2="800" x1="1344" />
        </branch>
    </sheet>
</drawing>