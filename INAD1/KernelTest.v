`timescale 1ns / 1ps


module KernelTest;

	// Inputs
	reg [7:0] dataIn;
	reg clk;

	// Outputs
	wire [7:0] dataOut;
		integer i;

	// Instantiate the Unit Under Test (UUT)
	Kernel uut (
		.dataIn(dataIn), 
		.clk(clk), 
		.dataOut(dataOut)
	);
	
	always #20 clk = ~clk;
	

	initial begin
		// Initialize Inputs
		dataIn = 0;
		clk = 0;

		// Wait 100 ns for global reset to finish
		
		for(i=0; i<=20; i= i+1) begin
			#40
			dataIn = i;
		end
		#40
			dataIn = 0;
		// Add stimulus here

	end
      
endmodule

