`timescale 1ns / 1ps


module Kernel(
	input [WIDTH-1:0] dataIn,
	input clk,
	
	output reg [WIDTH-1:0] dataOut
	
	

    );
	
	//Parameters
	parameter WIDTH = 8;
	parameter LENGTH = 10;
	parameter MANTISA = 4;
	
	reg [WIDTH + MANTISA -1:0] filters [LENGTH:0];
	wire [WIDTH + MANTISA -1:0] wires [LENGTH+1:0];

	assign wires[0] = 0;
	
	reg [WIDTH + MANTISA -1:0] tmp; //tmp for bitshift before sending out
	reg set;
	
	integer j;
	
	initial begin
		set = 0;
		
		
		for(j=0; j < LENGTH+1; j=j+1) begin
			filters[j] = 0;
		end
		
		filters[0] = 'b1000;
		filters[LENGTH] = 'b10000;
		
		#10
		
		set = 1;
		
	end
	
	always@(posedge clk) begin
		#10
		dataOut = wires[LENGTH+1] >> 4;
	end
	
		generate 
		genvar i;
		for(i=0; i <=LENGTH; i=i+1) begin 
		
			kernelBlock #(.WIDTH(WIDTH), .MANTISA(MANTISA)) 
						 KB ( .data(dataIn), 
								.ldata(wires[i]),
								.filterData(filters[i]),
								.add_clk(clk),
								.write_clk(clk), 
								.setFilter(set), 
								.value(wires[i+1]));
								
		end
	endgenerate

endmodule
