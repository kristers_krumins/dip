
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name INAD1 -dir "/home/raimonds/Desktop/repos/dip/INAD1/planAhead_run_2" -part xc3s500efg320-5
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "constr.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {SPIAD.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {LedCheck.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {T2.vf}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set_property top T2 $srcset
add_files [list {constr.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc3s500efg320-5
