`timescale 1ns / 1ps

module FiltGen(clk, filter, addr, setF);

input clk;
output reg [15:0] filter;
output reg [11:0] addr;
output reg setF;

reg [7:0] i;

initial begin
	setF = 0;
	filter = 0;
	addr = 0;
	i = 0;
end

always@(posedge clk) begin
	
	i = i + 1;
	
	if(i > 5) begin
			setF = 1;
	end


end

endmodule
