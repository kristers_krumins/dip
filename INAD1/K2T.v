`timescale 1ns / 1ps


module K2T;

	// Inputs
	wire [11:0] dataIn;
	reg clk_in;
	wire clk;
	wire L0;
	wire L1;
	wire L2;
	wire L3;

	// Outputs
	wire [11:0] dataOut;
	
	
	wire fclk;
	reg [11:0] addr;
	reg[15:0] val;

	// Instantiate the Unit Under Test (UUT)
	Kernel uut (
		.dataIn(dataIn), 
		.clk(clk_in), 
		.dataOut(dataOut),
		.setF_clk(fclk),
		.addr(addr),
		.fVal(val)
	);
	
	slowClock sc (.clk(clk_in), .o_clk(clk));
	
	DataGen dg (.clk(clk_in), .Numb(dataIn));
	
	LedCheck lc (.clk(clk_in), .L(dataOut), .LED0(L0), .LED1(L1), .LED2(L2), .LED3(L3));
	
	//FiltGen fg (.clk(clk_in), .setF(fclk));

	always begin #10 clk_in = ~clk_in; end

	initial begin
		// Initialize Inputs
		clk_in = 0;
		

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

