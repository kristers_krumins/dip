////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.20131013
//  \   \         Application: netgen
//  /   /         Filename: T2_sta.v
// /___/   /\     Timestamp: Wed Jan 15 18:58:39 2020
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -intstyle ise -sta -w T2.ncd T2_sta.v 
// Device	: 3s500efg320-5 (PRODUCTION 1.27 2013-10-13)
// Input file	: T2.ncd
// Output file	: T2_sta.v
// # of Modules	: 1
// Design Name	: T2
// Xilinx        : E:\Xilinx\14.7\ISE_DS\ISE\
//             
// Purpose:    
//     This verilog netlist is a verification model and uses library 
//     primitives which may not represent the true implementation of 
//     the device, however the netlist is functionally correct and 
//     should not be modified. This file cannot be synthesized and 
//     should only be used with supported static timing analysis tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module T2 (
  MISO_L, MISO_R, CS, L0, L1, L2, L3, SCLK
);
  input MISO_L;
  input MISO_R;
  output CS;
  output L0;
  output L1;
  output L2;
  output L3;
  output SCLK;
  wire MISO_L_IBUF_22;
  wire MISO_R_IBUF_23;
  wire \L3/O ;
  wire \CS/O ;
  wire \L2/O ;
  wire \L0/O ;
  wire \MISO_R/INBUF ;
  wire \L1/O ;
  wire \MISO_L/INBUF ;
  wire \L3/OUTPUT/OFF/O1INV_40 ;
  wire \CS/OUTPUT/OFF/O1INV_48 ;
  wire \L2/OUTPUT/OFF/O1INV_32 ;
  wire \L0/OUTPUT/OFF/O1INV_68 ;
  wire \L1/OUTPUT/OFF/O1INV_76 ;
  X_OBUF   L3_OBUF (
    .I(\L3/O ),
    .O(L3)
  );
  X_OBUF   CS_OBUF (
    .I(\CS/O ),
    .O(CS)
  );
  X_OBUF   L2_OBUF (
    .I(\L2/O ),
    .O(L2)
  );
  X_OBUF   L0_OBUF (
    .I(\L0/O ),
    .O(L0)
  );
  X_BUF   MISO_R_IBUF (
    .I(MISO_R),
    .O(\MISO_R/INBUF )
  );
  X_OBUF   L1_OBUF (
    .I(\L1/O ),
    .O(L1)
  );
  X_BUF   MISO_L_IBUF (
    .I(MISO_L),
    .O(\MISO_L/INBUF )
  );
  X_BUF   \MISO_L/IFF/IMUX  (
    .I(\MISO_L/INBUF ),
    .O(MISO_L_IBUF_22)
  );
  X_BUF   \MISO_R/IFF/IMUX  (
    .I(\MISO_R/INBUF ),
    .O(MISO_R_IBUF_23)
  );
  X_BUF   \L3/OUTPUT/OFF/OMUX  (
    .I(\L3/OUTPUT/OFF/O1INV_40 ),
    .O(\L3/O )
  );
  X_BUF   \L3/OUTPUT/OFF/O1INV  (
    .I(1'b0),
    .O(\L3/OUTPUT/OFF/O1INV_40 )
  );
  X_BUF   \CS/OUTPUT/OFF/OMUX  (
    .I(\CS/OUTPUT/OFF/O1INV_48 ),
    .O(\CS/O )
  );
  X_BUF   \CS/OUTPUT/OFF/O1INV  (
    .I(1'b0),
    .O(\CS/OUTPUT/OFF/O1INV_48 )
  );
  X_BUF   \L2/OUTPUT/OFF/OMUX  (
    .I(\L2/OUTPUT/OFF/O1INV_32 ),
    .O(\L2/O )
  );
  X_BUF   \L2/OUTPUT/OFF/O1INV  (
    .I(1'b0),
    .O(\L2/OUTPUT/OFF/O1INV_32 )
  );
  X_BUF   \L0/OUTPUT/OFF/OMUX  (
    .I(\L0/OUTPUT/OFF/O1INV_68 ),
    .O(\L0/O )
  );
  X_BUF   \L0/OUTPUT/OFF/O1INV  (
    .I(1'b1),
    .O(\L0/OUTPUT/OFF/O1INV_68 )
  );
  X_BUF   \L1/OUTPUT/OFF/OMUX  (
    .I(\L1/OUTPUT/OFF/O1INV_76 ),
    .O(\L1/O )
  );
  X_BUF   \L1/OUTPUT/OFF/O1INV  (
    .I(1'b0),
    .O(\L1/OUTPUT/OFF/O1INV_76 )
  );
endmodule

