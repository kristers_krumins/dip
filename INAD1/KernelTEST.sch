<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="CLK_IN" />
        <signal name="L0" />
        <signal name="L1" />
        <signal name="L2" />
        <signal name="L3" />
        <signal name="XLXN_10(11:0)" />
        <signal name="XLXN_11(11:0)" />
        <signal name="CL" />
        <port polarity="Output" name="L0" />
        <port polarity="Output" name="L1" />
        <port polarity="Output" name="L2" />
        <port polarity="Output" name="L3" />
        <port polarity="Input" name="CL" />
        <blockdef name="Kernel">
            <timestamp>2020-1-17T7:40:31</timestamp>
            <line x2="0" y1="32" y2="32" x1="64" />
            <rect width="64" x="0" y="84" height="24" />
            <line x2="0" y1="96" y2="96" x1="64" />
            <rect width="64" x="0" y="148" height="24" />
            <line x2="0" y1="160" y2="160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="256" x="64" y="-128" height="384" />
        </blockdef>
        <blockdef name="DataGen">
            <timestamp>2020-1-17T7:50:11</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="LedCheck">
            <timestamp>2020-1-17T7:50:26</timestamp>
            <rect width="64" x="0" y="20" height="24" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-140" height="24" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-256" height="320" />
        </blockdef>
        <blockdef name="slowClock">
            <timestamp>2020-1-17T7:50:34</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <block symbolname="LedCheck" name="XLXI_3">
            <blockpin signalname="CLK_IN" name="clk" />
            <blockpin signalname="XLXN_11(11:0)" name="L(11:0)" />
            <blockpin name="R(11:0)" />
            <blockpin signalname="L0" name="LED0" />
            <blockpin signalname="L1" name="LED1" />
            <blockpin signalname="L2" name="LED2" />
            <blockpin signalname="L3" name="LED3" />
        </block>
        <block symbolname="Kernel" name="XLXI_4">
            <blockpin signalname="CLK_IN" name="clk" />
            <blockpin name="setF_clk" />
            <blockpin signalname="XLXN_10(11:0)" name="dataIn(11:0)" />
            <blockpin name="addr(11:0)" />
            <blockpin name="fVal(15:0)" />
            <blockpin signalname="XLXN_11(11:0)" name="dataOut(11:0)" />
        </block>
        <block symbolname="DataGen" name="XLXI_5">
            <blockpin signalname="CLK_IN" name="clk" />
            <blockpin signalname="XLXN_10(11:0)" name="Numb(11:0)" />
        </block>
        <block symbolname="slowClock" name="XLXI_6">
            <blockpin signalname="CL" name="clk" />
            <blockpin signalname="CLK_IN" name="o_clk" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1696" y="960" name="XLXI_3" orien="R0">
        </instance>
        <branch name="L0">
            <wire x2="2112" y1="736" y2="736" x1="2080" />
        </branch>
        <iomarker fontsize="28" x="2112" y="736" name="L0" orien="R0" />
        <branch name="L1">
            <wire x2="2112" y1="800" y2="800" x1="2080" />
        </branch>
        <iomarker fontsize="28" x="2112" y="800" name="L1" orien="R0" />
        <branch name="L2">
            <wire x2="2112" y1="864" y2="864" x1="2080" />
        </branch>
        <iomarker fontsize="28" x="2112" y="864" name="L2" orien="R0" />
        <branch name="L3">
            <wire x2="2112" y1="928" y2="928" x1="2080" />
        </branch>
        <iomarker fontsize="28" x="2112" y="928" name="L3" orien="R0" />
        <branch name="XLXN_10(11:0)">
            <wire x2="1136" y1="976" y2="976" x1="1056" />
        </branch>
        <branch name="XLXN_11(11:0)">
            <wire x2="1680" y1="912" y2="912" x1="1520" />
            <wire x2="1696" y1="832" y2="832" x1="1680" />
            <wire x2="1680" y1="832" y2="912" x1="1680" />
        </branch>
        <instance x="672" y="1008" name="XLXI_5" orien="R0">
        </instance>
        <instance x="848" y="592" name="XLXI_6" orien="R0">
        </instance>
        <branch name="CLK_IN">
            <wire x2="640" y1="672" y2="976" x1="640" />
            <wire x2="672" y1="976" y2="976" x1="640" />
            <wire x2="1248" y1="672" y2="672" x1="640" />
            <wire x2="1664" y1="672" y2="672" x1="1248" />
            <wire x2="1664" y1="672" y2="736" x1="1664" />
            <wire x2="1696" y1="736" y2="736" x1="1664" />
            <wire x2="1248" y1="672" y2="816" x1="1248" />
            <wire x2="1056" y1="816" y2="912" x1="1056" />
            <wire x2="1136" y1="912" y2="912" x1="1056" />
            <wire x2="1248" y1="816" y2="816" x1="1056" />
            <wire x2="1248" y1="560" y2="560" x1="1232" />
            <wire x2="1248" y1="560" y2="672" x1="1248" />
        </branch>
        <instance x="1136" y="1008" name="XLXI_4" orien="R0">
        </instance>
        <branch name="CL">
            <wire x2="848" y1="560" y2="560" x1="816" />
        </branch>
        <iomarker fontsize="28" x="816" y="560" name="CL" orien="R180" />
    </sheet>
</drawing>