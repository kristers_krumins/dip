/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/raimonds/Desktop/repos/dip/INAD1/LedCheck.v";
static int ng1[] = {1, 0};
static int ng2[] = {0, 0};
static int ng3[] = {2047, 0};
static int ng4[] = {128, 0};
static int ng5[] = {1000, 0};



static void Initial_15_0(char *t0)
{
    char *t1;
    char *t2;

LAB0:    xsi_set_current_line(15, ng0);

LAB2:    xsi_set_current_line(16, ng0);
    t1 = ((char*)((ng1)));
    t2 = (t0 + 1768);
    xsi_vlogvar_wait_assign_value(t2, t1, 0, 0, 1, 0LL);
    xsi_set_current_line(17, ng0);
    t1 = ((char*)((ng2)));
    t2 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t2, t1, 0, 0, 1, 0LL);
    xsi_set_current_line(18, ng0);
    t1 = ((char*)((ng2)));
    t2 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t2, t1, 0, 0, 1, 0LL);
    xsi_set_current_line(19, ng0);
    t1 = ((char*)((ng2)));
    t2 = (t0 + 2248);
    xsi_vlogvar_wait_assign_value(t2, t1, 0, 0, 1, 0LL);

LAB1:    return;
}

static void Always_22_1(char *t0)
{
    char t6[8];
    char t10[8];
    char t24[8];
    char t28[8];
    char t36[8];
    char t68[8];
    char t83[8];
    char t87[8];
    char t101[8];
    char t105[8];
    char t113[8];
    char t145[8];
    char t153[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    char *t8;
    char *t9;
    char *t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    char *t17;
    char *t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    char *t25;
    char *t26;
    char *t27;
    char *t29;
    unsigned int t30;
    unsigned int t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    char *t35;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    char *t40;
    char *t41;
    char *t42;
    unsigned int t43;
    unsigned int t44;
    unsigned int t45;
    unsigned int t46;
    unsigned int t47;
    unsigned int t48;
    unsigned int t49;
    char *t50;
    char *t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    unsigned int t55;
    unsigned int t56;
    unsigned int t57;
    unsigned int t58;
    unsigned int t59;
    int t60;
    int t61;
    unsigned int t62;
    unsigned int t63;
    unsigned int t64;
    unsigned int t65;
    unsigned int t66;
    unsigned int t67;
    char *t69;
    unsigned int t70;
    unsigned int t71;
    unsigned int t72;
    unsigned int t73;
    unsigned int t74;
    char *t75;
    char *t76;
    unsigned int t77;
    unsigned int t78;
    unsigned int t79;
    unsigned int t80;
    char *t81;
    char *t82;
    char *t84;
    char *t85;
    char *t86;
    char *t88;
    unsigned int t89;
    unsigned int t90;
    unsigned int t91;
    unsigned int t92;
    unsigned int t93;
    char *t94;
    char *t95;
    unsigned int t96;
    unsigned int t97;
    unsigned int t98;
    char *t99;
    char *t100;
    char *t102;
    char *t103;
    char *t104;
    char *t106;
    unsigned int t107;
    unsigned int t108;
    unsigned int t109;
    unsigned int t110;
    unsigned int t111;
    char *t112;
    unsigned int t114;
    unsigned int t115;
    unsigned int t116;
    char *t117;
    char *t118;
    char *t119;
    unsigned int t120;
    unsigned int t121;
    unsigned int t122;
    unsigned int t123;
    unsigned int t124;
    unsigned int t125;
    unsigned int t126;
    char *t127;
    char *t128;
    unsigned int t129;
    unsigned int t130;
    unsigned int t131;
    unsigned int t132;
    unsigned int t133;
    unsigned int t134;
    unsigned int t135;
    unsigned int t136;
    int t137;
    int t138;
    unsigned int t139;
    unsigned int t140;
    unsigned int t141;
    unsigned int t142;
    unsigned int t143;
    unsigned int t144;
    char *t146;
    unsigned int t147;
    unsigned int t148;
    unsigned int t149;
    unsigned int t150;
    unsigned int t151;
    char *t152;
    unsigned int t154;
    unsigned int t155;
    unsigned int t156;
    char *t157;
    char *t158;
    char *t159;
    unsigned int t160;
    unsigned int t161;
    unsigned int t162;
    unsigned int t163;
    unsigned int t164;
    unsigned int t165;
    unsigned int t166;
    char *t167;
    char *t168;
    unsigned int t169;
    unsigned int t170;
    unsigned int t171;
    int t172;
    unsigned int t173;
    unsigned int t174;
    unsigned int t175;
    int t176;
    unsigned int t177;
    unsigned int t178;
    unsigned int t179;
    unsigned int t180;
    char *t181;
    unsigned int t182;
    unsigned int t183;
    unsigned int t184;
    unsigned int t185;
    unsigned int t186;
    char *t187;
    char *t188;

LAB0:    t1 = (t0 + 3408U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(22, ng0);
    t2 = (t0 + 3728);
    *((int *)t2) = 1;
    t3 = (t0 + 3440);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(22, ng0);

LAB5:    xsi_set_current_line(24, ng0);
    t4 = (t0 + 1048U);
    t5 = *((char **)t4);
    t4 = ((char*)((ng1)));
    memset(t6, 0, 8);
    t7 = (t5 + 4);
    if (*((unsigned int *)t7) != 0)
        goto LAB7;

LAB6:    t8 = (t4 + 4);
    if (*((unsigned int *)t8) != 0)
        goto LAB7;

LAB10:    if (*((unsigned int *)t5) > *((unsigned int *)t4))
        goto LAB8;

LAB9:    memset(t10, 0, 8);
    t11 = (t6 + 4);
    t12 = *((unsigned int *)t11);
    t13 = (~(t12));
    t14 = *((unsigned int *)t6);
    t15 = (t14 & t13);
    t16 = (t15 & 1U);
    if (t16 != 0)
        goto LAB11;

LAB12:    if (*((unsigned int *)t11) != 0)
        goto LAB13;

LAB14:    t18 = (t10 + 4);
    t19 = *((unsigned int *)t10);
    t20 = *((unsigned int *)t18);
    t21 = (t19 || t20);
    if (t21 > 0)
        goto LAB15;

LAB16:    memcpy(t36, t10, 8);

LAB17:    memset(t68, 0, 8);
    t69 = (t36 + 4);
    t70 = *((unsigned int *)t69);
    t71 = (~(t70));
    t72 = *((unsigned int *)t36);
    t73 = (t72 & t71);
    t74 = (t73 & 1U);
    if (t74 != 0)
        goto LAB30;

LAB31:    if (*((unsigned int *)t69) != 0)
        goto LAB32;

LAB33:    t76 = (t68 + 4);
    t77 = *((unsigned int *)t68);
    t78 = (!(t77));
    t79 = *((unsigned int *)t76);
    t80 = (t78 || t79);
    if (t80 > 0)
        goto LAB34;

LAB35:    memcpy(t153, t68, 8);

LAB36:    t181 = (t153 + 4);
    t182 = *((unsigned int *)t181);
    t183 = (~(t182));
    t184 = *((unsigned int *)t153);
    t185 = (t184 & t183);
    t186 = (t185 != 0);
    if (t186 > 0)
        goto LAB68;

LAB69:    xsi_set_current_line(27, ng0);

LAB72:    xsi_set_current_line(28, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 1, 0LL);

LAB70:    xsi_set_current_line(31, ng0);
    t2 = (t0 + 1048U);
    t3 = *((char **)t2);
    t2 = ((char*)((ng4)));
    memset(t6, 0, 8);
    t4 = (t3 + 4);
    if (*((unsigned int *)t4) != 0)
        goto LAB74;

LAB73:    t5 = (t2 + 4);
    if (*((unsigned int *)t5) != 0)
        goto LAB74;

LAB77:    if (*((unsigned int *)t3) > *((unsigned int *)t2))
        goto LAB75;

LAB76:    memset(t10, 0, 8);
    t8 = (t6 + 4);
    t12 = *((unsigned int *)t8);
    t13 = (~(t12));
    t14 = *((unsigned int *)t6);
    t15 = (t14 & t13);
    t16 = (t15 & 1U);
    if (t16 != 0)
        goto LAB78;

LAB79:    if (*((unsigned int *)t8) != 0)
        goto LAB80;

LAB81:    t11 = (t10 + 4);
    t19 = *((unsigned int *)t10);
    t20 = *((unsigned int *)t11);
    t21 = (t19 || t20);
    if (t21 > 0)
        goto LAB82;

LAB83:    memcpy(t36, t10, 8);

LAB84:    memset(t68, 0, 8);
    t50 = (t36 + 4);
    t70 = *((unsigned int *)t50);
    t71 = (~(t70));
    t72 = *((unsigned int *)t36);
    t73 = (t72 & t71);
    t74 = (t73 & 1U);
    if (t74 != 0)
        goto LAB97;

LAB98:    if (*((unsigned int *)t50) != 0)
        goto LAB99;

LAB100:    t69 = (t68 + 4);
    t77 = *((unsigned int *)t68);
    t78 = (!(t77));
    t79 = *((unsigned int *)t69);
    t80 = (t78 || t79);
    if (t80 > 0)
        goto LAB101;

LAB102:    memcpy(t153, t68, 8);

LAB103:    t167 = (t153 + 4);
    t182 = *((unsigned int *)t167);
    t183 = (~(t182));
    t184 = *((unsigned int *)t153);
    t185 = (t184 & t183);
    t186 = (t185 != 0);
    if (t186 > 0)
        goto LAB135;

LAB136:    xsi_set_current_line(34, ng0);

LAB139:    xsi_set_current_line(35, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 1, 0LL);

LAB137:    xsi_set_current_line(37, ng0);
    t2 = (t0 + 1048U);
    t3 = *((char **)t2);
    t2 = ((char*)((ng5)));
    memset(t6, 0, 8);
    t4 = (t3 + 4);
    if (*((unsigned int *)t4) != 0)
        goto LAB141;

LAB140:    t5 = (t2 + 4);
    if (*((unsigned int *)t5) != 0)
        goto LAB141;

LAB144:    if (*((unsigned int *)t3) > *((unsigned int *)t2))
        goto LAB142;

LAB143:    memset(t10, 0, 8);
    t8 = (t6 + 4);
    t12 = *((unsigned int *)t8);
    t13 = (~(t12));
    t14 = *((unsigned int *)t6);
    t15 = (t14 & t13);
    t16 = (t15 & 1U);
    if (t16 != 0)
        goto LAB145;

LAB146:    if (*((unsigned int *)t8) != 0)
        goto LAB147;

LAB148:    t11 = (t10 + 4);
    t19 = *((unsigned int *)t10);
    t20 = *((unsigned int *)t11);
    t21 = (t19 || t20);
    if (t21 > 0)
        goto LAB149;

LAB150:    memcpy(t36, t10, 8);

LAB151:    memset(t68, 0, 8);
    t50 = (t36 + 4);
    t70 = *((unsigned int *)t50);
    t71 = (~(t70));
    t72 = *((unsigned int *)t36);
    t73 = (t72 & t71);
    t74 = (t73 & 1U);
    if (t74 != 0)
        goto LAB164;

LAB165:    if (*((unsigned int *)t50) != 0)
        goto LAB166;

LAB167:    t69 = (t68 + 4);
    t77 = *((unsigned int *)t68);
    t78 = (!(t77));
    t79 = *((unsigned int *)t69);
    t80 = (t78 || t79);
    if (t80 > 0)
        goto LAB168;

LAB169:    memcpy(t153, t68, 8);

LAB170:    t167 = (t153 + 4);
    t182 = *((unsigned int *)t167);
    t183 = (~(t182));
    t184 = *((unsigned int *)t153);
    t185 = (t184 & t183);
    t186 = (t185 != 0);
    if (t186 > 0)
        goto LAB202;

LAB203:    xsi_set_current_line(40, ng0);

LAB206:    xsi_set_current_line(41, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 2248);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 1, 0LL);

LAB204:    goto LAB2;

LAB7:    t9 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t9) = 1;
    goto LAB9;

LAB8:    *((unsigned int *)t6) = 1;
    goto LAB9;

LAB11:    *((unsigned int *)t10) = 1;
    goto LAB14;

LAB13:    t17 = (t10 + 4);
    *((unsigned int *)t10) = 1;
    *((unsigned int *)t17) = 1;
    goto LAB14;

LAB15:    t22 = (t0 + 1048U);
    t23 = *((char **)t22);
    t22 = ((char*)((ng3)));
    memset(t24, 0, 8);
    t25 = (t23 + 4);
    if (*((unsigned int *)t25) != 0)
        goto LAB19;

LAB18:    t26 = (t22 + 4);
    if (*((unsigned int *)t26) != 0)
        goto LAB19;

LAB22:    if (*((unsigned int *)t23) < *((unsigned int *)t22))
        goto LAB20;

LAB21:    memset(t28, 0, 8);
    t29 = (t24 + 4);
    t30 = *((unsigned int *)t29);
    t31 = (~(t30));
    t32 = *((unsigned int *)t24);
    t33 = (t32 & t31);
    t34 = (t33 & 1U);
    if (t34 != 0)
        goto LAB23;

LAB24:    if (*((unsigned int *)t29) != 0)
        goto LAB25;

LAB26:    t37 = *((unsigned int *)t10);
    t38 = *((unsigned int *)t28);
    t39 = (t37 & t38);
    *((unsigned int *)t36) = t39;
    t40 = (t10 + 4);
    t41 = (t28 + 4);
    t42 = (t36 + 4);
    t43 = *((unsigned int *)t40);
    t44 = *((unsigned int *)t41);
    t45 = (t43 | t44);
    *((unsigned int *)t42) = t45;
    t46 = *((unsigned int *)t42);
    t47 = (t46 != 0);
    if (t47 == 1)
        goto LAB27;

LAB28:
LAB29:    goto LAB17;

LAB19:    t27 = (t24 + 4);
    *((unsigned int *)t24) = 1;
    *((unsigned int *)t27) = 1;
    goto LAB21;

LAB20:    *((unsigned int *)t24) = 1;
    goto LAB21;

LAB23:    *((unsigned int *)t28) = 1;
    goto LAB26;

LAB25:    t35 = (t28 + 4);
    *((unsigned int *)t28) = 1;
    *((unsigned int *)t35) = 1;
    goto LAB26;

LAB27:    t48 = *((unsigned int *)t36);
    t49 = *((unsigned int *)t42);
    *((unsigned int *)t36) = (t48 | t49);
    t50 = (t10 + 4);
    t51 = (t28 + 4);
    t52 = *((unsigned int *)t10);
    t53 = (~(t52));
    t54 = *((unsigned int *)t50);
    t55 = (~(t54));
    t56 = *((unsigned int *)t28);
    t57 = (~(t56));
    t58 = *((unsigned int *)t51);
    t59 = (~(t58));
    t60 = (t53 & t55);
    t61 = (t57 & t59);
    t62 = (~(t60));
    t63 = (~(t61));
    t64 = *((unsigned int *)t42);
    *((unsigned int *)t42) = (t64 & t62);
    t65 = *((unsigned int *)t42);
    *((unsigned int *)t42) = (t65 & t63);
    t66 = *((unsigned int *)t36);
    *((unsigned int *)t36) = (t66 & t62);
    t67 = *((unsigned int *)t36);
    *((unsigned int *)t36) = (t67 & t63);
    goto LAB29;

LAB30:    *((unsigned int *)t68) = 1;
    goto LAB33;

LAB32:    t75 = (t68 + 4);
    *((unsigned int *)t68) = 1;
    *((unsigned int *)t75) = 1;
    goto LAB33;

LAB34:    t81 = (t0 + 1208U);
    t82 = *((char **)t81);
    t81 = ((char*)((ng1)));
    memset(t83, 0, 8);
    t84 = (t82 + 4);
    if (*((unsigned int *)t84) != 0)
        goto LAB38;

LAB37:    t85 = (t81 + 4);
    if (*((unsigned int *)t85) != 0)
        goto LAB38;

LAB41:    if (*((unsigned int *)t82) > *((unsigned int *)t81))
        goto LAB39;

LAB40:    memset(t87, 0, 8);
    t88 = (t83 + 4);
    t89 = *((unsigned int *)t88);
    t90 = (~(t89));
    t91 = *((unsigned int *)t83);
    t92 = (t91 & t90);
    t93 = (t92 & 1U);
    if (t93 != 0)
        goto LAB42;

LAB43:    if (*((unsigned int *)t88) != 0)
        goto LAB44;

LAB45:    t95 = (t87 + 4);
    t96 = *((unsigned int *)t87);
    t97 = *((unsigned int *)t95);
    t98 = (t96 || t97);
    if (t98 > 0)
        goto LAB46;

LAB47:    memcpy(t113, t87, 8);

LAB48:    memset(t145, 0, 8);
    t146 = (t113 + 4);
    t147 = *((unsigned int *)t146);
    t148 = (~(t147));
    t149 = *((unsigned int *)t113);
    t150 = (t149 & t148);
    t151 = (t150 & 1U);
    if (t151 != 0)
        goto LAB61;

LAB62:    if (*((unsigned int *)t146) != 0)
        goto LAB63;

LAB64:    t154 = *((unsigned int *)t68);
    t155 = *((unsigned int *)t145);
    t156 = (t154 | t155);
    *((unsigned int *)t153) = t156;
    t157 = (t68 + 4);
    t158 = (t145 + 4);
    t159 = (t153 + 4);
    t160 = *((unsigned int *)t157);
    t161 = *((unsigned int *)t158);
    t162 = (t160 | t161);
    *((unsigned int *)t159) = t162;
    t163 = *((unsigned int *)t159);
    t164 = (t163 != 0);
    if (t164 == 1)
        goto LAB65;

LAB66:
LAB67:    goto LAB36;

LAB38:    t86 = (t83 + 4);
    *((unsigned int *)t83) = 1;
    *((unsigned int *)t86) = 1;
    goto LAB40;

LAB39:    *((unsigned int *)t83) = 1;
    goto LAB40;

LAB42:    *((unsigned int *)t87) = 1;
    goto LAB45;

LAB44:    t94 = (t87 + 4);
    *((unsigned int *)t87) = 1;
    *((unsigned int *)t94) = 1;
    goto LAB45;

LAB46:    t99 = (t0 + 1048U);
    t100 = *((char **)t99);
    t99 = ((char*)((ng3)));
    memset(t101, 0, 8);
    t102 = (t100 + 4);
    if (*((unsigned int *)t102) != 0)
        goto LAB50;

LAB49:    t103 = (t99 + 4);
    if (*((unsigned int *)t103) != 0)
        goto LAB50;

LAB53:    if (*((unsigned int *)t100) < *((unsigned int *)t99))
        goto LAB51;

LAB52:    memset(t105, 0, 8);
    t106 = (t101 + 4);
    t107 = *((unsigned int *)t106);
    t108 = (~(t107));
    t109 = *((unsigned int *)t101);
    t110 = (t109 & t108);
    t111 = (t110 & 1U);
    if (t111 != 0)
        goto LAB54;

LAB55:    if (*((unsigned int *)t106) != 0)
        goto LAB56;

LAB57:    t114 = *((unsigned int *)t87);
    t115 = *((unsigned int *)t105);
    t116 = (t114 & t115);
    *((unsigned int *)t113) = t116;
    t117 = (t87 + 4);
    t118 = (t105 + 4);
    t119 = (t113 + 4);
    t120 = *((unsigned int *)t117);
    t121 = *((unsigned int *)t118);
    t122 = (t120 | t121);
    *((unsigned int *)t119) = t122;
    t123 = *((unsigned int *)t119);
    t124 = (t123 != 0);
    if (t124 == 1)
        goto LAB58;

LAB59:
LAB60:    goto LAB48;

LAB50:    t104 = (t101 + 4);
    *((unsigned int *)t101) = 1;
    *((unsigned int *)t104) = 1;
    goto LAB52;

LAB51:    *((unsigned int *)t101) = 1;
    goto LAB52;

LAB54:    *((unsigned int *)t105) = 1;
    goto LAB57;

LAB56:    t112 = (t105 + 4);
    *((unsigned int *)t105) = 1;
    *((unsigned int *)t112) = 1;
    goto LAB57;

LAB58:    t125 = *((unsigned int *)t113);
    t126 = *((unsigned int *)t119);
    *((unsigned int *)t113) = (t125 | t126);
    t127 = (t87 + 4);
    t128 = (t105 + 4);
    t129 = *((unsigned int *)t87);
    t130 = (~(t129));
    t131 = *((unsigned int *)t127);
    t132 = (~(t131));
    t133 = *((unsigned int *)t105);
    t134 = (~(t133));
    t135 = *((unsigned int *)t128);
    t136 = (~(t135));
    t137 = (t130 & t132);
    t138 = (t134 & t136);
    t139 = (~(t137));
    t140 = (~(t138));
    t141 = *((unsigned int *)t119);
    *((unsigned int *)t119) = (t141 & t139);
    t142 = *((unsigned int *)t119);
    *((unsigned int *)t119) = (t142 & t140);
    t143 = *((unsigned int *)t113);
    *((unsigned int *)t113) = (t143 & t139);
    t144 = *((unsigned int *)t113);
    *((unsigned int *)t113) = (t144 & t140);
    goto LAB60;

LAB61:    *((unsigned int *)t145) = 1;
    goto LAB64;

LAB63:    t152 = (t145 + 4);
    *((unsigned int *)t145) = 1;
    *((unsigned int *)t152) = 1;
    goto LAB64;

LAB65:    t165 = *((unsigned int *)t153);
    t166 = *((unsigned int *)t159);
    *((unsigned int *)t153) = (t165 | t166);
    t167 = (t68 + 4);
    t168 = (t145 + 4);
    t169 = *((unsigned int *)t167);
    t170 = (~(t169));
    t171 = *((unsigned int *)t68);
    t172 = (t171 & t170);
    t173 = *((unsigned int *)t168);
    t174 = (~(t173));
    t175 = *((unsigned int *)t145);
    t176 = (t175 & t174);
    t177 = (~(t172));
    t178 = (~(t176));
    t179 = *((unsigned int *)t159);
    *((unsigned int *)t159) = (t179 & t177);
    t180 = *((unsigned int *)t159);
    *((unsigned int *)t159) = (t180 & t178);
    goto LAB67;

LAB68:    xsi_set_current_line(24, ng0);

LAB71:    xsi_set_current_line(25, ng0);
    t187 = ((char*)((ng1)));
    t188 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t188, t187, 0, 0, 1, 0LL);
    goto LAB70;

LAB74:    t7 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t7) = 1;
    goto LAB76;

LAB75:    *((unsigned int *)t6) = 1;
    goto LAB76;

LAB78:    *((unsigned int *)t10) = 1;
    goto LAB81;

LAB80:    t9 = (t10 + 4);
    *((unsigned int *)t10) = 1;
    *((unsigned int *)t9) = 1;
    goto LAB81;

LAB82:    t17 = (t0 + 1048U);
    t18 = *((char **)t17);
    t17 = ((char*)((ng3)));
    memset(t24, 0, 8);
    t22 = (t18 + 4);
    if (*((unsigned int *)t22) != 0)
        goto LAB86;

LAB85:    t23 = (t17 + 4);
    if (*((unsigned int *)t23) != 0)
        goto LAB86;

LAB89:    if (*((unsigned int *)t18) < *((unsigned int *)t17))
        goto LAB87;

LAB88:    memset(t28, 0, 8);
    t26 = (t24 + 4);
    t30 = *((unsigned int *)t26);
    t31 = (~(t30));
    t32 = *((unsigned int *)t24);
    t33 = (t32 & t31);
    t34 = (t33 & 1U);
    if (t34 != 0)
        goto LAB90;

LAB91:    if (*((unsigned int *)t26) != 0)
        goto LAB92;

LAB93:    t37 = *((unsigned int *)t10);
    t38 = *((unsigned int *)t28);
    t39 = (t37 & t38);
    *((unsigned int *)t36) = t39;
    t29 = (t10 + 4);
    t35 = (t28 + 4);
    t40 = (t36 + 4);
    t43 = *((unsigned int *)t29);
    t44 = *((unsigned int *)t35);
    t45 = (t43 | t44);
    *((unsigned int *)t40) = t45;
    t46 = *((unsigned int *)t40);
    t47 = (t46 != 0);
    if (t47 == 1)
        goto LAB94;

LAB95:
LAB96:    goto LAB84;

LAB86:    t25 = (t24 + 4);
    *((unsigned int *)t24) = 1;
    *((unsigned int *)t25) = 1;
    goto LAB88;

LAB87:    *((unsigned int *)t24) = 1;
    goto LAB88;

LAB90:    *((unsigned int *)t28) = 1;
    goto LAB93;

LAB92:    t27 = (t28 + 4);
    *((unsigned int *)t28) = 1;
    *((unsigned int *)t27) = 1;
    goto LAB93;

LAB94:    t48 = *((unsigned int *)t36);
    t49 = *((unsigned int *)t40);
    *((unsigned int *)t36) = (t48 | t49);
    t41 = (t10 + 4);
    t42 = (t28 + 4);
    t52 = *((unsigned int *)t10);
    t53 = (~(t52));
    t54 = *((unsigned int *)t41);
    t55 = (~(t54));
    t56 = *((unsigned int *)t28);
    t57 = (~(t56));
    t58 = *((unsigned int *)t42);
    t59 = (~(t58));
    t60 = (t53 & t55);
    t61 = (t57 & t59);
    t62 = (~(t60));
    t63 = (~(t61));
    t64 = *((unsigned int *)t40);
    *((unsigned int *)t40) = (t64 & t62);
    t65 = *((unsigned int *)t40);
    *((unsigned int *)t40) = (t65 & t63);
    t66 = *((unsigned int *)t36);
    *((unsigned int *)t36) = (t66 & t62);
    t67 = *((unsigned int *)t36);
    *((unsigned int *)t36) = (t67 & t63);
    goto LAB96;

LAB97:    *((unsigned int *)t68) = 1;
    goto LAB100;

LAB99:    t51 = (t68 + 4);
    *((unsigned int *)t68) = 1;
    *((unsigned int *)t51) = 1;
    goto LAB100;

LAB101:    t75 = (t0 + 1208U);
    t76 = *((char **)t75);
    t75 = ((char*)((ng4)));
    memset(t83, 0, 8);
    t81 = (t76 + 4);
    if (*((unsigned int *)t81) != 0)
        goto LAB105;

LAB104:    t82 = (t75 + 4);
    if (*((unsigned int *)t82) != 0)
        goto LAB105;

LAB108:    if (*((unsigned int *)t76) > *((unsigned int *)t75))
        goto LAB106;

LAB107:    memset(t87, 0, 8);
    t85 = (t83 + 4);
    t89 = *((unsigned int *)t85);
    t90 = (~(t89));
    t91 = *((unsigned int *)t83);
    t92 = (t91 & t90);
    t93 = (t92 & 1U);
    if (t93 != 0)
        goto LAB109;

LAB110:    if (*((unsigned int *)t85) != 0)
        goto LAB111;

LAB112:    t88 = (t87 + 4);
    t96 = *((unsigned int *)t87);
    t97 = *((unsigned int *)t88);
    t98 = (t96 || t97);
    if (t98 > 0)
        goto LAB113;

LAB114:    memcpy(t113, t87, 8);

LAB115:    memset(t145, 0, 8);
    t127 = (t113 + 4);
    t147 = *((unsigned int *)t127);
    t148 = (~(t147));
    t149 = *((unsigned int *)t113);
    t150 = (t149 & t148);
    t151 = (t150 & 1U);
    if (t151 != 0)
        goto LAB128;

LAB129:    if (*((unsigned int *)t127) != 0)
        goto LAB130;

LAB131:    t154 = *((unsigned int *)t68);
    t155 = *((unsigned int *)t145);
    t156 = (t154 | t155);
    *((unsigned int *)t153) = t156;
    t146 = (t68 + 4);
    t152 = (t145 + 4);
    t157 = (t153 + 4);
    t160 = *((unsigned int *)t146);
    t161 = *((unsigned int *)t152);
    t162 = (t160 | t161);
    *((unsigned int *)t157) = t162;
    t163 = *((unsigned int *)t157);
    t164 = (t163 != 0);
    if (t164 == 1)
        goto LAB132;

LAB133:
LAB134:    goto LAB103;

LAB105:    t84 = (t83 + 4);
    *((unsigned int *)t83) = 1;
    *((unsigned int *)t84) = 1;
    goto LAB107;

LAB106:    *((unsigned int *)t83) = 1;
    goto LAB107;

LAB109:    *((unsigned int *)t87) = 1;
    goto LAB112;

LAB111:    t86 = (t87 + 4);
    *((unsigned int *)t87) = 1;
    *((unsigned int *)t86) = 1;
    goto LAB112;

LAB113:    t94 = (t0 + 1048U);
    t95 = *((char **)t94);
    t94 = ((char*)((ng3)));
    memset(t101, 0, 8);
    t99 = (t95 + 4);
    if (*((unsigned int *)t99) != 0)
        goto LAB117;

LAB116:    t100 = (t94 + 4);
    if (*((unsigned int *)t100) != 0)
        goto LAB117;

LAB120:    if (*((unsigned int *)t95) < *((unsigned int *)t94))
        goto LAB118;

LAB119:    memset(t105, 0, 8);
    t103 = (t101 + 4);
    t107 = *((unsigned int *)t103);
    t108 = (~(t107));
    t109 = *((unsigned int *)t101);
    t110 = (t109 & t108);
    t111 = (t110 & 1U);
    if (t111 != 0)
        goto LAB121;

LAB122:    if (*((unsigned int *)t103) != 0)
        goto LAB123;

LAB124:    t114 = *((unsigned int *)t87);
    t115 = *((unsigned int *)t105);
    t116 = (t114 & t115);
    *((unsigned int *)t113) = t116;
    t106 = (t87 + 4);
    t112 = (t105 + 4);
    t117 = (t113 + 4);
    t120 = *((unsigned int *)t106);
    t121 = *((unsigned int *)t112);
    t122 = (t120 | t121);
    *((unsigned int *)t117) = t122;
    t123 = *((unsigned int *)t117);
    t124 = (t123 != 0);
    if (t124 == 1)
        goto LAB125;

LAB126:
LAB127:    goto LAB115;

LAB117:    t102 = (t101 + 4);
    *((unsigned int *)t101) = 1;
    *((unsigned int *)t102) = 1;
    goto LAB119;

LAB118:    *((unsigned int *)t101) = 1;
    goto LAB119;

LAB121:    *((unsigned int *)t105) = 1;
    goto LAB124;

LAB123:    t104 = (t105 + 4);
    *((unsigned int *)t105) = 1;
    *((unsigned int *)t104) = 1;
    goto LAB124;

LAB125:    t125 = *((unsigned int *)t113);
    t126 = *((unsigned int *)t117);
    *((unsigned int *)t113) = (t125 | t126);
    t118 = (t87 + 4);
    t119 = (t105 + 4);
    t129 = *((unsigned int *)t87);
    t130 = (~(t129));
    t131 = *((unsigned int *)t118);
    t132 = (~(t131));
    t133 = *((unsigned int *)t105);
    t134 = (~(t133));
    t135 = *((unsigned int *)t119);
    t136 = (~(t135));
    t137 = (t130 & t132);
    t138 = (t134 & t136);
    t139 = (~(t137));
    t140 = (~(t138));
    t141 = *((unsigned int *)t117);
    *((unsigned int *)t117) = (t141 & t139);
    t142 = *((unsigned int *)t117);
    *((unsigned int *)t117) = (t142 & t140);
    t143 = *((unsigned int *)t113);
    *((unsigned int *)t113) = (t143 & t139);
    t144 = *((unsigned int *)t113);
    *((unsigned int *)t113) = (t144 & t140);
    goto LAB127;

LAB128:    *((unsigned int *)t145) = 1;
    goto LAB131;

LAB130:    t128 = (t145 + 4);
    *((unsigned int *)t145) = 1;
    *((unsigned int *)t128) = 1;
    goto LAB131;

LAB132:    t165 = *((unsigned int *)t153);
    t166 = *((unsigned int *)t157);
    *((unsigned int *)t153) = (t165 | t166);
    t158 = (t68 + 4);
    t159 = (t145 + 4);
    t169 = *((unsigned int *)t158);
    t170 = (~(t169));
    t171 = *((unsigned int *)t68);
    t172 = (t171 & t170);
    t173 = *((unsigned int *)t159);
    t174 = (~(t173));
    t175 = *((unsigned int *)t145);
    t176 = (t175 & t174);
    t177 = (~(t172));
    t178 = (~(t176));
    t179 = *((unsigned int *)t157);
    *((unsigned int *)t157) = (t179 & t177);
    t180 = *((unsigned int *)t157);
    *((unsigned int *)t157) = (t180 & t178);
    goto LAB134;

LAB135:    xsi_set_current_line(31, ng0);

LAB138:    xsi_set_current_line(32, ng0);
    t168 = ((char*)((ng1)));
    t181 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t181, t168, 0, 0, 1, 0LL);
    goto LAB137;

LAB141:    t7 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t7) = 1;
    goto LAB143;

LAB142:    *((unsigned int *)t6) = 1;
    goto LAB143;

LAB145:    *((unsigned int *)t10) = 1;
    goto LAB148;

LAB147:    t9 = (t10 + 4);
    *((unsigned int *)t10) = 1;
    *((unsigned int *)t9) = 1;
    goto LAB148;

LAB149:    t17 = (t0 + 1048U);
    t18 = *((char **)t17);
    t17 = ((char*)((ng3)));
    memset(t24, 0, 8);
    t22 = (t18 + 4);
    if (*((unsigned int *)t22) != 0)
        goto LAB153;

LAB152:    t23 = (t17 + 4);
    if (*((unsigned int *)t23) != 0)
        goto LAB153;

LAB156:    if (*((unsigned int *)t18) < *((unsigned int *)t17))
        goto LAB154;

LAB155:    memset(t28, 0, 8);
    t26 = (t24 + 4);
    t30 = *((unsigned int *)t26);
    t31 = (~(t30));
    t32 = *((unsigned int *)t24);
    t33 = (t32 & t31);
    t34 = (t33 & 1U);
    if (t34 != 0)
        goto LAB157;

LAB158:    if (*((unsigned int *)t26) != 0)
        goto LAB159;

LAB160:    t37 = *((unsigned int *)t10);
    t38 = *((unsigned int *)t28);
    t39 = (t37 & t38);
    *((unsigned int *)t36) = t39;
    t29 = (t10 + 4);
    t35 = (t28 + 4);
    t40 = (t36 + 4);
    t43 = *((unsigned int *)t29);
    t44 = *((unsigned int *)t35);
    t45 = (t43 | t44);
    *((unsigned int *)t40) = t45;
    t46 = *((unsigned int *)t40);
    t47 = (t46 != 0);
    if (t47 == 1)
        goto LAB161;

LAB162:
LAB163:    goto LAB151;

LAB153:    t25 = (t24 + 4);
    *((unsigned int *)t24) = 1;
    *((unsigned int *)t25) = 1;
    goto LAB155;

LAB154:    *((unsigned int *)t24) = 1;
    goto LAB155;

LAB157:    *((unsigned int *)t28) = 1;
    goto LAB160;

LAB159:    t27 = (t28 + 4);
    *((unsigned int *)t28) = 1;
    *((unsigned int *)t27) = 1;
    goto LAB160;

LAB161:    t48 = *((unsigned int *)t36);
    t49 = *((unsigned int *)t40);
    *((unsigned int *)t36) = (t48 | t49);
    t41 = (t10 + 4);
    t42 = (t28 + 4);
    t52 = *((unsigned int *)t10);
    t53 = (~(t52));
    t54 = *((unsigned int *)t41);
    t55 = (~(t54));
    t56 = *((unsigned int *)t28);
    t57 = (~(t56));
    t58 = *((unsigned int *)t42);
    t59 = (~(t58));
    t60 = (t53 & t55);
    t61 = (t57 & t59);
    t62 = (~(t60));
    t63 = (~(t61));
    t64 = *((unsigned int *)t40);
    *((unsigned int *)t40) = (t64 & t62);
    t65 = *((unsigned int *)t40);
    *((unsigned int *)t40) = (t65 & t63);
    t66 = *((unsigned int *)t36);
    *((unsigned int *)t36) = (t66 & t62);
    t67 = *((unsigned int *)t36);
    *((unsigned int *)t36) = (t67 & t63);
    goto LAB163;

LAB164:    *((unsigned int *)t68) = 1;
    goto LAB167;

LAB166:    t51 = (t68 + 4);
    *((unsigned int *)t68) = 1;
    *((unsigned int *)t51) = 1;
    goto LAB167;

LAB168:    t75 = (t0 + 1208U);
    t76 = *((char **)t75);
    t75 = ((char*)((ng5)));
    memset(t83, 0, 8);
    t81 = (t76 + 4);
    if (*((unsigned int *)t81) != 0)
        goto LAB172;

LAB171:    t82 = (t75 + 4);
    if (*((unsigned int *)t82) != 0)
        goto LAB172;

LAB175:    if (*((unsigned int *)t76) > *((unsigned int *)t75))
        goto LAB173;

LAB174:    memset(t87, 0, 8);
    t85 = (t83 + 4);
    t89 = *((unsigned int *)t85);
    t90 = (~(t89));
    t91 = *((unsigned int *)t83);
    t92 = (t91 & t90);
    t93 = (t92 & 1U);
    if (t93 != 0)
        goto LAB176;

LAB177:    if (*((unsigned int *)t85) != 0)
        goto LAB178;

LAB179:    t88 = (t87 + 4);
    t96 = *((unsigned int *)t87);
    t97 = *((unsigned int *)t88);
    t98 = (t96 || t97);
    if (t98 > 0)
        goto LAB180;

LAB181:    memcpy(t113, t87, 8);

LAB182:    memset(t145, 0, 8);
    t127 = (t113 + 4);
    t147 = *((unsigned int *)t127);
    t148 = (~(t147));
    t149 = *((unsigned int *)t113);
    t150 = (t149 & t148);
    t151 = (t150 & 1U);
    if (t151 != 0)
        goto LAB195;

LAB196:    if (*((unsigned int *)t127) != 0)
        goto LAB197;

LAB198:    t154 = *((unsigned int *)t68);
    t155 = *((unsigned int *)t145);
    t156 = (t154 | t155);
    *((unsigned int *)t153) = t156;
    t146 = (t68 + 4);
    t152 = (t145 + 4);
    t157 = (t153 + 4);
    t160 = *((unsigned int *)t146);
    t161 = *((unsigned int *)t152);
    t162 = (t160 | t161);
    *((unsigned int *)t157) = t162;
    t163 = *((unsigned int *)t157);
    t164 = (t163 != 0);
    if (t164 == 1)
        goto LAB199;

LAB200:
LAB201:    goto LAB170;

LAB172:    t84 = (t83 + 4);
    *((unsigned int *)t83) = 1;
    *((unsigned int *)t84) = 1;
    goto LAB174;

LAB173:    *((unsigned int *)t83) = 1;
    goto LAB174;

LAB176:    *((unsigned int *)t87) = 1;
    goto LAB179;

LAB178:    t86 = (t87 + 4);
    *((unsigned int *)t87) = 1;
    *((unsigned int *)t86) = 1;
    goto LAB179;

LAB180:    t94 = (t0 + 1048U);
    t95 = *((char **)t94);
    t94 = ((char*)((ng3)));
    memset(t101, 0, 8);
    t99 = (t95 + 4);
    if (*((unsigned int *)t99) != 0)
        goto LAB184;

LAB183:    t100 = (t94 + 4);
    if (*((unsigned int *)t100) != 0)
        goto LAB184;

LAB187:    if (*((unsigned int *)t95) < *((unsigned int *)t94))
        goto LAB185;

LAB186:    memset(t105, 0, 8);
    t103 = (t101 + 4);
    t107 = *((unsigned int *)t103);
    t108 = (~(t107));
    t109 = *((unsigned int *)t101);
    t110 = (t109 & t108);
    t111 = (t110 & 1U);
    if (t111 != 0)
        goto LAB188;

LAB189:    if (*((unsigned int *)t103) != 0)
        goto LAB190;

LAB191:    t114 = *((unsigned int *)t87);
    t115 = *((unsigned int *)t105);
    t116 = (t114 & t115);
    *((unsigned int *)t113) = t116;
    t106 = (t87 + 4);
    t112 = (t105 + 4);
    t117 = (t113 + 4);
    t120 = *((unsigned int *)t106);
    t121 = *((unsigned int *)t112);
    t122 = (t120 | t121);
    *((unsigned int *)t117) = t122;
    t123 = *((unsigned int *)t117);
    t124 = (t123 != 0);
    if (t124 == 1)
        goto LAB192;

LAB193:
LAB194:    goto LAB182;

LAB184:    t102 = (t101 + 4);
    *((unsigned int *)t101) = 1;
    *((unsigned int *)t102) = 1;
    goto LAB186;

LAB185:    *((unsigned int *)t101) = 1;
    goto LAB186;

LAB188:    *((unsigned int *)t105) = 1;
    goto LAB191;

LAB190:    t104 = (t105 + 4);
    *((unsigned int *)t105) = 1;
    *((unsigned int *)t104) = 1;
    goto LAB191;

LAB192:    t125 = *((unsigned int *)t113);
    t126 = *((unsigned int *)t117);
    *((unsigned int *)t113) = (t125 | t126);
    t118 = (t87 + 4);
    t119 = (t105 + 4);
    t129 = *((unsigned int *)t87);
    t130 = (~(t129));
    t131 = *((unsigned int *)t118);
    t132 = (~(t131));
    t133 = *((unsigned int *)t105);
    t134 = (~(t133));
    t135 = *((unsigned int *)t119);
    t136 = (~(t135));
    t137 = (t130 & t132);
    t138 = (t134 & t136);
    t139 = (~(t137));
    t140 = (~(t138));
    t141 = *((unsigned int *)t117);
    *((unsigned int *)t117) = (t141 & t139);
    t142 = *((unsigned int *)t117);
    *((unsigned int *)t117) = (t142 & t140);
    t143 = *((unsigned int *)t113);
    *((unsigned int *)t113) = (t143 & t139);
    t144 = *((unsigned int *)t113);
    *((unsigned int *)t113) = (t144 & t140);
    goto LAB194;

LAB195:    *((unsigned int *)t145) = 1;
    goto LAB198;

LAB197:    t128 = (t145 + 4);
    *((unsigned int *)t145) = 1;
    *((unsigned int *)t128) = 1;
    goto LAB198;

LAB199:    t165 = *((unsigned int *)t153);
    t166 = *((unsigned int *)t157);
    *((unsigned int *)t153) = (t165 | t166);
    t158 = (t68 + 4);
    t159 = (t145 + 4);
    t169 = *((unsigned int *)t158);
    t170 = (~(t169));
    t171 = *((unsigned int *)t68);
    t172 = (t171 & t170);
    t173 = *((unsigned int *)t159);
    t174 = (~(t173));
    t175 = *((unsigned int *)t145);
    t176 = (t175 & t174);
    t177 = (~(t172));
    t178 = (~(t176));
    t179 = *((unsigned int *)t157);
    *((unsigned int *)t157) = (t179 & t177);
    t180 = *((unsigned int *)t157);
    *((unsigned int *)t157) = (t180 & t178);
    goto LAB201;

LAB202:    xsi_set_current_line(37, ng0);

LAB205:    xsi_set_current_line(38, ng0);
    t168 = ((char*)((ng1)));
    t181 = (t0 + 2248);
    xsi_vlogvar_wait_assign_value(t181, t168, 0, 0, 1, 0LL);
    goto LAB204;

}


extern void work_m_05896177934100398076_3696334355_init()
{
	static char *pe[] = {(void *)Initial_15_0,(void *)Always_22_1};
	xsi_register_didat("work_m_05896177934100398076_3696334355", "isim/K2T_isim_beh.exe.sim/work/m_05896177934100398076_3696334355.didat");
	xsi_register_executes(pe);
}
