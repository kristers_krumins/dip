////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.20131013
//  \   \         Application: netgen
//  /   /         Filename: T2_timesim.v
// /___/   /\     Timestamp: Wed Jan 15 18:58:49 2020
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -intstyle ise -s 5 -pcf T2.pcf -sdf_anno true -sdf_path netgen/par -insert_glbl true -insert_pp_buffers true -w -dir netgen/par -ofmt verilog -sim T2.ncd T2_timesim.v 
// Device	: 3s500efg320-5 (PRODUCTION 1.27 2013-10-13)
// Input file	: T2.ncd
// Output file	: C:\Users\Kristers\Desktop\INAD1\netgen\par\T2_timesim.v
// # of Modules	: 1
// Design Name	: T2
// Xilinx        : E:\Xilinx\14.7\ISE_DS\ISE\
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module T2 (
  MISO_L, MISO_R, CS, L0, L1, L2, L3, SCLK
);
  input MISO_L;
  input MISO_R;
  output CS;
  output L0;
  output L1;
  output L2;
  output L3;
  output SCLK;
  wire MISO_L_IBUF_22;
  wire MISO_R_IBUF_23;
  wire \L3/O ;
  wire \L2/O ;
  wire \CS/O ;
  wire \MISO_L/INBUF ;
  wire \L1/O ;
  wire \L0/O ;
  wire \MISO_R/INBUF ;
  wire \L3/OUTPUT/OFF/O1INV_40 ;
  wire \L2/OUTPUT/OFF/O1INV_32 ;
  wire \CS/OUTPUT/OFF/O1INV_48 ;
  wire \L1/OUTPUT/OFF/O1INV_76 ;
  wire \L0/OUTPUT/OFF/O1INV_68 ;
  initial $sdf_annotate("netgen/par/t2_timesim.sdf");
  X_OPAD #(
    .LOC ( "PAD40" ))
  \L3/PAD  (
    .PAD(L3)
  );
  X_OBUF #(
    .LOC ( "PAD40" ))
  L3_OBUF (
    .I(\L3/O ),
    .O(L3)
  );
  X_OPAD #(
    .LOC ( "PAD41" ))
  \L2/PAD  (
    .PAD(L2)
  );
  X_OBUF #(
    .LOC ( "PAD41" ))
  L2_OBUF (
    .I(\L2/O ),
    .O(L2)
  );
  X_OPAD #(
    .LOC ( "PAD4" ))
  \CS/PAD  (
    .PAD(CS)
  );
  X_OBUF #(
    .LOC ( "PAD4" ))
  CS_OBUF (
    .I(\CS/O ),
    .O(CS)
  );
  X_IPAD #(
    .LOC ( "PAD7" ))
  \MISO_L/PAD  (
    .PAD(MISO_L)
  );
  X_BUF #(
    .LOC ( "PAD7" ))
  MISO_L_IBUF (
    .I(MISO_L),
    .O(\MISO_L/INBUF )
  );
  X_OPAD #(
    .LOC ( "PAD44" ))
  \L1/PAD  (
    .PAD(L1)
  );
  X_OBUF #(
    .LOC ( "PAD44" ))
  L1_OBUF (
    .I(\L1/O ),
    .O(L1)
  );
  X_OPAD #(
    .LOC ( "PAD45" ))
  \L0/PAD  (
    .PAD(L0)
  );
  X_OBUF #(
    .LOC ( "PAD45" ))
  L0_OBUF (
    .I(\L0/O ),
    .O(L0)
  );
  X_IPAD #(
    .LOC ( "PAD5" ))
  \MISO_R/PAD  (
    .PAD(MISO_R)
  );
  X_BUF #(
    .LOC ( "PAD5" ))
  MISO_R_IBUF (
    .I(MISO_R),
    .O(\MISO_R/INBUF )
  );
  X_BUF #(
    .LOC ( "PAD5" ))
  \MISO_R/IFF/IMUX  (
    .I(\MISO_R/INBUF ),
    .O(MISO_R_IBUF_23)
  );
  X_BUF #(
    .LOC ( "PAD7" ))
  \MISO_L/IFF/IMUX  (
    .I(\MISO_L/INBUF ),
    .O(MISO_L_IBUF_22)
  );
  X_BUF #(
    .LOC ( "PAD40" ))
  \L3/OUTPUT/OFF/OMUX  (
    .I(\L3/OUTPUT/OFF/O1INV_40 ),
    .O(\L3/O )
  );
  X_BUF #(
    .LOC ( "PAD40" ))
  \L3/OUTPUT/OFF/O1INV  (
    .I(1'b0),
    .O(\L3/OUTPUT/OFF/O1INV_40 )
  );
  X_BUF #(
    .LOC ( "PAD41" ))
  \L2/OUTPUT/OFF/OMUX  (
    .I(\L2/OUTPUT/OFF/O1INV_32 ),
    .O(\L2/O )
  );
  X_BUF #(
    .LOC ( "PAD41" ))
  \L2/OUTPUT/OFF/O1INV  (
    .I(1'b0),
    .O(\L2/OUTPUT/OFF/O1INV_32 )
  );
  X_BUF #(
    .LOC ( "PAD4" ))
  \CS/OUTPUT/OFF/OMUX  (
    .I(\CS/OUTPUT/OFF/O1INV_48 ),
    .O(\CS/O )
  );
  X_BUF #(
    .LOC ( "PAD4" ))
  \CS/OUTPUT/OFF/O1INV  (
    .I(1'b0),
    .O(\CS/OUTPUT/OFF/O1INV_48 )
  );
  X_BUF #(
    .LOC ( "PAD44" ))
  \L1/OUTPUT/OFF/OMUX  (
    .I(\L1/OUTPUT/OFF/O1INV_76 ),
    .O(\L1/O )
  );
  X_BUF #(
    .LOC ( "PAD44" ))
  \L1/OUTPUT/OFF/O1INV  (
    .I(1'b0),
    .O(\L1/OUTPUT/OFF/O1INV_76 )
  );
  X_BUF #(
    .LOC ( "PAD45" ))
  \L0/OUTPUT/OFF/OMUX  (
    .I(\L0/OUTPUT/OFF/O1INV_68 ),
    .O(\L0/O )
  );
  X_BUF #(
    .LOC ( "PAD45" ))
  \L0/OUTPUT/OFF/O1INV  (
    .I(1'b1),
    .O(\L0/OUTPUT/OFF/O1INV_68 )
  );
endmodule


`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

