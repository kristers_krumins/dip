////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.20131013
//  \   \         Application: netgen
//  /   /         Filename: T2_synthesis.v
// /___/   /\     Timestamp: Wed Jan 15 23:01:17 2020
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -intstyle ise -insert_glbl true -w -dir netgen/synthesis -ofmt verilog -sim T2.ngc T2_synthesis.v 
// Device	: xc3s500e-5-fg320
// Input file	: T2.ngc
// Output file	: /home/raimonds/Desktop/INAD1/netgen/synthesis/T2_synthesis.v
// # of Modules	: 1
// Design Name	: T2
// Xilinx        : /opt/Xilinx/14.7/ISE_DS/ISE/
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module T2 (
  CLK_IN, CS, RESET, L0, L1, L2, L3, MISO_LP, MISO_RP, SCLK
);
  input CLK_IN;
  output CS;
  input RESET;
  output L0;
  output L1;
  output L2;
  output L3;
  input MISO_LP;
  input MISO_RP;
  output SCLK;
  wire CLK_IN_BUFGP_1;
  wire N0;
  wire N1;
  wire RESET_BUFGP_10;
  wire \XLXI_3/CS_12 ;
  wire \XLXI_3/SCLK_13 ;
  GND   XST_GND (
    .G(N0)
  );
  VCC   XST_VCC (
    .P(N1)
  );
  FD #(
    .INIT ( 1'b1 ))
  \XLXI_3/CS  (
    .C(RESET_BUFGP_10),
    .D(N0),
    .Q(\XLXI_3/CS_12 )
  );
  OBUF   CS_OBUF (
    .I(\XLXI_3/CS_12 ),
    .O(CS)
  );
  OBUF   L0_OBUF (
    .I(N1),
    .O(L0)
  );
  OBUF   L1_OBUF (
    .I(N1),
    .O(L1)
  );
  OBUF   L2_OBUF (
    .I(N1),
    .O(L2)
  );
  OBUF   L3_OBUF (
    .I(N1),
    .O(L3)
  );
  OBUF   SCLK_OBUF (
    .I(\XLXI_3/SCLK_13 ),
    .O(SCLK)
  );
  FDR #(
    .INIT ( 1'b0 ))
  \XLXI_3/SCLK  (
    .C(CLK_IN_BUFGP_1),
    .D(N1),
    .R(\XLXI_3/SCLK_13 ),
    .Q(\XLXI_3/SCLK_13 )
  );
  BUFGP   RESET_BUFGP (
    .I(RESET),
    .O(RESET_BUFGP_10)
  );
  BUFGP   CLK_IN_BUFGP (
    .I(CLK_IN),
    .O(CLK_IN_BUFGP_1)
  );
endmodule


`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

