<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1(11:0)" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <port polarity="Input" name="XLXN_2" />
        <port polarity="Input" name="XLXN_3" />
        <port polarity="Input" name="XLXN_4" />
        <port polarity="Output" name="XLXN_6" />
        <port polarity="Output" name="XLXN_7" />
        <blockdef name="Kernel">
            <timestamp>2020-1-16T18:15:21</timestamp>
            <line x2="0" y1="32" y2="32" x1="64" />
            <rect width="64" x="0" y="84" height="24" />
            <line x2="0" y1="96" y2="96" x1="64" />
            <rect width="64" x="0" y="148" height="24" />
            <line x2="0" y1="160" y2="160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="256" x="64" y="-128" height="384" />
        </blockdef>
        <blockdef name="SPIAD">
            <timestamp>2020-1-16T21:7:43</timestamp>
            <line x2="0" y1="96" y2="96" x1="64" />
            <line x2="384" y1="32" y2="32" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-256" height="448" />
        </blockdef>
        <block symbolname="Kernel" name="XLXI_1">
            <blockpin signalname="XLXN_5" name="clk" />
            <blockpin name="setF_clk" />
            <blockpin signalname="XLXN_1(11:0)" name="dataIn(11:0)" />
            <blockpin name="addr(11:0)" />
            <blockpin name="fVal(15:0)" />
            <blockpin name="dataOut(11:0)" />
        </block>
        <block symbolname="SPIAD" name="XLXI_2">
            <blockpin signalname="XLXN_2" name="clkIN" />
            <blockpin signalname="XLXN_4" name="MISO_L" />
            <blockpin signalname="XLXN_3" name="MISO_R" />
            <blockpin signalname="XLXN_6" name="SCLK" />
            <blockpin signalname="XLXN_7" name="CS" />
            <blockpin signalname="XLXN_5" name="CLK" />
            <blockpin signalname="XLXN_1(11:0)" name="dataOut_R(11:0)" />
            <blockpin name="dataOut_L(11:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1472" y="1184" name="XLXI_1" orien="R0">
        </instance>
        <instance x="656" y="1296" name="XLXI_2" orien="R0">
        </instance>
        <branch name="XLXN_1(11:0)">
            <wire x2="1248" y1="1200" y2="1200" x1="1040" />
            <wire x2="1248" y1="1152" y2="1200" x1="1248" />
            <wire x2="1472" y1="1152" y2="1152" x1="1248" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="656" y1="1392" y2="1392" x1="624" />
        </branch>
        <iomarker fontsize="28" x="624" y="1392" name="XLXN_2" orien="R180" />
        <branch name="XLXN_3">
            <wire x2="656" y1="1264" y2="1264" x1="624" />
        </branch>
        <iomarker fontsize="28" x="624" y="1264" name="XLXN_3" orien="R180" />
        <branch name="XLXN_4">
            <wire x2="656" y1="1168" y2="1168" x1="624" />
        </branch>
        <iomarker fontsize="28" x="624" y="1168" name="XLXN_4" orien="R180" />
        <branch name="XLXN_5">
            <wire x2="1232" y1="1328" y2="1328" x1="1040" />
            <wire x2="1232" y1="1088" y2="1328" x1="1232" />
            <wire x2="1472" y1="1088" y2="1088" x1="1232" />
        </branch>
        <branch name="XLXN_6">
            <wire x2="1072" y1="1072" y2="1072" x1="1040" />
        </branch>
        <iomarker fontsize="28" x="1072" y="1072" name="XLXN_6" orien="R0" />
        <branch name="XLXN_7">
            <wire x2="1072" y1="1136" y2="1136" x1="1040" />
        </branch>
        <iomarker fontsize="28" x="1072" y="1136" name="XLXN_7" orien="R0" />
    </sheet>
</drawing>