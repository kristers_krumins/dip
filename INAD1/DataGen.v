`timescale 1ns / 1ps

module DataGen(clk, Numb);
	 
	parameter WIDTH = 12;
	output reg [WIDTH -1:0] Numb;

	 input clk;
	
	initial begin 
		Numb = 1;
	end
	
	always@(negedge clk)begin
		Numb <= Numb + 1;
		if(Numb > 2000) begin
			Numb <= 0;
		end
	end 

endmodule 

