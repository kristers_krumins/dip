<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="SCLK" />
        <signal name="CS" />
        <signal name="XLXN_7(11:0)" />
        <signal name="XLXN_8(11:0)" />
        <signal name="L0" />
        <signal name="L1" />
        <signal name="L2" />
        <signal name="L3" />
        <signal name="MISO_L" />
        <signal name="MISO_R" />
        <port polarity="Output" name="SCLK" />
        <port polarity="Output" name="CS" />
        <port polarity="Output" name="L0" />
        <port polarity="Output" name="L1" />
        <port polarity="Output" name="L2" />
        <port polarity="Output" name="L3" />
        <port polarity="Input" name="MISO_L" />
        <port polarity="Input" name="MISO_R" />
        <blockdef name="LedCheck">
            <timestamp>2020-1-15T15:23:8</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-140" height="24" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="SPIAD">
            <timestamp>2020-1-15T15:39:43</timestamp>
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-256" height="256" />
        </blockdef>
        <block symbolname="LedCheck" name="XLXI_2">
            <blockpin name="clk" />
            <blockpin signalname="XLXN_7(11:0)" name="L(11:0)" />
            <blockpin signalname="XLXN_8(11:0)" name="R(11:0)" />
            <blockpin signalname="L0" name="LED0" />
            <blockpin signalname="L1" name="LED1" />
            <blockpin signalname="L2" name="LED2" />
            <blockpin signalname="L3" name="LED3" />
        </block>
        <block symbolname="SPIAD" name="XLXI_4">
            <blockpin name="MISO_L" />
            <blockpin name="MISO_R" />
            <blockpin name="SCLK" />
            <blockpin name="CS" />
            <blockpin name="dataOut_R(11:0)" />
            <blockpin name="dataOut_L(11:0)" />
            <blockpin name="CLK2" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="SCLK">
            <wire x2="1008" y1="656" y2="656" x1="992" />
            <wire x2="1024" y1="656" y2="656" x1="1008" />
        </branch>
        <iomarker fontsize="28" x="1024" y="656" name="SCLK" orien="R0" />
        <branch name="CS">
            <wire x2="1008" y1="720" y2="720" x1="992" />
            <wire x2="1024" y1="720" y2="720" x1="1008" />
        </branch>
        <iomarker fontsize="28" x="1024" y="720" name="CS" orien="R0" />
        <instance x="1264" y="912" name="XLXI_2" orien="R0">
        </instance>
        <branch name="XLXN_7(11:0)">
            <wire x2="1264" y1="784" y2="784" x1="992" />
        </branch>
        <branch name="XLXN_8(11:0)">
            <wire x2="1120" y1="848" y2="848" x1="992" />
            <wire x2="1120" y1="848" y2="880" x1="1120" />
            <wire x2="1264" y1="880" y2="880" x1="1120" />
        </branch>
        <branch name="L0">
            <wire x2="1680" y1="688" y2="688" x1="1648" />
        </branch>
        <iomarker fontsize="28" x="1680" y="688" name="L0" orien="R0" />
        <branch name="L1">
            <wire x2="1664" y1="752" y2="752" x1="1648" />
            <wire x2="1680" y1="752" y2="752" x1="1664" />
        </branch>
        <branch name="L2">
            <wire x2="1680" y1="816" y2="816" x1="1648" />
        </branch>
        <iomarker fontsize="28" x="1680" y="816" name="L2" orien="R0" />
        <branch name="L3">
            <wire x2="1680" y1="880" y2="880" x1="1648" />
        </branch>
        <iomarker fontsize="28" x="1680" y="880" name="L3" orien="R0" />
        <iomarker fontsize="28" x="1680" y="752" name="L1" orien="R0" />
        <branch name="MISO_L">
            <wire x2="464" y1="768" y2="768" x1="448" />
            <wire x2="480" y1="768" y2="768" x1="464" />
        </branch>
        <branch name="MISO_R">
            <wire x2="464" y1="864" y2="864" x1="448" />
            <wire x2="480" y1="864" y2="864" x1="464" />
        </branch>
        <iomarker fontsize="28" x="448" y="768" name="MISO_L" orien="R180" />
        <iomarker fontsize="28" x="448" y="864" name="MISO_R" orien="R180" />
        <instance x="544" y="944" name="XLXI_4" orien="R0">
        </instance>
    </sheet>
</drawing>