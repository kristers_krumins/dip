`timescale 1ns / 1ps

module SPIAD(clkIN,MISO_L,MISO_R,dataOut_R,dataOut_L,SCLK,CS,CLK);
	 	parameter WIDTH = 12;
		
		input clkIN;
		reg reset;
		
		input MISO_L;
		input MISO_R;
		
		output reg [WIDTH - 1: 0] dataOut_R;
		output reg [WIDTH - 1: 0] dataOut_L;
		
		output reg SCLK;	//clock to ADC
		output reg CS;   //chip select
		output reg CLK;  //clock out
		 
	 reg [6:0] i;
	 reg [6:0] j;

	 reg [15:0]temp_R;
	 reg [15:0]temp_L;	 
	initial begin
	CLK = 0;
	SCLK <= 0;
	CS <= 1;
	i <= 0;
	j = 0;
	dataOut_L <= 0;
	dataOut_R <= 0;
	temp_L<=0;
	temp_R<=0;
	end
	
	always@(posedge clkIN)begin
		

	
		//CLK = ~CLK;
	
		if(CS == 0) begin
			 SCLK <= ~SCLK;
		end
		else begin
			SCLK <= SCLK;
		end
		
		j <= j+1;
		if(j == 20)
		begin
			reset <= 1;
		end
		else begin
			reset <=0;
		end
		
		
		
	end
	
	always@(posedge SCLK) begin
		if(i < 4)begin
			dataOut_L[0]<= MISO_L;
			dataOut_R[0]<= MISO_R;
		
		end
		else begin
			dataOut_L[15-i]<= MISO_L;
			dataOut_R[15-i]<= MISO_R;
		end

		i = i + 1;
		if(i==16)begin
			CLK = 1;
			i = 0;
		end
		else begin
			CLK = 0;
		end
	end
		
	always@(posedge reset)begin CS <= 0; end
	
	
//	
//	
//	always @(posedge SCLK) begin		
//			
//			temp_L[15-i] = MISO_L;
//			temp_R[15-i] = MISO_R;
//			temp_L = temp_L+1;
//			i = i+1;
//			
//			if(i == 16)begin
//				i = 0;
//				CLK <= 1;
//			end
// 
//			else begin
//				CLK <= 0;
//			end
//			
//		end
endmodule
