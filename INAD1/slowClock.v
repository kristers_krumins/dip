`timescale 1ns / 1ps

module slowClock(
	clk,
	o_clk
    );
	input clk;
	output reg o_clk;

	 
reg [31:0] i;

initial begin
	o_clk = 0;
	i = 0;
 end
 
 always@(posedge clk) begin
	i = i + 1;
	if(i > 40000) begin
		i = 0;
		o_clk = ~o_clk;
	end
 end

endmodule
