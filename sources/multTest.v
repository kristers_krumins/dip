`timescale 1ns / 1ps

module multTest;
	
	parameter SIZE = 11;
	// Inputs
	reg [SIZE:0] NumbIn;
	reg [SIZE:0] NumbIn2;
	reg clk;

	// Outputs
	wire [SIZE:0] NumbOut;

	// Instantiate the Unit Under Test (UUT)
	multTemp #(.CHAR_SIZE(8), .MANTISSA_SIZE(4)) uut (
		.NumbIn(NumbIn),
		.NumbIn2(NumbIn2),
		.clk(clk),
		.NumbOut(NumbOut)
	);

	always #10 clk = ~clk;
	
	initial begin
	clk = 0;
		NumbIn = 'b10_0000;
		NumbIn2 = 'b1000;
		#20
		NumbIn = 'b10_0000;
		NumbIn2 = 'b1_0000;
		#20
		NumbIn = 'b100_0000;
		NumbIn2 = 'b1000;
		#20
		NumbIn = 'b100_0000;
		NumbIn2 = 'b100;
		#20
		NumbIn = 'b100_0000;
		NumbIn2 = 'b10;
		#20
		NumbIn = 'b10_0000;
		NumbIn2 = 'b1_1000;
		#20
		NumbIn = 'b101_0000;
		NumbIn2 = 'b1_1000;
	end
      
endmodule

