<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1(11:0)" />
        <signal name="XLXN_2(11:0)" />
        <signal name="CLK_IN" />
        <signal name="XLXN_5(11:0)" />
        <signal name="L0" />
        <signal name="L1" />
        <signal name="L2" />
        <signal name="L3" />
        <port polarity="Input" name="CLK_IN" />
        <port polarity="Output" name="L0" />
        <port polarity="Output" name="L1" />
        <port polarity="Output" name="L2" />
        <port polarity="Output" name="L3" />
        <blockdef name="multTemp">
            <timestamp>2020-1-15T21:51:4</timestamp>
            <rect width="320" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="384" y="-172" height="24" />
            <line x2="448" y1="-160" y2="-160" x1="384" />
        </blockdef>
        <blockdef name="multGen">
            <timestamp>2020-1-15T21:52:58</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="LedCheck">
            <timestamp>2020-1-15T17:26:15</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-140" height="24" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <block symbolname="multTemp" name="XLXI_1">
            <blockpin signalname="CLK_IN" name="clk" />
            <blockpin signalname="XLXN_2(11:0)" name="NumbIn(11:0)" />
            <blockpin signalname="XLXN_1(11:0)" name="NumbIn2(11:0)" />
            <blockpin signalname="XLXN_5(11:0)" name="NumbOut(11:0)" />
        </block>
        <block symbolname="multGen" name="XLXI_2">
            <blockpin signalname="CLK_IN" name="clk" />
            <blockpin signalname="XLXN_2(11:0)" name="Numb1(11:0)" />
            <blockpin signalname="XLXN_1(11:0)" name="Numb2(11:0)" />
        </block>
        <block symbolname="LedCheck" name="XLXI_3">
            <blockpin signalname="CLK_IN" name="clk" />
            <blockpin name="L(11:0)" />
            <blockpin signalname="XLXN_5(11:0)" name="R(11:0)" />
            <blockpin signalname="L0" name="LED0" />
            <blockpin signalname="L1" name="LED1" />
            <blockpin signalname="L2" name="LED2" />
            <blockpin signalname="L3" name="LED3" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1472" y="1136" name="XLXI_1" orien="R0">
        </instance>
        <instance x="976" y="1104" name="XLXI_2" orien="R0">
        </instance>
        <branch name="XLXN_1(11:0)">
            <wire x2="1408" y1="1072" y2="1072" x1="1360" />
            <wire x2="1408" y1="1072" y2="1104" x1="1408" />
            <wire x2="1472" y1="1104" y2="1104" x1="1408" />
        </branch>
        <branch name="XLXN_2(11:0)">
            <wire x2="1408" y1="1008" y2="1008" x1="1360" />
            <wire x2="1408" y1="1008" y2="1040" x1="1408" />
            <wire x2="1472" y1="1040" y2="1040" x1="1408" />
        </branch>
        <branch name="CLK_IN">
            <wire x2="976" y1="1008" y2="1008" x1="944" />
        </branch>
        <iomarker fontsize="28" x="944" y="1008" name="CLK_IN" orien="R180" />
        <branch name="CLK_IN">
            <wire x2="1456" y1="928" y2="928" x1="1440" />
            <wire x2="1456" y1="928" y2="976" x1="1456" />
            <wire x2="1472" y1="976" y2="976" x1="1456" />
        </branch>
        <iomarker fontsize="28" x="1440" y="928" name="CLK_IN" orien="R180" />
        <instance x="1744" y="800" name="XLXI_3" orien="R0">
        </instance>
        <branch name="XLXN_5(11:0)">
            <wire x2="1744" y1="768" y2="768" x1="1680" />
            <wire x2="1680" y1="768" y2="880" x1="1680" />
            <wire x2="1984" y1="880" y2="880" x1="1680" />
            <wire x2="1984" y1="880" y2="976" x1="1984" />
            <wire x2="1984" y1="976" y2="976" x1="1920" />
        </branch>
        <branch name="CLK_IN">
            <wire x2="1744" y1="576" y2="576" x1="1712" />
        </branch>
        <iomarker fontsize="28" x="1712" y="576" name="CLK_IN" orien="R180" />
        <branch name="L0">
            <wire x2="2160" y1="576" y2="576" x1="2128" />
        </branch>
        <iomarker fontsize="28" x="2160" y="576" name="L0" orien="R0" />
        <branch name="L1">
            <wire x2="2160" y1="640" y2="640" x1="2128" />
        </branch>
        <iomarker fontsize="28" x="2160" y="640" name="L1" orien="R0" />
        <branch name="L2">
            <wire x2="2160" y1="704" y2="704" x1="2128" />
        </branch>
        <iomarker fontsize="28" x="2160" y="704" name="L2" orien="R0" />
        <branch name="L3">
            <wire x2="2160" y1="768" y2="768" x1="2128" />
        </branch>
        <iomarker fontsize="28" x="2160" y="768" name="L3" orien="R0" />
    </sheet>
</drawing>