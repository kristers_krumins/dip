////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.20131013
//  \   \         Application: netgen
//  /   /         Filename: KernelTEST_synthesis.v
// /___/   /\     Timestamp: Thu Jan 16 12:39:18 2020
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -intstyle ise -insert_glbl true -w -dir netgen/synthesis -ofmt verilog -sim KernelTEST.ngc KernelTEST_synthesis.v 
// Device	: xc3s500e-5-fg320
// Input file	: KernelTEST.ngc
// Output file	: /home/raimonds/Desktop/repos/dip/INAD1/netgen/synthesis/KernelTEST_synthesis.v
// # of Modules	: 1
// Design Name	: KernelTEST
// Xilinx        : /opt/Xilinx/14.7/ISE_DS/ISE/
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module KernelTEST (
  CLK, L0, L1, L2, L3
);
  input CLK;
  output L0;
  output L1;
  output L2;
  output L3;
  wire CLK_BUFGP_1;
  wire CLK_inv;
  wire N0;
  wire N1;
  wire \XLXI_3/LED1_21 ;
  wire \XLXI_3/LED2_22 ;
  wire \XLXI_3/LED3_23 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<0>1 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<0>2 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<1>1 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<1>2 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<1>_0_rt_30 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<2>1 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<2>2 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<2>_rt_34 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<3>1 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<3>2 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<4>1 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<4>2 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<4>_1_rt_41 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<5>1 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<5>2 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<5>_rt_45 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<6>_rt_47 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<0>1_49 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<0>2_50 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<1>1 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<2>1_54 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<3>1_56 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<3>2 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<4>1_59 ;
  wire \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<5>1_61 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<0>_62 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<0>_rt_63 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<10>_64 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<10>_rt_65 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<11>_66 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<11>_rt_67 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<12>_68 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<12>_rt_69 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<13>_70 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<13>_rt_71 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<14>_72 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<14>_rt_73 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<1>_74 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<1>_rt_75 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<2>_76 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<2>_rt_77 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<3>_78 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<3>_rt_79 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<4>_80 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<4>_rt_81 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<5>_82 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<5>_rt_83 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<6>_84 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<6>_rt_85 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<7>_86 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<7>_rt_87 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<8>_88 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<8>_rt_89 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<9>_90 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<9>_rt_91 ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<0> ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<10> ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<11> ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<12> ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<13> ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<14> ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<15> ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<1> ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<2> ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<3> ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<4> ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<5> ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<6> ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<7> ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<8> ;
  wire \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<9> ;
  wire \XLXI_4/KernelBL[0].KB/value<0> ;
  wire \XLXI_4/KernelBL[0].KB/value<1> ;
  wire \XLXI_4/KernelBL[0].KB/value<10> ;
  wire \XLXI_4/KernelBL[0].KB/value<11> ;
  wire \XLXI_4/KernelBL[0].KB/value<12> ;
  wire \XLXI_4/KernelBL[0].KB/value<13> ;
  wire \XLXI_4/KernelBL[0].KB/value<14> ;
  wire \XLXI_4/KernelBL[0].KB/value<15> ;
  wire \XLXI_4/KernelBL[0].KB/value<2> ;
  wire \XLXI_4/KernelBL[0].KB/value<3> ;
  wire \XLXI_4/KernelBL[0].KB/value<4> ;
  wire \XLXI_4/KernelBL[0].KB/value<5> ;
  wire \XLXI_4/KernelBL[0].KB/value<6> ;
  wire \XLXI_4/KernelBL[0].KB/value<7> ;
  wire \XLXI_4/KernelBL[0].KB/value<8> ;
  wire \XLXI_4/KernelBL[0].KB/value<9> ;
  wire \XLXI_4/KernelBL[0].KB/value_add0000<0> ;
  wire \XLXI_4/KernelBL[0].KB/value_add0000<10> ;
  wire \XLXI_4/KernelBL[0].KB/value_add0000<11> ;
  wire \XLXI_4/KernelBL[0].KB/value_add0000<12> ;
  wire \XLXI_4/KernelBL[0].KB/value_add0000<13> ;
  wire \XLXI_4/KernelBL[0].KB/value_add0000<14> ;
  wire \XLXI_4/KernelBL[0].KB/value_add0000<15> ;
  wire \XLXI_4/KernelBL[0].KB/value_add0000<1> ;
  wire \XLXI_4/KernelBL[0].KB/value_add0000<2> ;
  wire \XLXI_4/KernelBL[0].KB/value_add0000<3> ;
  wire \XLXI_4/KernelBL[0].KB/value_add0000<4> ;
  wire \XLXI_4/KernelBL[0].KB/value_add0000<5> ;
  wire \XLXI_4/KernelBL[0].KB/value_add0000<6> ;
  wire \XLXI_4/KernelBL[0].KB/value_add0000<7> ;
  wire \XLXI_4/KernelBL[0].KB/value_add0000<8> ;
  wire \XLXI_4/KernelBL[0].KB/value_add0000<9> ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<0>_140 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<10>_141 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<11>_142 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<12>_143 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<13>_144 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<14>_145 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<1>_146 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<2>_147 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<3>_148 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<4>_149 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<5>_150 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<6>_151 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<7>_152 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<8>_153 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<9>_154 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<0>_155 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<10>_156 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<11>_157 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<12>_158 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<13>_159 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<14>_160 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<15>_161 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<1>_162 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<2>_163 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<3>_164 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<4>_165 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<5>_166 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<6>_167 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<7>_168 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<8>_169 ;
  wire \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<9>_170 ;
  wire \XLXI_4/KernelBL[10].KB/_old_tmp_2<10> ;
  wire \XLXI_4/KernelBL[10].KB/_old_tmp_2<11> ;
  wire \XLXI_4/KernelBL[10].KB/_old_tmp_2<12> ;
  wire \XLXI_4/KernelBL[10].KB/_old_tmp_2<13> ;
  wire \XLXI_4/KernelBL[10].KB/_old_tmp_2<14> ;
  wire \XLXI_4/KernelBL[10].KB/_old_tmp_2<15> ;
  wire \XLXI_4/KernelBL[10].KB/_old_tmp_2<16> ;
  wire \XLXI_4/KernelBL[10].KB/_old_tmp_2<17> ;
  wire \XLXI_4/KernelBL[10].KB/_old_tmp_2<18> ;
  wire \XLXI_4/KernelBL[10].KB/_old_tmp_2<19> ;
  wire \XLXI_4/KernelBL[10].KB/_old_tmp_2<4> ;
  wire \XLXI_4/KernelBL[10].KB/_old_tmp_2<5> ;
  wire \XLXI_4/KernelBL[10].KB/_old_tmp_2<6> ;
  wire \XLXI_4/KernelBL[10].KB/_old_tmp_2<7> ;
  wire \XLXI_4/KernelBL[10].KB/_old_tmp_2<8> ;
  wire \XLXI_4/KernelBL[10].KB/_old_tmp_2<9> ;
  wire \XLXI_4/KernelBL[10].KB/test<0> ;
  wire \XLXI_4/KernelBL[10].KB/test<1> ;
  wire \XLXI_4/KernelBL[10].KB/test<10> ;
  wire \XLXI_4/KernelBL[10].KB/test<11> ;
  wire \XLXI_4/KernelBL[10].KB/test<12> ;
  wire \XLXI_4/KernelBL[10].KB/test<13> ;
  wire \XLXI_4/KernelBL[10].KB/test<14> ;
  wire \XLXI_4/KernelBL[10].KB/test<15> ;
  wire \XLXI_4/KernelBL[10].KB/test<2> ;
  wire \XLXI_4/KernelBL[10].KB/test<3> ;
  wire \XLXI_4/KernelBL[10].KB/test<4> ;
  wire \XLXI_4/KernelBL[10].KB/test<5> ;
  wire \XLXI_4/KernelBL[10].KB/test<6> ;
  wire \XLXI_4/KernelBL[10].KB/test<7> ;
  wire \XLXI_4/KernelBL[10].KB/test<8> ;
  wire \XLXI_4/KernelBL[10].KB/test<9> ;
  wire \XLXI_4/KernelBL[10].KB/value<10> ;
  wire \XLXI_4/KernelBL[10].KB/value<11> ;
  wire \XLXI_4/KernelBL[10].KB/value<12> ;
  wire \XLXI_4/KernelBL[10].KB/value<13> ;
  wire \XLXI_4/KernelBL[10].KB/value<14> ;
  wire \XLXI_4/KernelBL[10].KB/value<15> ;
  wire \XLXI_4/KernelBL[10].KB/value<4> ;
  wire \XLXI_4/KernelBL[10].KB/value<5> ;
  wire \XLXI_4/KernelBL[10].KB/value<6> ;
  wire \XLXI_4/KernelBL[10].KB/value<7> ;
  wire \XLXI_4/KernelBL[10].KB/value<8> ;
  wire \XLXI_4/KernelBL[10].KB/value<9> ;
  wire \XLXI_4/KernelBL[10].KB/value_add0000<10> ;
  wire \XLXI_4/KernelBL[10].KB/value_add0000<11> ;
  wire \XLXI_4/KernelBL[10].KB/value_add0000<12> ;
  wire \XLXI_4/KernelBL[10].KB/value_add0000<13> ;
  wire \XLXI_4/KernelBL[10].KB/value_add0000<14> ;
  wire \XLXI_4/KernelBL[10].KB/value_add0000<15> ;
  wire \XLXI_4/KernelBL[10].KB/value_add0000<4> ;
  wire \XLXI_4/KernelBL[10].KB/value_add0000<5> ;
  wire \XLXI_4/KernelBL[10].KB/value_add0000<6> ;
  wire \XLXI_4/KernelBL[10].KB/value_add0000<7> ;
  wire \XLXI_4/KernelBL[10].KB/value_add0000<8> ;
  wire \XLXI_4/KernelBL[10].KB/value_add0000<9> ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<0>_227 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<10>_228 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<11>_229 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<12>_230 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<13>_231 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<14>_232 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<1>_233 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<2>_234 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<3>_235 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<4>_236 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<5>_237 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<6>_238 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<7>_239 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<8>_240 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<9>_241 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<0>_242 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<10>_243 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<11>_244 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<12>_245 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<13>_246 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<14>_247 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<15>_248 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<1>_249 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<2>_250 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<3>_251 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<4>_252 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<5>_253 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<6>_254 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<7>_255 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<8>_256 ;
  wire \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<9>_257 ;
  wire \XLXI_4/KernelBL[1].KB/_old_tmp_2<10> ;
  wire \XLXI_4/KernelBL[1].KB/_old_tmp_2<11> ;
  wire \XLXI_4/KernelBL[1].KB/_old_tmp_2<12> ;
  wire \XLXI_4/KernelBL[1].KB/_old_tmp_2<13> ;
  wire \XLXI_4/KernelBL[1].KB/_old_tmp_2<14> ;
  wire \XLXI_4/KernelBL[1].KB/_old_tmp_2<15> ;
  wire \XLXI_4/KernelBL[1].KB/_old_tmp_2<16> ;
  wire \XLXI_4/KernelBL[1].KB/_old_tmp_2<17> ;
  wire \XLXI_4/KernelBL[1].KB/_old_tmp_2<18> ;
  wire \XLXI_4/KernelBL[1].KB/_old_tmp_2<19> ;
  wire \XLXI_4/KernelBL[1].KB/_old_tmp_2<4> ;
  wire \XLXI_4/KernelBL[1].KB/_old_tmp_2<5> ;
  wire \XLXI_4/KernelBL[1].KB/_old_tmp_2<6> ;
  wire \XLXI_4/KernelBL[1].KB/_old_tmp_2<7> ;
  wire \XLXI_4/KernelBL[1].KB/_old_tmp_2<8> ;
  wire \XLXI_4/KernelBL[1].KB/_old_tmp_2<9> ;
  wire \XLXI_4/KernelBL[1].KB/test<0> ;
  wire \XLXI_4/KernelBL[1].KB/test<1> ;
  wire \XLXI_4/KernelBL[1].KB/test<10> ;
  wire \XLXI_4/KernelBL[1].KB/test<11> ;
  wire \XLXI_4/KernelBL[1].KB/test<12> ;
  wire \XLXI_4/KernelBL[1].KB/test<13> ;
  wire \XLXI_4/KernelBL[1].KB/test<14> ;
  wire \XLXI_4/KernelBL[1].KB/test<15> ;
  wire \XLXI_4/KernelBL[1].KB/test<2> ;
  wire \XLXI_4/KernelBL[1].KB/test<3> ;
  wire \XLXI_4/KernelBL[1].KB/test<4> ;
  wire \XLXI_4/KernelBL[1].KB/test<5> ;
  wire \XLXI_4/KernelBL[1].KB/test<6> ;
  wire \XLXI_4/KernelBL[1].KB/test<7> ;
  wire \XLXI_4/KernelBL[1].KB/test<8> ;
  wire \XLXI_4/KernelBL[1].KB/test<9> ;
  wire \XLXI_4/KernelBL[1].KB/value<0> ;
  wire \XLXI_4/KernelBL[1].KB/value<1> ;
  wire \XLXI_4/KernelBL[1].KB/value<10> ;
  wire \XLXI_4/KernelBL[1].KB/value<11> ;
  wire \XLXI_4/KernelBL[1].KB/value<12> ;
  wire \XLXI_4/KernelBL[1].KB/value<13> ;
  wire \XLXI_4/KernelBL[1].KB/value<14> ;
  wire \XLXI_4/KernelBL[1].KB/value<15> ;
  wire \XLXI_4/KernelBL[1].KB/value<2> ;
  wire \XLXI_4/KernelBL[1].KB/value<3> ;
  wire \XLXI_4/KernelBL[1].KB/value<4> ;
  wire \XLXI_4/KernelBL[1].KB/value<5> ;
  wire \XLXI_4/KernelBL[1].KB/value<6> ;
  wire \XLXI_4/KernelBL[1].KB/value<7> ;
  wire \XLXI_4/KernelBL[1].KB/value<8> ;
  wire \XLXI_4/KernelBL[1].KB/value<9> ;
  wire \XLXI_4/KernelBL[1].KB/value_add0000<0> ;
  wire \XLXI_4/KernelBL[1].KB/value_add0000<10> ;
  wire \XLXI_4/KernelBL[1].KB/value_add0000<11> ;
  wire \XLXI_4/KernelBL[1].KB/value_add0000<12> ;
  wire \XLXI_4/KernelBL[1].KB/value_add0000<13> ;
  wire \XLXI_4/KernelBL[1].KB/value_add0000<14> ;
  wire \XLXI_4/KernelBL[1].KB/value_add0000<15> ;
  wire \XLXI_4/KernelBL[1].KB/value_add0000<1> ;
  wire \XLXI_4/KernelBL[1].KB/value_add0000<2> ;
  wire \XLXI_4/KernelBL[1].KB/value_add0000<3> ;
  wire \XLXI_4/KernelBL[1].KB/value_add0000<4> ;
  wire \XLXI_4/KernelBL[1].KB/value_add0000<5> ;
  wire \XLXI_4/KernelBL[1].KB/value_add0000<6> ;
  wire \XLXI_4/KernelBL[1].KB/value_add0000<7> ;
  wire \XLXI_4/KernelBL[1].KB/value_add0000<8> ;
  wire \XLXI_4/KernelBL[1].KB/value_add0000<9> ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<0>_322 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<10>_323 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<11>_324 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<12>_325 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<13>_326 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<14>_327 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<1>_328 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<2>_329 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<3>_330 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<4>_331 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<5>_332 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<6>_333 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<7>_334 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<8>_335 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<9>_336 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<0>_337 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<10>_338 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<11>_339 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<12>_340 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<13>_341 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<14>_342 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<15>_343 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<1>_344 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<2>_345 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<3>_346 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<4>_347 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<5>_348 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<6>_349 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<7>_350 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<8>_351 ;
  wire \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<9>_352 ;
  wire \XLXI_4/KernelBL[2].KB/_old_tmp_2<10> ;
  wire \XLXI_4/KernelBL[2].KB/_old_tmp_2<11> ;
  wire \XLXI_4/KernelBL[2].KB/_old_tmp_2<12> ;
  wire \XLXI_4/KernelBL[2].KB/_old_tmp_2<13> ;
  wire \XLXI_4/KernelBL[2].KB/_old_tmp_2<14> ;
  wire \XLXI_4/KernelBL[2].KB/_old_tmp_2<15> ;
  wire \XLXI_4/KernelBL[2].KB/_old_tmp_2<16> ;
  wire \XLXI_4/KernelBL[2].KB/_old_tmp_2<17> ;
  wire \XLXI_4/KernelBL[2].KB/_old_tmp_2<18> ;
  wire \XLXI_4/KernelBL[2].KB/_old_tmp_2<19> ;
  wire \XLXI_4/KernelBL[2].KB/_old_tmp_2<4> ;
  wire \XLXI_4/KernelBL[2].KB/_old_tmp_2<5> ;
  wire \XLXI_4/KernelBL[2].KB/_old_tmp_2<6> ;
  wire \XLXI_4/KernelBL[2].KB/_old_tmp_2<7> ;
  wire \XLXI_4/KernelBL[2].KB/_old_tmp_2<8> ;
  wire \XLXI_4/KernelBL[2].KB/_old_tmp_2<9> ;
  wire \XLXI_4/KernelBL[2].KB/test<0> ;
  wire \XLXI_4/KernelBL[2].KB/test<1> ;
  wire \XLXI_4/KernelBL[2].KB/test<10> ;
  wire \XLXI_4/KernelBL[2].KB/test<11> ;
  wire \XLXI_4/KernelBL[2].KB/test<12> ;
  wire \XLXI_4/KernelBL[2].KB/test<13> ;
  wire \XLXI_4/KernelBL[2].KB/test<14> ;
  wire \XLXI_4/KernelBL[2].KB/test<15> ;
  wire \XLXI_4/KernelBL[2].KB/test<2> ;
  wire \XLXI_4/KernelBL[2].KB/test<3> ;
  wire \XLXI_4/KernelBL[2].KB/test<4> ;
  wire \XLXI_4/KernelBL[2].KB/test<5> ;
  wire \XLXI_4/KernelBL[2].KB/test<6> ;
  wire \XLXI_4/KernelBL[2].KB/test<7> ;
  wire \XLXI_4/KernelBL[2].KB/test<8> ;
  wire \XLXI_4/KernelBL[2].KB/test<9> ;
  wire \XLXI_4/KernelBL[2].KB/value<0> ;
  wire \XLXI_4/KernelBL[2].KB/value<1> ;
  wire \XLXI_4/KernelBL[2].KB/value<10> ;
  wire \XLXI_4/KernelBL[2].KB/value<11> ;
  wire \XLXI_4/KernelBL[2].KB/value<12> ;
  wire \XLXI_4/KernelBL[2].KB/value<13> ;
  wire \XLXI_4/KernelBL[2].KB/value<14> ;
  wire \XLXI_4/KernelBL[2].KB/value<15> ;
  wire \XLXI_4/KernelBL[2].KB/value<2> ;
  wire \XLXI_4/KernelBL[2].KB/value<3> ;
  wire \XLXI_4/KernelBL[2].KB/value<4> ;
  wire \XLXI_4/KernelBL[2].KB/value<5> ;
  wire \XLXI_4/KernelBL[2].KB/value<6> ;
  wire \XLXI_4/KernelBL[2].KB/value<7> ;
  wire \XLXI_4/KernelBL[2].KB/value<8> ;
  wire \XLXI_4/KernelBL[2].KB/value<9> ;
  wire \XLXI_4/KernelBL[2].KB/value_add0000<0> ;
  wire \XLXI_4/KernelBL[2].KB/value_add0000<10> ;
  wire \XLXI_4/KernelBL[2].KB/value_add0000<11> ;
  wire \XLXI_4/KernelBL[2].KB/value_add0000<12> ;
  wire \XLXI_4/KernelBL[2].KB/value_add0000<13> ;
  wire \XLXI_4/KernelBL[2].KB/value_add0000<14> ;
  wire \XLXI_4/KernelBL[2].KB/value_add0000<15> ;
  wire \XLXI_4/KernelBL[2].KB/value_add0000<1> ;
  wire \XLXI_4/KernelBL[2].KB/value_add0000<2> ;
  wire \XLXI_4/KernelBL[2].KB/value_add0000<3> ;
  wire \XLXI_4/KernelBL[2].KB/value_add0000<4> ;
  wire \XLXI_4/KernelBL[2].KB/value_add0000<5> ;
  wire \XLXI_4/KernelBL[2].KB/value_add0000<6> ;
  wire \XLXI_4/KernelBL[2].KB/value_add0000<7> ;
  wire \XLXI_4/KernelBL[2].KB/value_add0000<8> ;
  wire \XLXI_4/KernelBL[2].KB/value_add0000<9> ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<0>_417 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<10>_418 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<11>_419 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<12>_420 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<13>_421 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<14>_422 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<1>_423 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<2>_424 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<3>_425 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<4>_426 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<5>_427 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<6>_428 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<7>_429 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<8>_430 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<9>_431 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<0>_432 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<10>_433 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<11>_434 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<12>_435 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<13>_436 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<14>_437 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<15>_438 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<1>_439 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<2>_440 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<3>_441 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<4>_442 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<5>_443 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<6>_444 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<7>_445 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<8>_446 ;
  wire \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<9>_447 ;
  wire \XLXI_4/KernelBL[3].KB/_old_tmp_2<10> ;
  wire \XLXI_4/KernelBL[3].KB/_old_tmp_2<11> ;
  wire \XLXI_4/KernelBL[3].KB/_old_tmp_2<12> ;
  wire \XLXI_4/KernelBL[3].KB/_old_tmp_2<13> ;
  wire \XLXI_4/KernelBL[3].KB/_old_tmp_2<14> ;
  wire \XLXI_4/KernelBL[3].KB/_old_tmp_2<15> ;
  wire \XLXI_4/KernelBL[3].KB/_old_tmp_2<16> ;
  wire \XLXI_4/KernelBL[3].KB/_old_tmp_2<17> ;
  wire \XLXI_4/KernelBL[3].KB/_old_tmp_2<18> ;
  wire \XLXI_4/KernelBL[3].KB/_old_tmp_2<19> ;
  wire \XLXI_4/KernelBL[3].KB/_old_tmp_2<4> ;
  wire \XLXI_4/KernelBL[3].KB/_old_tmp_2<5> ;
  wire \XLXI_4/KernelBL[3].KB/_old_tmp_2<6> ;
  wire \XLXI_4/KernelBL[3].KB/_old_tmp_2<7> ;
  wire \XLXI_4/KernelBL[3].KB/_old_tmp_2<8> ;
  wire \XLXI_4/KernelBL[3].KB/_old_tmp_2<9> ;
  wire \XLXI_4/KernelBL[3].KB/test<0> ;
  wire \XLXI_4/KernelBL[3].KB/test<1> ;
  wire \XLXI_4/KernelBL[3].KB/test<10> ;
  wire \XLXI_4/KernelBL[3].KB/test<11> ;
  wire \XLXI_4/KernelBL[3].KB/test<12> ;
  wire \XLXI_4/KernelBL[3].KB/test<13> ;
  wire \XLXI_4/KernelBL[3].KB/test<14> ;
  wire \XLXI_4/KernelBL[3].KB/test<15> ;
  wire \XLXI_4/KernelBL[3].KB/test<2> ;
  wire \XLXI_4/KernelBL[3].KB/test<3> ;
  wire \XLXI_4/KernelBL[3].KB/test<4> ;
  wire \XLXI_4/KernelBL[3].KB/test<5> ;
  wire \XLXI_4/KernelBL[3].KB/test<6> ;
  wire \XLXI_4/KernelBL[3].KB/test<7> ;
  wire \XLXI_4/KernelBL[3].KB/test<8> ;
  wire \XLXI_4/KernelBL[3].KB/test<9> ;
  wire \XLXI_4/KernelBL[3].KB/value<0> ;
  wire \XLXI_4/KernelBL[3].KB/value<1> ;
  wire \XLXI_4/KernelBL[3].KB/value<10> ;
  wire \XLXI_4/KernelBL[3].KB/value<11> ;
  wire \XLXI_4/KernelBL[3].KB/value<12> ;
  wire \XLXI_4/KernelBL[3].KB/value<13> ;
  wire \XLXI_4/KernelBL[3].KB/value<14> ;
  wire \XLXI_4/KernelBL[3].KB/value<15> ;
  wire \XLXI_4/KernelBL[3].KB/value<2> ;
  wire \XLXI_4/KernelBL[3].KB/value<3> ;
  wire \XLXI_4/KernelBL[3].KB/value<4> ;
  wire \XLXI_4/KernelBL[3].KB/value<5> ;
  wire \XLXI_4/KernelBL[3].KB/value<6> ;
  wire \XLXI_4/KernelBL[3].KB/value<7> ;
  wire \XLXI_4/KernelBL[3].KB/value<8> ;
  wire \XLXI_4/KernelBL[3].KB/value<9> ;
  wire \XLXI_4/KernelBL[3].KB/value_add0000<0> ;
  wire \XLXI_4/KernelBL[3].KB/value_add0000<10> ;
  wire \XLXI_4/KernelBL[3].KB/value_add0000<11> ;
  wire \XLXI_4/KernelBL[3].KB/value_add0000<12> ;
  wire \XLXI_4/KernelBL[3].KB/value_add0000<13> ;
  wire \XLXI_4/KernelBL[3].KB/value_add0000<14> ;
  wire \XLXI_4/KernelBL[3].KB/value_add0000<15> ;
  wire \XLXI_4/KernelBL[3].KB/value_add0000<1> ;
  wire \XLXI_4/KernelBL[3].KB/value_add0000<2> ;
  wire \XLXI_4/KernelBL[3].KB/value_add0000<3> ;
  wire \XLXI_4/KernelBL[3].KB/value_add0000<4> ;
  wire \XLXI_4/KernelBL[3].KB/value_add0000<5> ;
  wire \XLXI_4/KernelBL[3].KB/value_add0000<6> ;
  wire \XLXI_4/KernelBL[3].KB/value_add0000<7> ;
  wire \XLXI_4/KernelBL[3].KB/value_add0000<8> ;
  wire \XLXI_4/KernelBL[3].KB/value_add0000<9> ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<0>_512 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<10>_513 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<11>_514 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<12>_515 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<13>_516 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<14>_517 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<1>_518 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<2>_519 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<3>_520 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<4>_521 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<5>_522 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<6>_523 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<7>_524 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<8>_525 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<9>_526 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<0>_527 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<10>_528 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<11>_529 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<12>_530 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<13>_531 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<14>_532 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<15>_533 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<1>_534 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<2>_535 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<3>_536 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<4>_537 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<5>_538 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<6>_539 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<7>_540 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<8>_541 ;
  wire \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<9>_542 ;
  wire \XLXI_4/KernelBL[4].KB/_old_tmp_2<10> ;
  wire \XLXI_4/KernelBL[4].KB/_old_tmp_2<11> ;
  wire \XLXI_4/KernelBL[4].KB/_old_tmp_2<12> ;
  wire \XLXI_4/KernelBL[4].KB/_old_tmp_2<13> ;
  wire \XLXI_4/KernelBL[4].KB/_old_tmp_2<14> ;
  wire \XLXI_4/KernelBL[4].KB/_old_tmp_2<15> ;
  wire \XLXI_4/KernelBL[4].KB/_old_tmp_2<16> ;
  wire \XLXI_4/KernelBL[4].KB/_old_tmp_2<17> ;
  wire \XLXI_4/KernelBL[4].KB/_old_tmp_2<18> ;
  wire \XLXI_4/KernelBL[4].KB/_old_tmp_2<19> ;
  wire \XLXI_4/KernelBL[4].KB/_old_tmp_2<4> ;
  wire \XLXI_4/KernelBL[4].KB/_old_tmp_2<5> ;
  wire \XLXI_4/KernelBL[4].KB/_old_tmp_2<6> ;
  wire \XLXI_4/KernelBL[4].KB/_old_tmp_2<7> ;
  wire \XLXI_4/KernelBL[4].KB/_old_tmp_2<8> ;
  wire \XLXI_4/KernelBL[4].KB/_old_tmp_2<9> ;
  wire \XLXI_4/KernelBL[4].KB/test<0> ;
  wire \XLXI_4/KernelBL[4].KB/test<1> ;
  wire \XLXI_4/KernelBL[4].KB/test<10> ;
  wire \XLXI_4/KernelBL[4].KB/test<11> ;
  wire \XLXI_4/KernelBL[4].KB/test<12> ;
  wire \XLXI_4/KernelBL[4].KB/test<13> ;
  wire \XLXI_4/KernelBL[4].KB/test<14> ;
  wire \XLXI_4/KernelBL[4].KB/test<15> ;
  wire \XLXI_4/KernelBL[4].KB/test<2> ;
  wire \XLXI_4/KernelBL[4].KB/test<3> ;
  wire \XLXI_4/KernelBL[4].KB/test<4> ;
  wire \XLXI_4/KernelBL[4].KB/test<5> ;
  wire \XLXI_4/KernelBL[4].KB/test<6> ;
  wire \XLXI_4/KernelBL[4].KB/test<7> ;
  wire \XLXI_4/KernelBL[4].KB/test<8> ;
  wire \XLXI_4/KernelBL[4].KB/test<9> ;
  wire \XLXI_4/KernelBL[4].KB/value<0> ;
  wire \XLXI_4/KernelBL[4].KB/value<1> ;
  wire \XLXI_4/KernelBL[4].KB/value<10> ;
  wire \XLXI_4/KernelBL[4].KB/value<11> ;
  wire \XLXI_4/KernelBL[4].KB/value<12> ;
  wire \XLXI_4/KernelBL[4].KB/value<13> ;
  wire \XLXI_4/KernelBL[4].KB/value<14> ;
  wire \XLXI_4/KernelBL[4].KB/value<15> ;
  wire \XLXI_4/KernelBL[4].KB/value<2> ;
  wire \XLXI_4/KernelBL[4].KB/value<3> ;
  wire \XLXI_4/KernelBL[4].KB/value<4> ;
  wire \XLXI_4/KernelBL[4].KB/value<5> ;
  wire \XLXI_4/KernelBL[4].KB/value<6> ;
  wire \XLXI_4/KernelBL[4].KB/value<7> ;
  wire \XLXI_4/KernelBL[4].KB/value<8> ;
  wire \XLXI_4/KernelBL[4].KB/value<9> ;
  wire \XLXI_4/KernelBL[4].KB/value_add0000<0> ;
  wire \XLXI_4/KernelBL[4].KB/value_add0000<10> ;
  wire \XLXI_4/KernelBL[4].KB/value_add0000<11> ;
  wire \XLXI_4/KernelBL[4].KB/value_add0000<12> ;
  wire \XLXI_4/KernelBL[4].KB/value_add0000<13> ;
  wire \XLXI_4/KernelBL[4].KB/value_add0000<14> ;
  wire \XLXI_4/KernelBL[4].KB/value_add0000<15> ;
  wire \XLXI_4/KernelBL[4].KB/value_add0000<1> ;
  wire \XLXI_4/KernelBL[4].KB/value_add0000<2> ;
  wire \XLXI_4/KernelBL[4].KB/value_add0000<3> ;
  wire \XLXI_4/KernelBL[4].KB/value_add0000<4> ;
  wire \XLXI_4/KernelBL[4].KB/value_add0000<5> ;
  wire \XLXI_4/KernelBL[4].KB/value_add0000<6> ;
  wire \XLXI_4/KernelBL[4].KB/value_add0000<7> ;
  wire \XLXI_4/KernelBL[4].KB/value_add0000<8> ;
  wire \XLXI_4/KernelBL[4].KB/value_add0000<9> ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<0>_607 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<10>_608 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<11>_609 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<12>_610 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<13>_611 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<14>_612 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<1>_613 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<2>_614 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<3>_615 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<4>_616 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<5>_617 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<6>_618 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<7>_619 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<8>_620 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<9>_621 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<0>_622 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<10>_623 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<11>_624 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<12>_625 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<13>_626 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<14>_627 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<15>_628 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<1>_629 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<2>_630 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<3>_631 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<4>_632 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<5>_633 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<6>_634 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<7>_635 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<8>_636 ;
  wire \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<9>_637 ;
  wire \XLXI_4/KernelBL[5].KB/_old_tmp_2<10> ;
  wire \XLXI_4/KernelBL[5].KB/_old_tmp_2<11> ;
  wire \XLXI_4/KernelBL[5].KB/_old_tmp_2<12> ;
  wire \XLXI_4/KernelBL[5].KB/_old_tmp_2<13> ;
  wire \XLXI_4/KernelBL[5].KB/_old_tmp_2<14> ;
  wire \XLXI_4/KernelBL[5].KB/_old_tmp_2<15> ;
  wire \XLXI_4/KernelBL[5].KB/_old_tmp_2<16> ;
  wire \XLXI_4/KernelBL[5].KB/_old_tmp_2<17> ;
  wire \XLXI_4/KernelBL[5].KB/_old_tmp_2<18> ;
  wire \XLXI_4/KernelBL[5].KB/_old_tmp_2<19> ;
  wire \XLXI_4/KernelBL[5].KB/_old_tmp_2<4> ;
  wire \XLXI_4/KernelBL[5].KB/_old_tmp_2<5> ;
  wire \XLXI_4/KernelBL[5].KB/_old_tmp_2<6> ;
  wire \XLXI_4/KernelBL[5].KB/_old_tmp_2<7> ;
  wire \XLXI_4/KernelBL[5].KB/_old_tmp_2<8> ;
  wire \XLXI_4/KernelBL[5].KB/_old_tmp_2<9> ;
  wire \XLXI_4/KernelBL[5].KB/test<0> ;
  wire \XLXI_4/KernelBL[5].KB/test<1> ;
  wire \XLXI_4/KernelBL[5].KB/test<10> ;
  wire \XLXI_4/KernelBL[5].KB/test<11> ;
  wire \XLXI_4/KernelBL[5].KB/test<12> ;
  wire \XLXI_4/KernelBL[5].KB/test<13> ;
  wire \XLXI_4/KernelBL[5].KB/test<14> ;
  wire \XLXI_4/KernelBL[5].KB/test<15> ;
  wire \XLXI_4/KernelBL[5].KB/test<2> ;
  wire \XLXI_4/KernelBL[5].KB/test<3> ;
  wire \XLXI_4/KernelBL[5].KB/test<4> ;
  wire \XLXI_4/KernelBL[5].KB/test<5> ;
  wire \XLXI_4/KernelBL[5].KB/test<6> ;
  wire \XLXI_4/KernelBL[5].KB/test<7> ;
  wire \XLXI_4/KernelBL[5].KB/test<8> ;
  wire \XLXI_4/KernelBL[5].KB/test<9> ;
  wire \XLXI_4/KernelBL[5].KB/value<0> ;
  wire \XLXI_4/KernelBL[5].KB/value<1> ;
  wire \XLXI_4/KernelBL[5].KB/value<10> ;
  wire \XLXI_4/KernelBL[5].KB/value<11> ;
  wire \XLXI_4/KernelBL[5].KB/value<12> ;
  wire \XLXI_4/KernelBL[5].KB/value<13> ;
  wire \XLXI_4/KernelBL[5].KB/value<14> ;
  wire \XLXI_4/KernelBL[5].KB/value<15> ;
  wire \XLXI_4/KernelBL[5].KB/value<2> ;
  wire \XLXI_4/KernelBL[5].KB/value<3> ;
  wire \XLXI_4/KernelBL[5].KB/value<4> ;
  wire \XLXI_4/KernelBL[5].KB/value<5> ;
  wire \XLXI_4/KernelBL[5].KB/value<6> ;
  wire \XLXI_4/KernelBL[5].KB/value<7> ;
  wire \XLXI_4/KernelBL[5].KB/value<8> ;
  wire \XLXI_4/KernelBL[5].KB/value<9> ;
  wire \XLXI_4/KernelBL[5].KB/value_add0000<0> ;
  wire \XLXI_4/KernelBL[5].KB/value_add0000<10> ;
  wire \XLXI_4/KernelBL[5].KB/value_add0000<11> ;
  wire \XLXI_4/KernelBL[5].KB/value_add0000<12> ;
  wire \XLXI_4/KernelBL[5].KB/value_add0000<13> ;
  wire \XLXI_4/KernelBL[5].KB/value_add0000<14> ;
  wire \XLXI_4/KernelBL[5].KB/value_add0000<15> ;
  wire \XLXI_4/KernelBL[5].KB/value_add0000<1> ;
  wire \XLXI_4/KernelBL[5].KB/value_add0000<2> ;
  wire \XLXI_4/KernelBL[5].KB/value_add0000<3> ;
  wire \XLXI_4/KernelBL[5].KB/value_add0000<4> ;
  wire \XLXI_4/KernelBL[5].KB/value_add0000<5> ;
  wire \XLXI_4/KernelBL[5].KB/value_add0000<6> ;
  wire \XLXI_4/KernelBL[5].KB/value_add0000<7> ;
  wire \XLXI_4/KernelBL[5].KB/value_add0000<8> ;
  wire \XLXI_4/KernelBL[5].KB/value_add0000<9> ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<0>_702 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<10>_703 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<11>_704 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<12>_705 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<13>_706 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<14>_707 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<1>_708 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<2>_709 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<3>_710 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<4>_711 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<5>_712 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<6>_713 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<7>_714 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<8>_715 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<9>_716 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<0>_717 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<10>_718 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<11>_719 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<12>_720 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<13>_721 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<14>_722 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<15>_723 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<1>_724 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<2>_725 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<3>_726 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<4>_727 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<5>_728 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<6>_729 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<7>_730 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<8>_731 ;
  wire \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<9>_732 ;
  wire \XLXI_4/KernelBL[6].KB/_old_tmp_2<10> ;
  wire \XLXI_4/KernelBL[6].KB/_old_tmp_2<11> ;
  wire \XLXI_4/KernelBL[6].KB/_old_tmp_2<12> ;
  wire \XLXI_4/KernelBL[6].KB/_old_tmp_2<13> ;
  wire \XLXI_4/KernelBL[6].KB/_old_tmp_2<14> ;
  wire \XLXI_4/KernelBL[6].KB/_old_tmp_2<15> ;
  wire \XLXI_4/KernelBL[6].KB/_old_tmp_2<16> ;
  wire \XLXI_4/KernelBL[6].KB/_old_tmp_2<17> ;
  wire \XLXI_4/KernelBL[6].KB/_old_tmp_2<18> ;
  wire \XLXI_4/KernelBL[6].KB/_old_tmp_2<19> ;
  wire \XLXI_4/KernelBL[6].KB/_old_tmp_2<4> ;
  wire \XLXI_4/KernelBL[6].KB/_old_tmp_2<5> ;
  wire \XLXI_4/KernelBL[6].KB/_old_tmp_2<6> ;
  wire \XLXI_4/KernelBL[6].KB/_old_tmp_2<7> ;
  wire \XLXI_4/KernelBL[6].KB/_old_tmp_2<8> ;
  wire \XLXI_4/KernelBL[6].KB/_old_tmp_2<9> ;
  wire \XLXI_4/KernelBL[6].KB/test<0> ;
  wire \XLXI_4/KernelBL[6].KB/test<1> ;
  wire \XLXI_4/KernelBL[6].KB/test<10> ;
  wire \XLXI_4/KernelBL[6].KB/test<11> ;
  wire \XLXI_4/KernelBL[6].KB/test<12> ;
  wire \XLXI_4/KernelBL[6].KB/test<13> ;
  wire \XLXI_4/KernelBL[6].KB/test<14> ;
  wire \XLXI_4/KernelBL[6].KB/test<15> ;
  wire \XLXI_4/KernelBL[6].KB/test<2> ;
  wire \XLXI_4/KernelBL[6].KB/test<3> ;
  wire \XLXI_4/KernelBL[6].KB/test<4> ;
  wire \XLXI_4/KernelBL[6].KB/test<5> ;
  wire \XLXI_4/KernelBL[6].KB/test<6> ;
  wire \XLXI_4/KernelBL[6].KB/test<7> ;
  wire \XLXI_4/KernelBL[6].KB/test<8> ;
  wire \XLXI_4/KernelBL[6].KB/test<9> ;
  wire \XLXI_4/KernelBL[6].KB/value<0> ;
  wire \XLXI_4/KernelBL[6].KB/value<1> ;
  wire \XLXI_4/KernelBL[6].KB/value<10> ;
  wire \XLXI_4/KernelBL[6].KB/value<11> ;
  wire \XLXI_4/KernelBL[6].KB/value<12> ;
  wire \XLXI_4/KernelBL[6].KB/value<13> ;
  wire \XLXI_4/KernelBL[6].KB/value<14> ;
  wire \XLXI_4/KernelBL[6].KB/value<15> ;
  wire \XLXI_4/KernelBL[6].KB/value<2> ;
  wire \XLXI_4/KernelBL[6].KB/value<3> ;
  wire \XLXI_4/KernelBL[6].KB/value<4> ;
  wire \XLXI_4/KernelBL[6].KB/value<5> ;
  wire \XLXI_4/KernelBL[6].KB/value<6> ;
  wire \XLXI_4/KernelBL[6].KB/value<7> ;
  wire \XLXI_4/KernelBL[6].KB/value<8> ;
  wire \XLXI_4/KernelBL[6].KB/value<9> ;
  wire \XLXI_4/KernelBL[6].KB/value_add0000<0> ;
  wire \XLXI_4/KernelBL[6].KB/value_add0000<10> ;
  wire \XLXI_4/KernelBL[6].KB/value_add0000<11> ;
  wire \XLXI_4/KernelBL[6].KB/value_add0000<12> ;
  wire \XLXI_4/KernelBL[6].KB/value_add0000<13> ;
  wire \XLXI_4/KernelBL[6].KB/value_add0000<14> ;
  wire \XLXI_4/KernelBL[6].KB/value_add0000<15> ;
  wire \XLXI_4/KernelBL[6].KB/value_add0000<1> ;
  wire \XLXI_4/KernelBL[6].KB/value_add0000<2> ;
  wire \XLXI_4/KernelBL[6].KB/value_add0000<3> ;
  wire \XLXI_4/KernelBL[6].KB/value_add0000<4> ;
  wire \XLXI_4/KernelBL[6].KB/value_add0000<5> ;
  wire \XLXI_4/KernelBL[6].KB/value_add0000<6> ;
  wire \XLXI_4/KernelBL[6].KB/value_add0000<7> ;
  wire \XLXI_4/KernelBL[6].KB/value_add0000<8> ;
  wire \XLXI_4/KernelBL[6].KB/value_add0000<9> ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<0>_797 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<10>_798 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<11>_799 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<12>_800 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<13>_801 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<14>_802 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<1>_803 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<2>_804 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<3>_805 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<4>_806 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<5>_807 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<6>_808 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<7>_809 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<8>_810 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<9>_811 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<0>_812 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<10>_813 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<11>_814 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<12>_815 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<13>_816 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<14>_817 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<15>_818 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<1>_819 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<2>_820 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<3>_821 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<4>_822 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<5>_823 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<6>_824 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<7>_825 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<8>_826 ;
  wire \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<9>_827 ;
  wire \XLXI_4/KernelBL[7].KB/_old_tmp_2<10> ;
  wire \XLXI_4/KernelBL[7].KB/_old_tmp_2<11> ;
  wire \XLXI_4/KernelBL[7].KB/_old_tmp_2<12> ;
  wire \XLXI_4/KernelBL[7].KB/_old_tmp_2<13> ;
  wire \XLXI_4/KernelBL[7].KB/_old_tmp_2<14> ;
  wire \XLXI_4/KernelBL[7].KB/_old_tmp_2<15> ;
  wire \XLXI_4/KernelBL[7].KB/_old_tmp_2<16> ;
  wire \XLXI_4/KernelBL[7].KB/_old_tmp_2<17> ;
  wire \XLXI_4/KernelBL[7].KB/_old_tmp_2<18> ;
  wire \XLXI_4/KernelBL[7].KB/_old_tmp_2<19> ;
  wire \XLXI_4/KernelBL[7].KB/_old_tmp_2<4> ;
  wire \XLXI_4/KernelBL[7].KB/_old_tmp_2<5> ;
  wire \XLXI_4/KernelBL[7].KB/_old_tmp_2<6> ;
  wire \XLXI_4/KernelBL[7].KB/_old_tmp_2<7> ;
  wire \XLXI_4/KernelBL[7].KB/_old_tmp_2<8> ;
  wire \XLXI_4/KernelBL[7].KB/_old_tmp_2<9> ;
  wire \XLXI_4/KernelBL[7].KB/test<0> ;
  wire \XLXI_4/KernelBL[7].KB/test<1> ;
  wire \XLXI_4/KernelBL[7].KB/test<10> ;
  wire \XLXI_4/KernelBL[7].KB/test<11> ;
  wire \XLXI_4/KernelBL[7].KB/test<12> ;
  wire \XLXI_4/KernelBL[7].KB/test<13> ;
  wire \XLXI_4/KernelBL[7].KB/test<14> ;
  wire \XLXI_4/KernelBL[7].KB/test<15> ;
  wire \XLXI_4/KernelBL[7].KB/test<2> ;
  wire \XLXI_4/KernelBL[7].KB/test<3> ;
  wire \XLXI_4/KernelBL[7].KB/test<4> ;
  wire \XLXI_4/KernelBL[7].KB/test<5> ;
  wire \XLXI_4/KernelBL[7].KB/test<6> ;
  wire \XLXI_4/KernelBL[7].KB/test<7> ;
  wire \XLXI_4/KernelBL[7].KB/test<8> ;
  wire \XLXI_4/KernelBL[7].KB/test<9> ;
  wire \XLXI_4/KernelBL[7].KB/value<0> ;
  wire \XLXI_4/KernelBL[7].KB/value<1> ;
  wire \XLXI_4/KernelBL[7].KB/value<10> ;
  wire \XLXI_4/KernelBL[7].KB/value<11> ;
  wire \XLXI_4/KernelBL[7].KB/value<12> ;
  wire \XLXI_4/KernelBL[7].KB/value<13> ;
  wire \XLXI_4/KernelBL[7].KB/value<14> ;
  wire \XLXI_4/KernelBL[7].KB/value<15> ;
  wire \XLXI_4/KernelBL[7].KB/value<2> ;
  wire \XLXI_4/KernelBL[7].KB/value<3> ;
  wire \XLXI_4/KernelBL[7].KB/value<4> ;
  wire \XLXI_4/KernelBL[7].KB/value<5> ;
  wire \XLXI_4/KernelBL[7].KB/value<6> ;
  wire \XLXI_4/KernelBL[7].KB/value<7> ;
  wire \XLXI_4/KernelBL[7].KB/value<8> ;
  wire \XLXI_4/KernelBL[7].KB/value<9> ;
  wire \XLXI_4/KernelBL[7].KB/value_add0000<0> ;
  wire \XLXI_4/KernelBL[7].KB/value_add0000<10> ;
  wire \XLXI_4/KernelBL[7].KB/value_add0000<11> ;
  wire \XLXI_4/KernelBL[7].KB/value_add0000<12> ;
  wire \XLXI_4/KernelBL[7].KB/value_add0000<13> ;
  wire \XLXI_4/KernelBL[7].KB/value_add0000<14> ;
  wire \XLXI_4/KernelBL[7].KB/value_add0000<15> ;
  wire \XLXI_4/KernelBL[7].KB/value_add0000<1> ;
  wire \XLXI_4/KernelBL[7].KB/value_add0000<2> ;
  wire \XLXI_4/KernelBL[7].KB/value_add0000<3> ;
  wire \XLXI_4/KernelBL[7].KB/value_add0000<4> ;
  wire \XLXI_4/KernelBL[7].KB/value_add0000<5> ;
  wire \XLXI_4/KernelBL[7].KB/value_add0000<6> ;
  wire \XLXI_4/KernelBL[7].KB/value_add0000<7> ;
  wire \XLXI_4/KernelBL[7].KB/value_add0000<8> ;
  wire \XLXI_4/KernelBL[7].KB/value_add0000<9> ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<0>_892 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<10>_893 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<11>_894 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<12>_895 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<13>_896 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<14>_897 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<1>_898 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<2>_899 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<3>_900 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<4>_901 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<5>_902 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<6>_903 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<7>_904 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<8>_905 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<9>_906 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<0>_907 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<10>_908 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<11>_909 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<12>_910 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<13>_911 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<14>_912 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<15>_913 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<1>_914 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<2>_915 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<3>_916 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<4>_917 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<5>_918 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<6>_919 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<7>_920 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<8>_921 ;
  wire \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<9>_922 ;
  wire \XLXI_4/KernelBL[8].KB/_old_tmp_2<10> ;
  wire \XLXI_4/KernelBL[8].KB/_old_tmp_2<11> ;
  wire \XLXI_4/KernelBL[8].KB/_old_tmp_2<12> ;
  wire \XLXI_4/KernelBL[8].KB/_old_tmp_2<13> ;
  wire \XLXI_4/KernelBL[8].KB/_old_tmp_2<14> ;
  wire \XLXI_4/KernelBL[8].KB/_old_tmp_2<15> ;
  wire \XLXI_4/KernelBL[8].KB/_old_tmp_2<16> ;
  wire \XLXI_4/KernelBL[8].KB/_old_tmp_2<17> ;
  wire \XLXI_4/KernelBL[8].KB/_old_tmp_2<18> ;
  wire \XLXI_4/KernelBL[8].KB/_old_tmp_2<19> ;
  wire \XLXI_4/KernelBL[8].KB/_old_tmp_2<4> ;
  wire \XLXI_4/KernelBL[8].KB/_old_tmp_2<5> ;
  wire \XLXI_4/KernelBL[8].KB/_old_tmp_2<6> ;
  wire \XLXI_4/KernelBL[8].KB/_old_tmp_2<7> ;
  wire \XLXI_4/KernelBL[8].KB/_old_tmp_2<8> ;
  wire \XLXI_4/KernelBL[8].KB/_old_tmp_2<9> ;
  wire \XLXI_4/KernelBL[8].KB/test<0> ;
  wire \XLXI_4/KernelBL[8].KB/test<1> ;
  wire \XLXI_4/KernelBL[8].KB/test<10> ;
  wire \XLXI_4/KernelBL[8].KB/test<11> ;
  wire \XLXI_4/KernelBL[8].KB/test<12> ;
  wire \XLXI_4/KernelBL[8].KB/test<13> ;
  wire \XLXI_4/KernelBL[8].KB/test<14> ;
  wire \XLXI_4/KernelBL[8].KB/test<15> ;
  wire \XLXI_4/KernelBL[8].KB/test<2> ;
  wire \XLXI_4/KernelBL[8].KB/test<3> ;
  wire \XLXI_4/KernelBL[8].KB/test<4> ;
  wire \XLXI_4/KernelBL[8].KB/test<5> ;
  wire \XLXI_4/KernelBL[8].KB/test<6> ;
  wire \XLXI_4/KernelBL[8].KB/test<7> ;
  wire \XLXI_4/KernelBL[8].KB/test<8> ;
  wire \XLXI_4/KernelBL[8].KB/test<9> ;
  wire \XLXI_4/KernelBL[8].KB/value<0> ;
  wire \XLXI_4/KernelBL[8].KB/value<1> ;
  wire \XLXI_4/KernelBL[8].KB/value<10> ;
  wire \XLXI_4/KernelBL[8].KB/value<11> ;
  wire \XLXI_4/KernelBL[8].KB/value<12> ;
  wire \XLXI_4/KernelBL[8].KB/value<13> ;
  wire \XLXI_4/KernelBL[8].KB/value<14> ;
  wire \XLXI_4/KernelBL[8].KB/value<15> ;
  wire \XLXI_4/KernelBL[8].KB/value<2> ;
  wire \XLXI_4/KernelBL[8].KB/value<3> ;
  wire \XLXI_4/KernelBL[8].KB/value<4> ;
  wire \XLXI_4/KernelBL[8].KB/value<5> ;
  wire \XLXI_4/KernelBL[8].KB/value<6> ;
  wire \XLXI_4/KernelBL[8].KB/value<7> ;
  wire \XLXI_4/KernelBL[8].KB/value<8> ;
  wire \XLXI_4/KernelBL[8].KB/value<9> ;
  wire \XLXI_4/KernelBL[8].KB/value_add0000<0> ;
  wire \XLXI_4/KernelBL[8].KB/value_add0000<10> ;
  wire \XLXI_4/KernelBL[8].KB/value_add0000<11> ;
  wire \XLXI_4/KernelBL[8].KB/value_add0000<12> ;
  wire \XLXI_4/KernelBL[8].KB/value_add0000<13> ;
  wire \XLXI_4/KernelBL[8].KB/value_add0000<14> ;
  wire \XLXI_4/KernelBL[8].KB/value_add0000<15> ;
  wire \XLXI_4/KernelBL[8].KB/value_add0000<1> ;
  wire \XLXI_4/KernelBL[8].KB/value_add0000<2> ;
  wire \XLXI_4/KernelBL[8].KB/value_add0000<3> ;
  wire \XLXI_4/KernelBL[8].KB/value_add0000<4> ;
  wire \XLXI_4/KernelBL[8].KB/value_add0000<5> ;
  wire \XLXI_4/KernelBL[8].KB/value_add0000<6> ;
  wire \XLXI_4/KernelBL[8].KB/value_add0000<7> ;
  wire \XLXI_4/KernelBL[8].KB/value_add0000<8> ;
  wire \XLXI_4/KernelBL[8].KB/value_add0000<9> ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<0>_987 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<10>_988 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<11>_989 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<12>_990 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<13>_991 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<14>_992 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<1>_993 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<2>_994 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<3>_995 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<4>_996 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<5>_997 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<6>_998 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<7>_999 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<8>_1000 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<9>_1001 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<0>_1002 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<10>_1003 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<11>_1004 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<12>_1005 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<13>_1006 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<14>_1007 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<15>_1008 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<1>_1009 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<2>_1010 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<3>_1011 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<4>_1012 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<5>_1013 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<6>_1014 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<7>_1015 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<8>_1016 ;
  wire \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<9>_1017 ;
  wire \XLXI_4/KernelBL[9].KB/_old_tmp_2<10> ;
  wire \XLXI_4/KernelBL[9].KB/_old_tmp_2<11> ;
  wire \XLXI_4/KernelBL[9].KB/_old_tmp_2<12> ;
  wire \XLXI_4/KernelBL[9].KB/_old_tmp_2<13> ;
  wire \XLXI_4/KernelBL[9].KB/_old_tmp_2<14> ;
  wire \XLXI_4/KernelBL[9].KB/_old_tmp_2<15> ;
  wire \XLXI_4/KernelBL[9].KB/_old_tmp_2<16> ;
  wire \XLXI_4/KernelBL[9].KB/_old_tmp_2<17> ;
  wire \XLXI_4/KernelBL[9].KB/_old_tmp_2<18> ;
  wire \XLXI_4/KernelBL[9].KB/_old_tmp_2<19> ;
  wire \XLXI_4/KernelBL[9].KB/_old_tmp_2<4> ;
  wire \XLXI_4/KernelBL[9].KB/_old_tmp_2<5> ;
  wire \XLXI_4/KernelBL[9].KB/_old_tmp_2<6> ;
  wire \XLXI_4/KernelBL[9].KB/_old_tmp_2<7> ;
  wire \XLXI_4/KernelBL[9].KB/_old_tmp_2<8> ;
  wire \XLXI_4/KernelBL[9].KB/_old_tmp_2<9> ;
  wire \XLXI_4/KernelBL[9].KB/test<0> ;
  wire \XLXI_4/KernelBL[9].KB/test<1> ;
  wire \XLXI_4/KernelBL[9].KB/test<10> ;
  wire \XLXI_4/KernelBL[9].KB/test<11> ;
  wire \XLXI_4/KernelBL[9].KB/test<12> ;
  wire \XLXI_4/KernelBL[9].KB/test<13> ;
  wire \XLXI_4/KernelBL[9].KB/test<14> ;
  wire \XLXI_4/KernelBL[9].KB/test<15> ;
  wire \XLXI_4/KernelBL[9].KB/test<2> ;
  wire \XLXI_4/KernelBL[9].KB/test<3> ;
  wire \XLXI_4/KernelBL[9].KB/test<4> ;
  wire \XLXI_4/KernelBL[9].KB/test<5> ;
  wire \XLXI_4/KernelBL[9].KB/test<6> ;
  wire \XLXI_4/KernelBL[9].KB/test<7> ;
  wire \XLXI_4/KernelBL[9].KB/test<8> ;
  wire \XLXI_4/KernelBL[9].KB/test<9> ;
  wire \XLXI_4/KernelBL[9].KB/value<0> ;
  wire \XLXI_4/KernelBL[9].KB/value<1> ;
  wire \XLXI_4/KernelBL[9].KB/value<10> ;
  wire \XLXI_4/KernelBL[9].KB/value<11> ;
  wire \XLXI_4/KernelBL[9].KB/value<12> ;
  wire \XLXI_4/KernelBL[9].KB/value<13> ;
  wire \XLXI_4/KernelBL[9].KB/value<14> ;
  wire \XLXI_4/KernelBL[9].KB/value<15> ;
  wire \XLXI_4/KernelBL[9].KB/value<2> ;
  wire \XLXI_4/KernelBL[9].KB/value<3> ;
  wire \XLXI_4/KernelBL[9].KB/value<4> ;
  wire \XLXI_4/KernelBL[9].KB/value<5> ;
  wire \XLXI_4/KernelBL[9].KB/value<6> ;
  wire \XLXI_4/KernelBL[9].KB/value<7> ;
  wire \XLXI_4/KernelBL[9].KB/value<8> ;
  wire \XLXI_4/KernelBL[9].KB/value<9> ;
  wire \XLXI_4/KernelBL[9].KB/value_add0000<0> ;
  wire \XLXI_4/KernelBL[9].KB/value_add0000<10> ;
  wire \XLXI_4/KernelBL[9].KB/value_add0000<11> ;
  wire \XLXI_4/KernelBL[9].KB/value_add0000<12> ;
  wire \XLXI_4/KernelBL[9].KB/value_add0000<13> ;
  wire \XLXI_4/KernelBL[9].KB/value_add0000<14> ;
  wire \XLXI_4/KernelBL[9].KB/value_add0000<15> ;
  wire \XLXI_4/KernelBL[9].KB/value_add0000<1> ;
  wire \XLXI_4/KernelBL[9].KB/value_add0000<2> ;
  wire \XLXI_4/KernelBL[9].KB/value_add0000<3> ;
  wire \XLXI_4/KernelBL[9].KB/value_add0000<4> ;
  wire \XLXI_4/KernelBL[9].KB/value_add0000<5> ;
  wire \XLXI_4/KernelBL[9].KB/value_add0000<6> ;
  wire \XLXI_4/KernelBL[9].KB/value_add0000<7> ;
  wire \XLXI_4/KernelBL[9].KB/value_add0000<8> ;
  wire \XLXI_4/KernelBL[9].KB/value_add0000<9> ;
  wire \XLXI_5/Mcompar_Numb_cmp_gt0000_cy<1>_rt_1096 ;
  wire \XLXI_5/Mcompar_Numb_cmp_gt0000_cy<4>_rt_1100 ;
  wire \XLXI_5/Mcompar_Numb_cmp_gt0000_lut[0] ;
  wire \XLXI_5/Mcompar_Numb_cmp_gt0000_lut[2] ;
  wire \XLXI_5/Mcompar_Numb_cmp_gt0000_lut[3] ;
  wire \XLXI_5/Mcompar_Numb_cmp_gt0000_lut[5] ;
  wire \XLXI_5/Mcount_Numb_cy<10>_rt_1108 ;
  wire \XLXI_5/Mcount_Numb_cy<1>_rt_1110 ;
  wire \XLXI_5/Mcount_Numb_cy<2>_rt_1112 ;
  wire \XLXI_5/Mcount_Numb_cy<3>_rt_1114 ;
  wire \XLXI_5/Mcount_Numb_cy<4>_rt_1116 ;
  wire \XLXI_5/Mcount_Numb_cy<5>_rt_1118 ;
  wire \XLXI_5/Mcount_Numb_cy<6>_rt_1120 ;
  wire \XLXI_5/Mcount_Numb_cy<7>_rt_1122 ;
  wire \XLXI_5/Mcount_Numb_cy<8>_rt_1124 ;
  wire \XLXI_5/Mcount_Numb_cy<9>_rt_1126 ;
  wire \XLXI_5/Mcount_Numb_xor<11>_rt_1128 ;
  wire \XLXI_5/Numb_cmp_gt0000 ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED ;
  wire \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED ;
  wire [11 : 0] Result;
  wire [6 : 0] \XLXI_3/Mcompar_LED3_cmp_gt0000_cy ;
  wire [5 : 0] \XLXI_3/Mcompar_LED3_cmp_gt0000_lut ;
  wire [11 : 0] \XLXI_4/dataOut ;
  wire [5 : 0] \XLXI_5/Mcompar_Numb_cmp_gt0000_cy ;
  wire [10 : 0] \XLXI_5/Mcount_Numb_cy ;
  wire [0 : 0] \XLXI_5/Mcount_Numb_lut ;
  wire [11 : 0] \XLXI_5/Numb ;
  GND   XST_GND (
    .G(N0)
  );
  VCC   XST_VCC (
    .P(N1)
  );
  FDR #(
    .INIT ( 1'b1 ))
  \XLXI_5/Numb_0  (
    .C(CLK_inv),
    .D(Result[0]),
    .R(\XLXI_5/Numb_cmp_gt0000 ),
    .Q(\XLXI_5/Numb [0])
  );
  FDR #(
    .INIT ( 1'b0 ))
  \XLXI_5/Numb_1  (
    .C(CLK_inv),
    .D(Result[1]),
    .R(\XLXI_5/Numb_cmp_gt0000 ),
    .Q(\XLXI_5/Numb [1])
  );
  FDR #(
    .INIT ( 1'b0 ))
  \XLXI_5/Numb_2  (
    .C(CLK_inv),
    .D(Result[2]),
    .R(\XLXI_5/Numb_cmp_gt0000 ),
    .Q(\XLXI_5/Numb [2])
  );
  FDR #(
    .INIT ( 1'b0 ))
  \XLXI_5/Numb_3  (
    .C(CLK_inv),
    .D(Result[3]),
    .R(\XLXI_5/Numb_cmp_gt0000 ),
    .Q(\XLXI_5/Numb [3])
  );
  FDR #(
    .INIT ( 1'b0 ))
  \XLXI_5/Numb_4  (
    .C(CLK_inv),
    .D(Result[4]),
    .R(\XLXI_5/Numb_cmp_gt0000 ),
    .Q(\XLXI_5/Numb [4])
  );
  FDR #(
    .INIT ( 1'b0 ))
  \XLXI_5/Numb_5  (
    .C(CLK_inv),
    .D(Result[5]),
    .R(\XLXI_5/Numb_cmp_gt0000 ),
    .Q(\XLXI_5/Numb [5])
  );
  FDR #(
    .INIT ( 1'b0 ))
  \XLXI_5/Numb_6  (
    .C(CLK_inv),
    .D(Result[6]),
    .R(\XLXI_5/Numb_cmp_gt0000 ),
    .Q(\XLXI_5/Numb [6])
  );
  FDR #(
    .INIT ( 1'b0 ))
  \XLXI_5/Numb_7  (
    .C(CLK_inv),
    .D(Result[7]),
    .R(\XLXI_5/Numb_cmp_gt0000 ),
    .Q(\XLXI_5/Numb [7])
  );
  FDR #(
    .INIT ( 1'b0 ))
  \XLXI_5/Numb_8  (
    .C(CLK_inv),
    .D(Result[8]),
    .R(\XLXI_5/Numb_cmp_gt0000 ),
    .Q(\XLXI_5/Numb [8])
  );
  FDR #(
    .INIT ( 1'b0 ))
  \XLXI_5/Numb_9  (
    .C(CLK_inv),
    .D(Result[9]),
    .R(\XLXI_5/Numb_cmp_gt0000 ),
    .Q(\XLXI_5/Numb [9])
  );
  FDR #(
    .INIT ( 1'b0 ))
  \XLXI_5/Numb_10  (
    .C(CLK_inv),
    .D(Result[10]),
    .R(\XLXI_5/Numb_cmp_gt0000 ),
    .Q(\XLXI_5/Numb [10])
  );
  FDR #(
    .INIT ( 1'b0 ))
  \XLXI_5/Numb_11  (
    .C(CLK_inv),
    .D(Result[11]),
    .R(\XLXI_5/Numb_cmp_gt0000 ),
    .Q(\XLXI_5/Numb [11])
  );
  LUT4 #(
    .INIT ( 16'h0001 ))
  \XLXI_5/Mcompar_Numb_cmp_gt0000_lut<0>  (
    .I0(\XLXI_5/Numb [0]),
    .I1(\XLXI_5/Numb [1]),
    .I2(\XLXI_5/Numb [2]),
    .I3(\XLXI_5/Numb [3]),
    .O(\XLXI_5/Mcompar_Numb_cmp_gt0000_lut[0] )
  );
  MUXCY   \XLXI_5/Mcompar_Numb_cmp_gt0000_cy<0>  (
    .CI(N1),
    .DI(N0),
    .S(\XLXI_5/Mcompar_Numb_cmp_gt0000_lut[0] ),
    .O(\XLXI_5/Mcompar_Numb_cmp_gt0000_cy [0])
  );
  MUXCY   \XLXI_5/Mcompar_Numb_cmp_gt0000_cy<1>  (
    .CI(\XLXI_5/Mcompar_Numb_cmp_gt0000_cy [0]),
    .DI(N1),
    .S(\XLXI_5/Mcompar_Numb_cmp_gt0000_cy<1>_rt_1096 ),
    .O(\XLXI_5/Mcompar_Numb_cmp_gt0000_cy [1])
  );
  MUXCY   \XLXI_5/Mcompar_Numb_cmp_gt0000_cy<2>  (
    .CI(\XLXI_5/Mcompar_Numb_cmp_gt0000_cy [1]),
    .DI(N0),
    .S(\XLXI_5/Mcompar_Numb_cmp_gt0000_lut[2] ),
    .O(\XLXI_5/Mcompar_Numb_cmp_gt0000_cy [2])
  );
  LUT4 #(
    .INIT ( 16'h8000 ))
  \XLXI_5/Mcompar_Numb_cmp_gt0000_lut<3>  (
    .I0(\XLXI_5/Numb [6]),
    .I1(\XLXI_5/Numb [7]),
    .I2(\XLXI_5/Numb [8]),
    .I3(\XLXI_5/Numb [9]),
    .O(\XLXI_5/Mcompar_Numb_cmp_gt0000_lut[3] )
  );
  MUXCY   \XLXI_5/Mcompar_Numb_cmp_gt0000_cy<3>  (
    .CI(\XLXI_5/Mcompar_Numb_cmp_gt0000_cy [2]),
    .DI(N1),
    .S(\XLXI_5/Mcompar_Numb_cmp_gt0000_lut[3] ),
    .O(\XLXI_5/Mcompar_Numb_cmp_gt0000_cy [3])
  );
  MUXCY   \XLXI_5/Mcompar_Numb_cmp_gt0000_cy<4>  (
    .CI(\XLXI_5/Mcompar_Numb_cmp_gt0000_cy [3]),
    .DI(N1),
    .S(\XLXI_5/Mcompar_Numb_cmp_gt0000_cy<4>_rt_1100 ),
    .O(\XLXI_5/Mcompar_Numb_cmp_gt0000_cy [4])
  );
  MUXCY   \XLXI_5/Mcompar_Numb_cmp_gt0000_cy<5>  (
    .CI(\XLXI_5/Mcompar_Numb_cmp_gt0000_cy [4]),
    .DI(N0),
    .S(\XLXI_5/Mcompar_Numb_cmp_gt0000_lut[5] ),
    .O(\XLXI_5/Mcompar_Numb_cmp_gt0000_cy [5])
  );
  MUXCY   \XLXI_5/Mcount_Numb_cy<0>  (
    .CI(N0),
    .DI(N1),
    .S(\XLXI_5/Mcount_Numb_lut [0]),
    .O(\XLXI_5/Mcount_Numb_cy [0])
  );
  XORCY   \XLXI_5/Mcount_Numb_xor<0>  (
    .CI(N0),
    .LI(\XLXI_5/Mcount_Numb_lut [0]),
    .O(Result[0])
  );
  MUXCY   \XLXI_5/Mcount_Numb_cy<1>  (
    .CI(\XLXI_5/Mcount_Numb_cy [0]),
    .DI(N0),
    .S(\XLXI_5/Mcount_Numb_cy<1>_rt_1110 ),
    .O(\XLXI_5/Mcount_Numb_cy [1])
  );
  XORCY   \XLXI_5/Mcount_Numb_xor<1>  (
    .CI(\XLXI_5/Mcount_Numb_cy [0]),
    .LI(\XLXI_5/Mcount_Numb_cy<1>_rt_1110 ),
    .O(Result[1])
  );
  MUXCY   \XLXI_5/Mcount_Numb_cy<2>  (
    .CI(\XLXI_5/Mcount_Numb_cy [1]),
    .DI(N0),
    .S(\XLXI_5/Mcount_Numb_cy<2>_rt_1112 ),
    .O(\XLXI_5/Mcount_Numb_cy [2])
  );
  XORCY   \XLXI_5/Mcount_Numb_xor<2>  (
    .CI(\XLXI_5/Mcount_Numb_cy [1]),
    .LI(\XLXI_5/Mcount_Numb_cy<2>_rt_1112 ),
    .O(Result[2])
  );
  MUXCY   \XLXI_5/Mcount_Numb_cy<3>  (
    .CI(\XLXI_5/Mcount_Numb_cy [2]),
    .DI(N0),
    .S(\XLXI_5/Mcount_Numb_cy<3>_rt_1114 ),
    .O(\XLXI_5/Mcount_Numb_cy [3])
  );
  XORCY   \XLXI_5/Mcount_Numb_xor<3>  (
    .CI(\XLXI_5/Mcount_Numb_cy [2]),
    .LI(\XLXI_5/Mcount_Numb_cy<3>_rt_1114 ),
    .O(Result[3])
  );
  MUXCY   \XLXI_5/Mcount_Numb_cy<4>  (
    .CI(\XLXI_5/Mcount_Numb_cy [3]),
    .DI(N0),
    .S(\XLXI_5/Mcount_Numb_cy<4>_rt_1116 ),
    .O(\XLXI_5/Mcount_Numb_cy [4])
  );
  XORCY   \XLXI_5/Mcount_Numb_xor<4>  (
    .CI(\XLXI_5/Mcount_Numb_cy [3]),
    .LI(\XLXI_5/Mcount_Numb_cy<4>_rt_1116 ),
    .O(Result[4])
  );
  MUXCY   \XLXI_5/Mcount_Numb_cy<5>  (
    .CI(\XLXI_5/Mcount_Numb_cy [4]),
    .DI(N0),
    .S(\XLXI_5/Mcount_Numb_cy<5>_rt_1118 ),
    .O(\XLXI_5/Mcount_Numb_cy [5])
  );
  XORCY   \XLXI_5/Mcount_Numb_xor<5>  (
    .CI(\XLXI_5/Mcount_Numb_cy [4]),
    .LI(\XLXI_5/Mcount_Numb_cy<5>_rt_1118 ),
    .O(Result[5])
  );
  MUXCY   \XLXI_5/Mcount_Numb_cy<6>  (
    .CI(\XLXI_5/Mcount_Numb_cy [5]),
    .DI(N0),
    .S(\XLXI_5/Mcount_Numb_cy<6>_rt_1120 ),
    .O(\XLXI_5/Mcount_Numb_cy [6])
  );
  XORCY   \XLXI_5/Mcount_Numb_xor<6>  (
    .CI(\XLXI_5/Mcount_Numb_cy [5]),
    .LI(\XLXI_5/Mcount_Numb_cy<6>_rt_1120 ),
    .O(Result[6])
  );
  MUXCY   \XLXI_5/Mcount_Numb_cy<7>  (
    .CI(\XLXI_5/Mcount_Numb_cy [6]),
    .DI(N0),
    .S(\XLXI_5/Mcount_Numb_cy<7>_rt_1122 ),
    .O(\XLXI_5/Mcount_Numb_cy [7])
  );
  XORCY   \XLXI_5/Mcount_Numb_xor<7>  (
    .CI(\XLXI_5/Mcount_Numb_cy [6]),
    .LI(\XLXI_5/Mcount_Numb_cy<7>_rt_1122 ),
    .O(Result[7])
  );
  MUXCY   \XLXI_5/Mcount_Numb_cy<8>  (
    .CI(\XLXI_5/Mcount_Numb_cy [7]),
    .DI(N0),
    .S(\XLXI_5/Mcount_Numb_cy<8>_rt_1124 ),
    .O(\XLXI_5/Mcount_Numb_cy [8])
  );
  XORCY   \XLXI_5/Mcount_Numb_xor<8>  (
    .CI(\XLXI_5/Mcount_Numb_cy [7]),
    .LI(\XLXI_5/Mcount_Numb_cy<8>_rt_1124 ),
    .O(Result[8])
  );
  MUXCY   \XLXI_5/Mcount_Numb_cy<9>  (
    .CI(\XLXI_5/Mcount_Numb_cy [8]),
    .DI(N0),
    .S(\XLXI_5/Mcount_Numb_cy<9>_rt_1126 ),
    .O(\XLXI_5/Mcount_Numb_cy [9])
  );
  XORCY   \XLXI_5/Mcount_Numb_xor<9>  (
    .CI(\XLXI_5/Mcount_Numb_cy [8]),
    .LI(\XLXI_5/Mcount_Numb_cy<9>_rt_1126 ),
    .O(Result[9])
  );
  MUXCY   \XLXI_5/Mcount_Numb_cy<10>  (
    .CI(\XLXI_5/Mcount_Numb_cy [9]),
    .DI(N0),
    .S(\XLXI_5/Mcount_Numb_cy<10>_rt_1108 ),
    .O(\XLXI_5/Mcount_Numb_cy [10])
  );
  XORCY   \XLXI_5/Mcount_Numb_xor<10>  (
    .CI(\XLXI_5/Mcount_Numb_cy [9]),
    .LI(\XLXI_5/Mcount_Numb_cy<10>_rt_1108 ),
    .O(Result[10])
  );
  XORCY   \XLXI_5/Mcount_Numb_xor<11>  (
    .CI(\XLXI_5/Mcount_Numb_cy [10]),
    .LI(\XLXI_5/Mcount_Numb_xor<11>_rt_1128 ),
    .O(Result[11])
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<6>  (
    .CI(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<5>2 ),
    .DI(N1),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<6>_rt_47 ),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy [6])
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<5>_1  (
    .CI(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<4>2 ),
    .DI(N0),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut<5>1_61 ),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<5>2 )
  );
  LUT2 #(
    .INIT ( 4'h1 ))
  \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<5>1  (
    .I0(\XLXI_4/dataOut [9]),
    .I1(\XLXI_4/dataOut [10]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut<5>1_61 )
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<4>_1  (
    .CI(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<3>2 ),
    .DI(N1),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<4>_1_rt_41 ),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<4>2 )
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<3>_1  (
    .CI(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<2>2 ),
    .DI(N0),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut<3>2 ),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<3>2 )
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<2>_1  (
    .CI(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<1>2 ),
    .DI(N1),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut<2>1_54 ),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<2>2 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<2>1  (
    .I0(\XLXI_4/dataOut [5]),
    .I1(\XLXI_4/dataOut [6]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut<2>1_54 )
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<1>_1  (
    .CI(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<0>2 ),
    .DI(N0),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut<1>1 ),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<1>2 )
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<0>_1  (
    .CI(N1),
    .DI(N0),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut<0>2_50 ),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<0>2 )
  );
  LUT4 #(
    .INIT ( 16'h0001 ))
  \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<0>2  (
    .I0(\XLXI_4/dataOut [0]),
    .I1(\XLXI_4/dataOut [1]),
    .I2(\XLXI_4/dataOut [2]),
    .I3(\XLXI_4/dataOut [3]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut<0>2_50 )
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<5>_0  (
    .CI(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<4>1 ),
    .DI(N0),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut [5]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<5>1 )
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<4>_0  (
    .CI(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<3>1 ),
    .DI(N0),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut<4>1_59 ),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<4>1 )
  );
  LUT4 #(
    .INIT ( 16'h0001 ))
  \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<4>1  (
    .I0(\XLXI_4/dataOut [7]),
    .I1(\XLXI_4/dataOut [8]),
    .I2(\XLXI_4/dataOut [9]),
    .I3(\XLXI_4/dataOut [10]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut<4>1_59 )
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<3>_0  (
    .CI(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<2>1 ),
    .DI(N1),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut<3>1_56 ),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<3>1 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<3>1  (
    .I0(\XLXI_4/dataOut [5]),
    .I1(\XLXI_4/dataOut [6]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut<3>1_56 )
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<2>_0  (
    .CI(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<1>1 ),
    .DI(N0),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut [2]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<2>1 )
  );
  LUT2 #(
    .INIT ( 4'h1 ))
  \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<2>  (
    .I0(\XLXI_4/dataOut [3]),
    .I1(\XLXI_4/dataOut [4]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut [2])
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<1>_0  (
    .CI(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<0>1 ),
    .DI(N1),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<1>_0_rt_30 ),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<1>1 )
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<0>_0  (
    .CI(N1),
    .DI(N0),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut<0>1_49 ),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<0>1 )
  );
  LUT2 #(
    .INIT ( 4'h1 ))
  \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<0>1  (
    .I0(\XLXI_4/dataOut [0]),
    .I1(\XLXI_4/dataOut [1]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut<0>1_49 )
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<5>  (
    .CI(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy [4]),
    .DI(N1),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<5>_rt_45 ),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy [5])
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<4>  (
    .CI(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy [3]),
    .DI(N1),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut [4]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy [4])
  );
  LUT4 #(
    .INIT ( 16'h8000 ))
  \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<4>  (
    .I0(\XLXI_4/dataOut [7]),
    .I1(\XLXI_4/dataOut [8]),
    .I2(\XLXI_4/dataOut [9]),
    .I3(\XLXI_4/dataOut [10]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut [4])
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<3>  (
    .CI(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy [2]),
    .DI(N0),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut [3]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy [3])
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<2>  (
    .CI(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy [1]),
    .DI(N1),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<2>_rt_34 ),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy [2])
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<1>  (
    .CI(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy [0]),
    .DI(N0),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut [1]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy [1])
  );
  MUXCY   \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<0>  (
    .CI(N1),
    .DI(N0),
    .S(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut [0]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy [0])
  );
  LUT4 #(
    .INIT ( 16'h0001 ))
  \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<0>  (
    .I0(\XLXI_4/dataOut [0]),
    .I1(\XLXI_4/dataOut [1]),
    .I2(\XLXI_4/dataOut [2]),
    .I3(\XLXI_4/dataOut [3]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut [0])
  );
  FDR #(
    .INIT ( 1'b1 ))
  \XLXI_3/LED2  (
    .C(CLK_BUFGP_1),
    .D(N1),
    .R(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy [6]),
    .Q(\XLXI_3/LED2_22 )
  );
  FDR #(
    .INIT ( 1'b1 ))
  \XLXI_3/LED1  (
    .C(CLK_BUFGP_1),
    .D(N1),
    .R(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<5>1 ),
    .Q(\XLXI_3/LED1_21 )
  );
  FDR #(
    .INIT ( 1'b1 ))
  \XLXI_3/LED3  (
    .C(CLK_BUFGP_1),
    .D(N1),
    .R(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy [5]),
    .Q(\XLXI_3/LED3_23 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[0].KB/value_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value_add0000<0> ),
    .Q(\XLXI_4/KernelBL[0].KB/value<0> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[0].KB/value_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value_add0000<1> ),
    .Q(\XLXI_4/KernelBL[0].KB/value<1> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[0].KB/value_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value_add0000<2> ),
    .Q(\XLXI_4/KernelBL[0].KB/value<2> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[0].KB/value_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value_add0000<3> ),
    .Q(\XLXI_4/KernelBL[0].KB/value<3> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[0].KB/value_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value_add0000<4> ),
    .Q(\XLXI_4/KernelBL[0].KB/value<4> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[0].KB/value_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value_add0000<5> ),
    .Q(\XLXI_4/KernelBL[0].KB/value<5> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[0].KB/value_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value_add0000<6> ),
    .Q(\XLXI_4/KernelBL[0].KB/value<6> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[0].KB/value_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value_add0000<7> ),
    .Q(\XLXI_4/KernelBL[0].KB/value<7> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[0].KB/value_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value_add0000<8> ),
    .Q(\XLXI_4/KernelBL[0].KB/value<8> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[0].KB/value_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value_add0000<9> ),
    .Q(\XLXI_4/KernelBL[0].KB/value<9> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[0].KB/value_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value_add0000<10> ),
    .Q(\XLXI_4/KernelBL[0].KB/value<10> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[0].KB/value_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value_add0000<11> ),
    .Q(\XLXI_4/KernelBL[0].KB/value<11> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[0].KB/value_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value_add0000<12> ),
    .Q(\XLXI_4/KernelBL[0].KB/value<12> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[0].KB/value_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value_add0000<13> ),
    .Q(\XLXI_4/KernelBL[0].KB/value<13> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[0].KB/value_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value_add0000<14> ),
    .Q(\XLXI_4/KernelBL[0].KB/value<14> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[0].KB/value_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value_add0000<15> ),
    .Q(\XLXI_4/KernelBL[0].KB/value<15> )
  );
  MULT18X18SIO #(
    .B_INPUT ( "DIRECT" ),
    .AREG ( 0 ),
    .BREG ( 0 ),
    .PREG ( 0 ))
  \XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2  (
    .CEA(N0),
    .CEB(N0),
    .CEP(N0),
    .CLK(N0),
    .RSTA(N0),
    .RSTB(N0),
    .RSTP(N0),
    .A({N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N1}),
    .B({N0, N0, \XLXI_5/Numb [11], \XLXI_5/Numb [10], \XLXI_5/Numb [9], \XLXI_5/Numb [8], \XLXI_5/Numb [7], \XLXI_5/Numb [6], \XLXI_5/Numb [5], 
\XLXI_5/Numb [4], \XLXI_5/Numb [3], \XLXI_5/Numb [2], \XLXI_5/Numb [1], \XLXI_5/Numb [0], N0, N0, N0, N0}),
    .BCIN({\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED }),
    .P({\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED , 
\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<15> , \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<14> , 
\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<13> , \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<12> , 
\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<11> , \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<10> , 
\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<9> , \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<8> , 
\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<7> , \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<6> , 
\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<5> , \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<4> , 
\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<3> , \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<2> , 
\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<1> , \XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<0> , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED }),
    .BCOUT({\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[0].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED })
  );
  MUXCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<0>  (
    .CI(N0),
    .DI(N0),
    .S(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<0>_rt_63 ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<0>_62 )
  );
  XORCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_xor<0>  (
    .CI(N0),
    .LI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<0>_rt_63 ),
    .O(\XLXI_4/KernelBL[0].KB/value_add0000<0> )
  );
  MUXCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<1>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<0>_62 ),
    .DI(N0),
    .S(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<1>_rt_75 ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<1>_74 )
  );
  XORCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_xor<1>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<0>_62 ),
    .LI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<1>_rt_75 ),
    .O(\XLXI_4/KernelBL[0].KB/value_add0000<1> )
  );
  MUXCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<2>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<1>_74 ),
    .DI(N0),
    .S(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<2>_rt_77 ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<2>_76 )
  );
  XORCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_xor<2>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<1>_74 ),
    .LI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<2>_rt_77 ),
    .O(\XLXI_4/KernelBL[0].KB/value_add0000<2> )
  );
  MUXCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<3>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<2>_76 ),
    .DI(N0),
    .S(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<3>_rt_79 ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<3>_78 )
  );
  XORCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_xor<3>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<2>_76 ),
    .LI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<3>_rt_79 ),
    .O(\XLXI_4/KernelBL[0].KB/value_add0000<3> )
  );
  MUXCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<4>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<3>_78 ),
    .DI(N0),
    .S(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<4>_rt_81 ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<4>_80 )
  );
  XORCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_xor<4>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<3>_78 ),
    .LI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<4>_rt_81 ),
    .O(\XLXI_4/KernelBL[0].KB/value_add0000<4> )
  );
  MUXCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<5>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<4>_80 ),
    .DI(N0),
    .S(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<5>_rt_83 ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<5>_82 )
  );
  XORCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_xor<5>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<4>_80 ),
    .LI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<5>_rt_83 ),
    .O(\XLXI_4/KernelBL[0].KB/value_add0000<5> )
  );
  MUXCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<6>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<5>_82 ),
    .DI(N0),
    .S(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<6>_rt_85 ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<6>_84 )
  );
  XORCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_xor<6>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<5>_82 ),
    .LI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<6>_rt_85 ),
    .O(\XLXI_4/KernelBL[0].KB/value_add0000<6> )
  );
  MUXCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<7>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<6>_84 ),
    .DI(N0),
    .S(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<7>_rt_87 ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<7>_86 )
  );
  XORCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_xor<7>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<6>_84 ),
    .LI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<7>_rt_87 ),
    .O(\XLXI_4/KernelBL[0].KB/value_add0000<7> )
  );
  MUXCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<8>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<7>_86 ),
    .DI(N0),
    .S(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<8>_rt_89 ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<8>_88 )
  );
  XORCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_xor<8>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<7>_86 ),
    .LI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<8>_rt_89 ),
    .O(\XLXI_4/KernelBL[0].KB/value_add0000<8> )
  );
  MUXCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<9>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<8>_88 ),
    .DI(N0),
    .S(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<9>_rt_91 ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<9>_90 )
  );
  XORCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_xor<9>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<8>_88 ),
    .LI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<9>_rt_91 ),
    .O(\XLXI_4/KernelBL[0].KB/value_add0000<9> )
  );
  MUXCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<10>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<9>_90 ),
    .DI(N0),
    .S(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<10>_rt_65 ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<10>_64 )
  );
  XORCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_xor<10>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<9>_90 ),
    .LI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<10>_rt_65 ),
    .O(\XLXI_4/KernelBL[0].KB/value_add0000<10> )
  );
  MUXCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<11>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<10>_64 ),
    .DI(N0),
    .S(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<11>_rt_67 ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<11>_66 )
  );
  XORCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_xor<11>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<10>_64 ),
    .LI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<11>_rt_67 ),
    .O(\XLXI_4/KernelBL[0].KB/value_add0000<11> )
  );
  MUXCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<12>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<11>_66 ),
    .DI(N0),
    .S(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<12>_rt_69 ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<12>_68 )
  );
  XORCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_xor<12>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<11>_66 ),
    .LI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<12>_rt_69 ),
    .O(\XLXI_4/KernelBL[0].KB/value_add0000<12> )
  );
  MUXCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<13>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<12>_68 ),
    .DI(N0),
    .S(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<13>_rt_71 ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<13>_70 )
  );
  XORCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_xor<13>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<12>_68 ),
    .LI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<13>_rt_71 ),
    .O(\XLXI_4/KernelBL[0].KB/value_add0000<13> )
  );
  MUXCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<14>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<13>_70 ),
    .DI(N0),
    .S(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<14>_rt_73 ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<14>_72 )
  );
  XORCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_xor<14>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<13>_70 ),
    .LI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<14>_rt_73 ),
    .O(\XLXI_4/KernelBL[0].KB/value_add0000<14> )
  );
  XORCY   \XLXI_4/KernelBL[0].KB/Madd_value_add0000_xor<15>  (
    .CI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<14>_72 ),
    .LI(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<15> ),
    .O(\XLXI_4/KernelBL[0].KB/value_add0000<15> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/test_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value<0> ),
    .Q(\XLXI_4/KernelBL[1].KB/test<0> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/test_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value<1> ),
    .Q(\XLXI_4/KernelBL[1].KB/test<1> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/test_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value<2> ),
    .Q(\XLXI_4/KernelBL[1].KB/test<2> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/test_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value<3> ),
    .Q(\XLXI_4/KernelBL[1].KB/test<3> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/test_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value<4> ),
    .Q(\XLXI_4/KernelBL[1].KB/test<4> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/test_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value<5> ),
    .Q(\XLXI_4/KernelBL[1].KB/test<5> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/test_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value<6> ),
    .Q(\XLXI_4/KernelBL[1].KB/test<6> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/test_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value<7> ),
    .Q(\XLXI_4/KernelBL[1].KB/test<7> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/test_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value<8> ),
    .Q(\XLXI_4/KernelBL[1].KB/test<8> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/test_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value<9> ),
    .Q(\XLXI_4/KernelBL[1].KB/test<9> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/test_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value<10> ),
    .Q(\XLXI_4/KernelBL[1].KB/test<10> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/test_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value<11> ),
    .Q(\XLXI_4/KernelBL[1].KB/test<11> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/test_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value<12> ),
    .Q(\XLXI_4/KernelBL[1].KB/test<12> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/test_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value<13> ),
    .Q(\XLXI_4/KernelBL[1].KB/test<13> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/test_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value<14> ),
    .Q(\XLXI_4/KernelBL[1].KB/test<14> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/test_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[0].KB/value<15> ),
    .Q(\XLXI_4/KernelBL[1].KB/test<15> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/value_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value_add0000<0> ),
    .Q(\XLXI_4/KernelBL[1].KB/value<0> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/value_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value_add0000<1> ),
    .Q(\XLXI_4/KernelBL[1].KB/value<1> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/value_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value_add0000<2> ),
    .Q(\XLXI_4/KernelBL[1].KB/value<2> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/value_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value_add0000<3> ),
    .Q(\XLXI_4/KernelBL[1].KB/value<3> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/value_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value_add0000<4> ),
    .Q(\XLXI_4/KernelBL[1].KB/value<4> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/value_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value_add0000<5> ),
    .Q(\XLXI_4/KernelBL[1].KB/value<5> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/value_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value_add0000<6> ),
    .Q(\XLXI_4/KernelBL[1].KB/value<6> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/value_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value_add0000<7> ),
    .Q(\XLXI_4/KernelBL[1].KB/value<7> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/value_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value_add0000<8> ),
    .Q(\XLXI_4/KernelBL[1].KB/value<8> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/value_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value_add0000<9> ),
    .Q(\XLXI_4/KernelBL[1].KB/value<9> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/value_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value_add0000<10> ),
    .Q(\XLXI_4/KernelBL[1].KB/value<10> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/value_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value_add0000<11> ),
    .Q(\XLXI_4/KernelBL[1].KB/value<11> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/value_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value_add0000<12> ),
    .Q(\XLXI_4/KernelBL[1].KB/value<12> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/value_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value_add0000<13> ),
    .Q(\XLXI_4/KernelBL[1].KB/value<13> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/value_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value_add0000<14> ),
    .Q(\XLXI_4/KernelBL[1].KB/value<14> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[1].KB/value_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value_add0000<15> ),
    .Q(\XLXI_4/KernelBL[1].KB/value<15> )
  );
  MULT18X18SIO #(
    .B_INPUT ( "DIRECT" ),
    .AREG ( 0 ),
    .BREG ( 0 ),
    .PREG ( 0 ))
  \XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2  (
    .CEA(N0),
    .CEB(N0),
    .CEP(N0),
    .CLK(N0),
    .RSTA(N0),
    .RSTB(N0),
    .RSTP(N0),
    .A({N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N1}),
    .B({N0, N0, \XLXI_5/Numb [11], \XLXI_5/Numb [10], \XLXI_5/Numb [9], \XLXI_5/Numb [8], \XLXI_5/Numb [7], \XLXI_5/Numb [6], \XLXI_5/Numb [5], 
\XLXI_5/Numb [4], \XLXI_5/Numb [3], \XLXI_5/Numb [2], \XLXI_5/Numb [1], \XLXI_5/Numb [0], N0, N0, N0, N0}),
    .BCIN({\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED }),
    .P({\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED , 
\XLXI_4/KernelBL[1].KB/_old_tmp_2<19> , \XLXI_4/KernelBL[1].KB/_old_tmp_2<18> , \XLXI_4/KernelBL[1].KB/_old_tmp_2<17> , 
\XLXI_4/KernelBL[1].KB/_old_tmp_2<16> , \XLXI_4/KernelBL[1].KB/_old_tmp_2<15> , \XLXI_4/KernelBL[1].KB/_old_tmp_2<14> , 
\XLXI_4/KernelBL[1].KB/_old_tmp_2<13> , \XLXI_4/KernelBL[1].KB/_old_tmp_2<12> , \XLXI_4/KernelBL[1].KB/_old_tmp_2<11> , 
\XLXI_4/KernelBL[1].KB/_old_tmp_2<10> , \XLXI_4/KernelBL[1].KB/_old_tmp_2<9> , \XLXI_4/KernelBL[1].KB/_old_tmp_2<8> , 
\XLXI_4/KernelBL[1].KB/_old_tmp_2<7> , \XLXI_4/KernelBL[1].KB/_old_tmp_2<6> , \XLXI_4/KernelBL[1].KB/_old_tmp_2<5> , 
\XLXI_4/KernelBL[1].KB/_old_tmp_2<4> , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED }),
    .BCOUT({\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[1].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED })
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<0>  (
    .I0(\XLXI_4/KernelBL[1].KB/test<0> ),
    .I1(\XLXI_4/KernelBL[1].KB/_old_tmp_2<4> ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<0>_242 )
  );
  MUXCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<0>  (
    .CI(N0),
    .DI(\XLXI_4/KernelBL[1].KB/_old_tmp_2<4> ),
    .S(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<0>_242 ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<0>_227 )
  );
  XORCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_xor<0>  (
    .CI(N0),
    .LI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<0>_242 ),
    .O(\XLXI_4/KernelBL[1].KB/value_add0000<0> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<1>  (
    .I0(\XLXI_4/KernelBL[1].KB/test<1> ),
    .I1(\XLXI_4/KernelBL[1].KB/_old_tmp_2<5> ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<1>_249 )
  );
  MUXCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<1>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<0>_227 ),
    .DI(\XLXI_4/KernelBL[1].KB/_old_tmp_2<5> ),
    .S(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<1>_249 ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<1>_233 )
  );
  XORCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_xor<1>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<0>_227 ),
    .LI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<1>_249 ),
    .O(\XLXI_4/KernelBL[1].KB/value_add0000<1> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<2>  (
    .I0(\XLXI_4/KernelBL[1].KB/test<2> ),
    .I1(\XLXI_4/KernelBL[1].KB/_old_tmp_2<6> ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<2>_250 )
  );
  MUXCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<2>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<1>_233 ),
    .DI(\XLXI_4/KernelBL[1].KB/_old_tmp_2<6> ),
    .S(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<2>_250 ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<2>_234 )
  );
  XORCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_xor<2>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<1>_233 ),
    .LI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<2>_250 ),
    .O(\XLXI_4/KernelBL[1].KB/value_add0000<2> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<3>  (
    .I0(\XLXI_4/KernelBL[1].KB/test<3> ),
    .I1(\XLXI_4/KernelBL[1].KB/_old_tmp_2<7> ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<3>_251 )
  );
  MUXCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<3>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<2>_234 ),
    .DI(\XLXI_4/KernelBL[1].KB/_old_tmp_2<7> ),
    .S(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<3>_251 ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<3>_235 )
  );
  XORCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_xor<3>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<2>_234 ),
    .LI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<3>_251 ),
    .O(\XLXI_4/KernelBL[1].KB/value_add0000<3> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<4>  (
    .I0(\XLXI_4/KernelBL[1].KB/test<4> ),
    .I1(\XLXI_4/KernelBL[1].KB/_old_tmp_2<8> ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<4>_252 )
  );
  MUXCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<4>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<3>_235 ),
    .DI(\XLXI_4/KernelBL[1].KB/_old_tmp_2<8> ),
    .S(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<4>_252 ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<4>_236 )
  );
  XORCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_xor<4>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<3>_235 ),
    .LI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<4>_252 ),
    .O(\XLXI_4/KernelBL[1].KB/value_add0000<4> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<5>  (
    .I0(\XLXI_4/KernelBL[1].KB/test<5> ),
    .I1(\XLXI_4/KernelBL[1].KB/_old_tmp_2<9> ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<5>_253 )
  );
  MUXCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<5>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<4>_236 ),
    .DI(\XLXI_4/KernelBL[1].KB/_old_tmp_2<9> ),
    .S(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<5>_253 ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<5>_237 )
  );
  XORCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_xor<5>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<4>_236 ),
    .LI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<5>_253 ),
    .O(\XLXI_4/KernelBL[1].KB/value_add0000<5> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<6>  (
    .I0(\XLXI_4/KernelBL[1].KB/test<6> ),
    .I1(\XLXI_4/KernelBL[1].KB/_old_tmp_2<10> ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<6>_254 )
  );
  MUXCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<6>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<5>_237 ),
    .DI(\XLXI_4/KernelBL[1].KB/_old_tmp_2<10> ),
    .S(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<6>_254 ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<6>_238 )
  );
  XORCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_xor<6>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<5>_237 ),
    .LI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<6>_254 ),
    .O(\XLXI_4/KernelBL[1].KB/value_add0000<6> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<7>  (
    .I0(\XLXI_4/KernelBL[1].KB/test<7> ),
    .I1(\XLXI_4/KernelBL[1].KB/_old_tmp_2<11> ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<7>_255 )
  );
  MUXCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<7>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<6>_238 ),
    .DI(\XLXI_4/KernelBL[1].KB/_old_tmp_2<11> ),
    .S(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<7>_255 ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<7>_239 )
  );
  XORCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_xor<7>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<6>_238 ),
    .LI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<7>_255 ),
    .O(\XLXI_4/KernelBL[1].KB/value_add0000<7> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<8>  (
    .I0(\XLXI_4/KernelBL[1].KB/test<8> ),
    .I1(\XLXI_4/KernelBL[1].KB/_old_tmp_2<12> ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<8>_256 )
  );
  MUXCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<8>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<7>_239 ),
    .DI(\XLXI_4/KernelBL[1].KB/_old_tmp_2<12> ),
    .S(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<8>_256 ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<8>_240 )
  );
  XORCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_xor<8>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<7>_239 ),
    .LI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<8>_256 ),
    .O(\XLXI_4/KernelBL[1].KB/value_add0000<8> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<9>  (
    .I0(\XLXI_4/KernelBL[1].KB/test<9> ),
    .I1(\XLXI_4/KernelBL[1].KB/_old_tmp_2<13> ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<9>_257 )
  );
  MUXCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<9>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<8>_240 ),
    .DI(\XLXI_4/KernelBL[1].KB/_old_tmp_2<13> ),
    .S(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<9>_257 ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<9>_241 )
  );
  XORCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_xor<9>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<8>_240 ),
    .LI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<9>_257 ),
    .O(\XLXI_4/KernelBL[1].KB/value_add0000<9> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<10>  (
    .I0(\XLXI_4/KernelBL[1].KB/test<10> ),
    .I1(\XLXI_4/KernelBL[1].KB/_old_tmp_2<14> ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<10>_243 )
  );
  MUXCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<10>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<9>_241 ),
    .DI(\XLXI_4/KernelBL[1].KB/_old_tmp_2<14> ),
    .S(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<10>_243 ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<10>_228 )
  );
  XORCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_xor<10>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<9>_241 ),
    .LI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<10>_243 ),
    .O(\XLXI_4/KernelBL[1].KB/value_add0000<10> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<11>  (
    .I0(\XLXI_4/KernelBL[1].KB/test<11> ),
    .I1(\XLXI_4/KernelBL[1].KB/_old_tmp_2<15> ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<11>_244 )
  );
  MUXCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<11>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<10>_228 ),
    .DI(\XLXI_4/KernelBL[1].KB/_old_tmp_2<15> ),
    .S(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<11>_244 ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<11>_229 )
  );
  XORCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_xor<11>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<10>_228 ),
    .LI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<11>_244 ),
    .O(\XLXI_4/KernelBL[1].KB/value_add0000<11> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<12>  (
    .I0(\XLXI_4/KernelBL[1].KB/test<12> ),
    .I1(\XLXI_4/KernelBL[1].KB/_old_tmp_2<16> ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<12>_245 )
  );
  MUXCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<12>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<11>_229 ),
    .DI(\XLXI_4/KernelBL[1].KB/_old_tmp_2<16> ),
    .S(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<12>_245 ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<12>_230 )
  );
  XORCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_xor<12>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<11>_229 ),
    .LI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<12>_245 ),
    .O(\XLXI_4/KernelBL[1].KB/value_add0000<12> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<13>  (
    .I0(\XLXI_4/KernelBL[1].KB/test<13> ),
    .I1(\XLXI_4/KernelBL[1].KB/_old_tmp_2<17> ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<13>_246 )
  );
  MUXCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<13>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<12>_230 ),
    .DI(\XLXI_4/KernelBL[1].KB/_old_tmp_2<17> ),
    .S(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<13>_246 ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<13>_231 )
  );
  XORCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_xor<13>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<12>_230 ),
    .LI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<13>_246 ),
    .O(\XLXI_4/KernelBL[1].KB/value_add0000<13> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<14>  (
    .I0(\XLXI_4/KernelBL[1].KB/test<14> ),
    .I1(\XLXI_4/KernelBL[1].KB/_old_tmp_2<18> ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<14>_247 )
  );
  MUXCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<14>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<13>_231 ),
    .DI(\XLXI_4/KernelBL[1].KB/_old_tmp_2<18> ),
    .S(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<14>_247 ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<14>_232 )
  );
  XORCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_xor<14>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<13>_231 ),
    .LI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<14>_247 ),
    .O(\XLXI_4/KernelBL[1].KB/value_add0000<14> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<15>  (
    .I0(\XLXI_4/KernelBL[1].KB/test<15> ),
    .I1(\XLXI_4/KernelBL[1].KB/_old_tmp_2<19> ),
    .O(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<15>_248 )
  );
  XORCY   \XLXI_4/KernelBL[1].KB/Madd_value_add0000_xor<15>  (
    .CI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_cy<14>_232 ),
    .LI(\XLXI_4/KernelBL[1].KB/Madd_value_add0000_lut<15>_248 ),
    .O(\XLXI_4/KernelBL[1].KB/value_add0000<15> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/test_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value<0> ),
    .Q(\XLXI_4/KernelBL[2].KB/test<0> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/test_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value<1> ),
    .Q(\XLXI_4/KernelBL[2].KB/test<1> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/test_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value<2> ),
    .Q(\XLXI_4/KernelBL[2].KB/test<2> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/test_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value<3> ),
    .Q(\XLXI_4/KernelBL[2].KB/test<3> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/test_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value<4> ),
    .Q(\XLXI_4/KernelBL[2].KB/test<4> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/test_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value<5> ),
    .Q(\XLXI_4/KernelBL[2].KB/test<5> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/test_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value<6> ),
    .Q(\XLXI_4/KernelBL[2].KB/test<6> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/test_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value<7> ),
    .Q(\XLXI_4/KernelBL[2].KB/test<7> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/test_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value<8> ),
    .Q(\XLXI_4/KernelBL[2].KB/test<8> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/test_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value<9> ),
    .Q(\XLXI_4/KernelBL[2].KB/test<9> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/test_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value<10> ),
    .Q(\XLXI_4/KernelBL[2].KB/test<10> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/test_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value<11> ),
    .Q(\XLXI_4/KernelBL[2].KB/test<11> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/test_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value<12> ),
    .Q(\XLXI_4/KernelBL[2].KB/test<12> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/test_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value<13> ),
    .Q(\XLXI_4/KernelBL[2].KB/test<13> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/test_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value<14> ),
    .Q(\XLXI_4/KernelBL[2].KB/test<14> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/test_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[1].KB/value<15> ),
    .Q(\XLXI_4/KernelBL[2].KB/test<15> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/value_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value_add0000<0> ),
    .Q(\XLXI_4/KernelBL[2].KB/value<0> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/value_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value_add0000<1> ),
    .Q(\XLXI_4/KernelBL[2].KB/value<1> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/value_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value_add0000<2> ),
    .Q(\XLXI_4/KernelBL[2].KB/value<2> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/value_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value_add0000<3> ),
    .Q(\XLXI_4/KernelBL[2].KB/value<3> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/value_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value_add0000<4> ),
    .Q(\XLXI_4/KernelBL[2].KB/value<4> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/value_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value_add0000<5> ),
    .Q(\XLXI_4/KernelBL[2].KB/value<5> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/value_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value_add0000<6> ),
    .Q(\XLXI_4/KernelBL[2].KB/value<6> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/value_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value_add0000<7> ),
    .Q(\XLXI_4/KernelBL[2].KB/value<7> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/value_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value_add0000<8> ),
    .Q(\XLXI_4/KernelBL[2].KB/value<8> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/value_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value_add0000<9> ),
    .Q(\XLXI_4/KernelBL[2].KB/value<9> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/value_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value_add0000<10> ),
    .Q(\XLXI_4/KernelBL[2].KB/value<10> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/value_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value_add0000<11> ),
    .Q(\XLXI_4/KernelBL[2].KB/value<11> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/value_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value_add0000<12> ),
    .Q(\XLXI_4/KernelBL[2].KB/value<12> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/value_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value_add0000<13> ),
    .Q(\XLXI_4/KernelBL[2].KB/value<13> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/value_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value_add0000<14> ),
    .Q(\XLXI_4/KernelBL[2].KB/value<14> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[2].KB/value_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value_add0000<15> ),
    .Q(\XLXI_4/KernelBL[2].KB/value<15> )
  );
  MULT18X18SIO #(
    .B_INPUT ( "DIRECT" ),
    .AREG ( 0 ),
    .BREG ( 0 ),
    .PREG ( 0 ))
  \XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2  (
    .CEA(N0),
    .CEB(N0),
    .CEP(N0),
    .CLK(N0),
    .RSTA(N0),
    .RSTB(N0),
    .RSTP(N0),
    .A({N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N1}),
    .B({N0, N0, \XLXI_5/Numb [11], \XLXI_5/Numb [10], \XLXI_5/Numb [9], \XLXI_5/Numb [8], \XLXI_5/Numb [7], \XLXI_5/Numb [6], \XLXI_5/Numb [5], 
\XLXI_5/Numb [4], \XLXI_5/Numb [3], \XLXI_5/Numb [2], \XLXI_5/Numb [1], \XLXI_5/Numb [0], N0, N0, N0, N0}),
    .BCIN({\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED }),
    .P({\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED , 
\XLXI_4/KernelBL[2].KB/_old_tmp_2<19> , \XLXI_4/KernelBL[2].KB/_old_tmp_2<18> , \XLXI_4/KernelBL[2].KB/_old_tmp_2<17> , 
\XLXI_4/KernelBL[2].KB/_old_tmp_2<16> , \XLXI_4/KernelBL[2].KB/_old_tmp_2<15> , \XLXI_4/KernelBL[2].KB/_old_tmp_2<14> , 
\XLXI_4/KernelBL[2].KB/_old_tmp_2<13> , \XLXI_4/KernelBL[2].KB/_old_tmp_2<12> , \XLXI_4/KernelBL[2].KB/_old_tmp_2<11> , 
\XLXI_4/KernelBL[2].KB/_old_tmp_2<10> , \XLXI_4/KernelBL[2].KB/_old_tmp_2<9> , \XLXI_4/KernelBL[2].KB/_old_tmp_2<8> , 
\XLXI_4/KernelBL[2].KB/_old_tmp_2<7> , \XLXI_4/KernelBL[2].KB/_old_tmp_2<6> , \XLXI_4/KernelBL[2].KB/_old_tmp_2<5> , 
\XLXI_4/KernelBL[2].KB/_old_tmp_2<4> , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED }),
    .BCOUT({\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[2].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED })
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<0>  (
    .I0(\XLXI_4/KernelBL[2].KB/test<0> ),
    .I1(\XLXI_4/KernelBL[2].KB/_old_tmp_2<4> ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<0>_337 )
  );
  MUXCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<0>  (
    .CI(N0),
    .DI(\XLXI_4/KernelBL[2].KB/_old_tmp_2<4> ),
    .S(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<0>_337 ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<0>_322 )
  );
  XORCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_xor<0>  (
    .CI(N0),
    .LI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<0>_337 ),
    .O(\XLXI_4/KernelBL[2].KB/value_add0000<0> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<1>  (
    .I0(\XLXI_4/KernelBL[2].KB/test<1> ),
    .I1(\XLXI_4/KernelBL[2].KB/_old_tmp_2<5> ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<1>_344 )
  );
  MUXCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<1>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<0>_322 ),
    .DI(\XLXI_4/KernelBL[2].KB/_old_tmp_2<5> ),
    .S(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<1>_344 ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<1>_328 )
  );
  XORCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_xor<1>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<0>_322 ),
    .LI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<1>_344 ),
    .O(\XLXI_4/KernelBL[2].KB/value_add0000<1> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<2>  (
    .I0(\XLXI_4/KernelBL[2].KB/test<2> ),
    .I1(\XLXI_4/KernelBL[2].KB/_old_tmp_2<6> ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<2>_345 )
  );
  MUXCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<2>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<1>_328 ),
    .DI(\XLXI_4/KernelBL[2].KB/_old_tmp_2<6> ),
    .S(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<2>_345 ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<2>_329 )
  );
  XORCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_xor<2>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<1>_328 ),
    .LI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<2>_345 ),
    .O(\XLXI_4/KernelBL[2].KB/value_add0000<2> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<3>  (
    .I0(\XLXI_4/KernelBL[2].KB/test<3> ),
    .I1(\XLXI_4/KernelBL[2].KB/_old_tmp_2<7> ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<3>_346 )
  );
  MUXCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<3>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<2>_329 ),
    .DI(\XLXI_4/KernelBL[2].KB/_old_tmp_2<7> ),
    .S(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<3>_346 ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<3>_330 )
  );
  XORCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_xor<3>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<2>_329 ),
    .LI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<3>_346 ),
    .O(\XLXI_4/KernelBL[2].KB/value_add0000<3> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<4>  (
    .I0(\XLXI_4/KernelBL[2].KB/test<4> ),
    .I1(\XLXI_4/KernelBL[2].KB/_old_tmp_2<8> ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<4>_347 )
  );
  MUXCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<4>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<3>_330 ),
    .DI(\XLXI_4/KernelBL[2].KB/_old_tmp_2<8> ),
    .S(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<4>_347 ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<4>_331 )
  );
  XORCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_xor<4>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<3>_330 ),
    .LI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<4>_347 ),
    .O(\XLXI_4/KernelBL[2].KB/value_add0000<4> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<5>  (
    .I0(\XLXI_4/KernelBL[2].KB/test<5> ),
    .I1(\XLXI_4/KernelBL[2].KB/_old_tmp_2<9> ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<5>_348 )
  );
  MUXCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<5>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<4>_331 ),
    .DI(\XLXI_4/KernelBL[2].KB/_old_tmp_2<9> ),
    .S(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<5>_348 ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<5>_332 )
  );
  XORCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_xor<5>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<4>_331 ),
    .LI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<5>_348 ),
    .O(\XLXI_4/KernelBL[2].KB/value_add0000<5> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<6>  (
    .I0(\XLXI_4/KernelBL[2].KB/test<6> ),
    .I1(\XLXI_4/KernelBL[2].KB/_old_tmp_2<10> ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<6>_349 )
  );
  MUXCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<6>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<5>_332 ),
    .DI(\XLXI_4/KernelBL[2].KB/_old_tmp_2<10> ),
    .S(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<6>_349 ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<6>_333 )
  );
  XORCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_xor<6>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<5>_332 ),
    .LI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<6>_349 ),
    .O(\XLXI_4/KernelBL[2].KB/value_add0000<6> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<7>  (
    .I0(\XLXI_4/KernelBL[2].KB/test<7> ),
    .I1(\XLXI_4/KernelBL[2].KB/_old_tmp_2<11> ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<7>_350 )
  );
  MUXCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<7>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<6>_333 ),
    .DI(\XLXI_4/KernelBL[2].KB/_old_tmp_2<11> ),
    .S(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<7>_350 ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<7>_334 )
  );
  XORCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_xor<7>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<6>_333 ),
    .LI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<7>_350 ),
    .O(\XLXI_4/KernelBL[2].KB/value_add0000<7> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<8>  (
    .I0(\XLXI_4/KernelBL[2].KB/test<8> ),
    .I1(\XLXI_4/KernelBL[2].KB/_old_tmp_2<12> ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<8>_351 )
  );
  MUXCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<8>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<7>_334 ),
    .DI(\XLXI_4/KernelBL[2].KB/_old_tmp_2<12> ),
    .S(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<8>_351 ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<8>_335 )
  );
  XORCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_xor<8>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<7>_334 ),
    .LI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<8>_351 ),
    .O(\XLXI_4/KernelBL[2].KB/value_add0000<8> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<9>  (
    .I0(\XLXI_4/KernelBL[2].KB/test<9> ),
    .I1(\XLXI_4/KernelBL[2].KB/_old_tmp_2<13> ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<9>_352 )
  );
  MUXCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<9>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<8>_335 ),
    .DI(\XLXI_4/KernelBL[2].KB/_old_tmp_2<13> ),
    .S(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<9>_352 ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<9>_336 )
  );
  XORCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_xor<9>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<8>_335 ),
    .LI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<9>_352 ),
    .O(\XLXI_4/KernelBL[2].KB/value_add0000<9> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<10>  (
    .I0(\XLXI_4/KernelBL[2].KB/test<10> ),
    .I1(\XLXI_4/KernelBL[2].KB/_old_tmp_2<14> ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<10>_338 )
  );
  MUXCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<10>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<9>_336 ),
    .DI(\XLXI_4/KernelBL[2].KB/_old_tmp_2<14> ),
    .S(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<10>_338 ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<10>_323 )
  );
  XORCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_xor<10>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<9>_336 ),
    .LI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<10>_338 ),
    .O(\XLXI_4/KernelBL[2].KB/value_add0000<10> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<11>  (
    .I0(\XLXI_4/KernelBL[2].KB/test<11> ),
    .I1(\XLXI_4/KernelBL[2].KB/_old_tmp_2<15> ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<11>_339 )
  );
  MUXCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<11>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<10>_323 ),
    .DI(\XLXI_4/KernelBL[2].KB/_old_tmp_2<15> ),
    .S(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<11>_339 ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<11>_324 )
  );
  XORCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_xor<11>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<10>_323 ),
    .LI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<11>_339 ),
    .O(\XLXI_4/KernelBL[2].KB/value_add0000<11> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<12>  (
    .I0(\XLXI_4/KernelBL[2].KB/test<12> ),
    .I1(\XLXI_4/KernelBL[2].KB/_old_tmp_2<16> ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<12>_340 )
  );
  MUXCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<12>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<11>_324 ),
    .DI(\XLXI_4/KernelBL[2].KB/_old_tmp_2<16> ),
    .S(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<12>_340 ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<12>_325 )
  );
  XORCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_xor<12>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<11>_324 ),
    .LI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<12>_340 ),
    .O(\XLXI_4/KernelBL[2].KB/value_add0000<12> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<13>  (
    .I0(\XLXI_4/KernelBL[2].KB/test<13> ),
    .I1(\XLXI_4/KernelBL[2].KB/_old_tmp_2<17> ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<13>_341 )
  );
  MUXCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<13>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<12>_325 ),
    .DI(\XLXI_4/KernelBL[2].KB/_old_tmp_2<17> ),
    .S(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<13>_341 ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<13>_326 )
  );
  XORCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_xor<13>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<12>_325 ),
    .LI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<13>_341 ),
    .O(\XLXI_4/KernelBL[2].KB/value_add0000<13> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<14>  (
    .I0(\XLXI_4/KernelBL[2].KB/test<14> ),
    .I1(\XLXI_4/KernelBL[2].KB/_old_tmp_2<18> ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<14>_342 )
  );
  MUXCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<14>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<13>_326 ),
    .DI(\XLXI_4/KernelBL[2].KB/_old_tmp_2<18> ),
    .S(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<14>_342 ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<14>_327 )
  );
  XORCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_xor<14>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<13>_326 ),
    .LI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<14>_342 ),
    .O(\XLXI_4/KernelBL[2].KB/value_add0000<14> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<15>  (
    .I0(\XLXI_4/KernelBL[2].KB/test<15> ),
    .I1(\XLXI_4/KernelBL[2].KB/_old_tmp_2<19> ),
    .O(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<15>_343 )
  );
  XORCY   \XLXI_4/KernelBL[2].KB/Madd_value_add0000_xor<15>  (
    .CI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_cy<14>_327 ),
    .LI(\XLXI_4/KernelBL[2].KB/Madd_value_add0000_lut<15>_343 ),
    .O(\XLXI_4/KernelBL[2].KB/value_add0000<15> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/test_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value<0> ),
    .Q(\XLXI_4/KernelBL[3].KB/test<0> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/test_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value<1> ),
    .Q(\XLXI_4/KernelBL[3].KB/test<1> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/test_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value<2> ),
    .Q(\XLXI_4/KernelBL[3].KB/test<2> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/test_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value<3> ),
    .Q(\XLXI_4/KernelBL[3].KB/test<3> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/test_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value<4> ),
    .Q(\XLXI_4/KernelBL[3].KB/test<4> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/test_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value<5> ),
    .Q(\XLXI_4/KernelBL[3].KB/test<5> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/test_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value<6> ),
    .Q(\XLXI_4/KernelBL[3].KB/test<6> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/test_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value<7> ),
    .Q(\XLXI_4/KernelBL[3].KB/test<7> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/test_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value<8> ),
    .Q(\XLXI_4/KernelBL[3].KB/test<8> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/test_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value<9> ),
    .Q(\XLXI_4/KernelBL[3].KB/test<9> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/test_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value<10> ),
    .Q(\XLXI_4/KernelBL[3].KB/test<10> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/test_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value<11> ),
    .Q(\XLXI_4/KernelBL[3].KB/test<11> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/test_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value<12> ),
    .Q(\XLXI_4/KernelBL[3].KB/test<12> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/test_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value<13> ),
    .Q(\XLXI_4/KernelBL[3].KB/test<13> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/test_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value<14> ),
    .Q(\XLXI_4/KernelBL[3].KB/test<14> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/test_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[2].KB/value<15> ),
    .Q(\XLXI_4/KernelBL[3].KB/test<15> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/value_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value_add0000<0> ),
    .Q(\XLXI_4/KernelBL[3].KB/value<0> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/value_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value_add0000<1> ),
    .Q(\XLXI_4/KernelBL[3].KB/value<1> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/value_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value_add0000<2> ),
    .Q(\XLXI_4/KernelBL[3].KB/value<2> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/value_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value_add0000<3> ),
    .Q(\XLXI_4/KernelBL[3].KB/value<3> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/value_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value_add0000<4> ),
    .Q(\XLXI_4/KernelBL[3].KB/value<4> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/value_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value_add0000<5> ),
    .Q(\XLXI_4/KernelBL[3].KB/value<5> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/value_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value_add0000<6> ),
    .Q(\XLXI_4/KernelBL[3].KB/value<6> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/value_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value_add0000<7> ),
    .Q(\XLXI_4/KernelBL[3].KB/value<7> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/value_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value_add0000<8> ),
    .Q(\XLXI_4/KernelBL[3].KB/value<8> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/value_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value_add0000<9> ),
    .Q(\XLXI_4/KernelBL[3].KB/value<9> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/value_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value_add0000<10> ),
    .Q(\XLXI_4/KernelBL[3].KB/value<10> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/value_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value_add0000<11> ),
    .Q(\XLXI_4/KernelBL[3].KB/value<11> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/value_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value_add0000<12> ),
    .Q(\XLXI_4/KernelBL[3].KB/value<12> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/value_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value_add0000<13> ),
    .Q(\XLXI_4/KernelBL[3].KB/value<13> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/value_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value_add0000<14> ),
    .Q(\XLXI_4/KernelBL[3].KB/value<14> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[3].KB/value_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value_add0000<15> ),
    .Q(\XLXI_4/KernelBL[3].KB/value<15> )
  );
  MULT18X18SIO #(
    .B_INPUT ( "DIRECT" ),
    .AREG ( 0 ),
    .BREG ( 0 ),
    .PREG ( 0 ))
  \XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2  (
    .CEA(N0),
    .CEB(N0),
    .CEP(N0),
    .CLK(N0),
    .RSTA(N0),
    .RSTB(N0),
    .RSTP(N0),
    .A({N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N1}),
    .B({N0, N0, \XLXI_5/Numb [11], \XLXI_5/Numb [10], \XLXI_5/Numb [9], \XLXI_5/Numb [8], \XLXI_5/Numb [7], \XLXI_5/Numb [6], \XLXI_5/Numb [5], 
\XLXI_5/Numb [4], \XLXI_5/Numb [3], \XLXI_5/Numb [2], \XLXI_5/Numb [1], \XLXI_5/Numb [0], N0, N0, N0, N0}),
    .BCIN({\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED }),
    .P({\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED , 
\XLXI_4/KernelBL[3].KB/_old_tmp_2<19> , \XLXI_4/KernelBL[3].KB/_old_tmp_2<18> , \XLXI_4/KernelBL[3].KB/_old_tmp_2<17> , 
\XLXI_4/KernelBL[3].KB/_old_tmp_2<16> , \XLXI_4/KernelBL[3].KB/_old_tmp_2<15> , \XLXI_4/KernelBL[3].KB/_old_tmp_2<14> , 
\XLXI_4/KernelBL[3].KB/_old_tmp_2<13> , \XLXI_4/KernelBL[3].KB/_old_tmp_2<12> , \XLXI_4/KernelBL[3].KB/_old_tmp_2<11> , 
\XLXI_4/KernelBL[3].KB/_old_tmp_2<10> , \XLXI_4/KernelBL[3].KB/_old_tmp_2<9> , \XLXI_4/KernelBL[3].KB/_old_tmp_2<8> , 
\XLXI_4/KernelBL[3].KB/_old_tmp_2<7> , \XLXI_4/KernelBL[3].KB/_old_tmp_2<6> , \XLXI_4/KernelBL[3].KB/_old_tmp_2<5> , 
\XLXI_4/KernelBL[3].KB/_old_tmp_2<4> , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED }),
    .BCOUT({\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[3].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED })
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<0>  (
    .I0(\XLXI_4/KernelBL[3].KB/test<0> ),
    .I1(\XLXI_4/KernelBL[3].KB/_old_tmp_2<4> ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<0>_432 )
  );
  MUXCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<0>  (
    .CI(N0),
    .DI(\XLXI_4/KernelBL[3].KB/_old_tmp_2<4> ),
    .S(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<0>_432 ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<0>_417 )
  );
  XORCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_xor<0>  (
    .CI(N0),
    .LI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<0>_432 ),
    .O(\XLXI_4/KernelBL[3].KB/value_add0000<0> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<1>  (
    .I0(\XLXI_4/KernelBL[3].KB/test<1> ),
    .I1(\XLXI_4/KernelBL[3].KB/_old_tmp_2<5> ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<1>_439 )
  );
  MUXCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<1>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<0>_417 ),
    .DI(\XLXI_4/KernelBL[3].KB/_old_tmp_2<5> ),
    .S(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<1>_439 ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<1>_423 )
  );
  XORCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_xor<1>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<0>_417 ),
    .LI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<1>_439 ),
    .O(\XLXI_4/KernelBL[3].KB/value_add0000<1> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<2>  (
    .I0(\XLXI_4/KernelBL[3].KB/test<2> ),
    .I1(\XLXI_4/KernelBL[3].KB/_old_tmp_2<6> ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<2>_440 )
  );
  MUXCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<2>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<1>_423 ),
    .DI(\XLXI_4/KernelBL[3].KB/_old_tmp_2<6> ),
    .S(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<2>_440 ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<2>_424 )
  );
  XORCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_xor<2>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<1>_423 ),
    .LI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<2>_440 ),
    .O(\XLXI_4/KernelBL[3].KB/value_add0000<2> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<3>  (
    .I0(\XLXI_4/KernelBL[3].KB/test<3> ),
    .I1(\XLXI_4/KernelBL[3].KB/_old_tmp_2<7> ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<3>_441 )
  );
  MUXCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<3>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<2>_424 ),
    .DI(\XLXI_4/KernelBL[3].KB/_old_tmp_2<7> ),
    .S(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<3>_441 ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<3>_425 )
  );
  XORCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_xor<3>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<2>_424 ),
    .LI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<3>_441 ),
    .O(\XLXI_4/KernelBL[3].KB/value_add0000<3> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<4>  (
    .I0(\XLXI_4/KernelBL[3].KB/test<4> ),
    .I1(\XLXI_4/KernelBL[3].KB/_old_tmp_2<8> ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<4>_442 )
  );
  MUXCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<4>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<3>_425 ),
    .DI(\XLXI_4/KernelBL[3].KB/_old_tmp_2<8> ),
    .S(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<4>_442 ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<4>_426 )
  );
  XORCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_xor<4>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<3>_425 ),
    .LI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<4>_442 ),
    .O(\XLXI_4/KernelBL[3].KB/value_add0000<4> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<5>  (
    .I0(\XLXI_4/KernelBL[3].KB/test<5> ),
    .I1(\XLXI_4/KernelBL[3].KB/_old_tmp_2<9> ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<5>_443 )
  );
  MUXCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<5>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<4>_426 ),
    .DI(\XLXI_4/KernelBL[3].KB/_old_tmp_2<9> ),
    .S(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<5>_443 ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<5>_427 )
  );
  XORCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_xor<5>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<4>_426 ),
    .LI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<5>_443 ),
    .O(\XLXI_4/KernelBL[3].KB/value_add0000<5> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<6>  (
    .I0(\XLXI_4/KernelBL[3].KB/test<6> ),
    .I1(\XLXI_4/KernelBL[3].KB/_old_tmp_2<10> ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<6>_444 )
  );
  MUXCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<6>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<5>_427 ),
    .DI(\XLXI_4/KernelBL[3].KB/_old_tmp_2<10> ),
    .S(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<6>_444 ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<6>_428 )
  );
  XORCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_xor<6>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<5>_427 ),
    .LI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<6>_444 ),
    .O(\XLXI_4/KernelBL[3].KB/value_add0000<6> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<7>  (
    .I0(\XLXI_4/KernelBL[3].KB/test<7> ),
    .I1(\XLXI_4/KernelBL[3].KB/_old_tmp_2<11> ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<7>_445 )
  );
  MUXCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<7>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<6>_428 ),
    .DI(\XLXI_4/KernelBL[3].KB/_old_tmp_2<11> ),
    .S(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<7>_445 ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<7>_429 )
  );
  XORCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_xor<7>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<6>_428 ),
    .LI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<7>_445 ),
    .O(\XLXI_4/KernelBL[3].KB/value_add0000<7> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<8>  (
    .I0(\XLXI_4/KernelBL[3].KB/test<8> ),
    .I1(\XLXI_4/KernelBL[3].KB/_old_tmp_2<12> ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<8>_446 )
  );
  MUXCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<8>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<7>_429 ),
    .DI(\XLXI_4/KernelBL[3].KB/_old_tmp_2<12> ),
    .S(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<8>_446 ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<8>_430 )
  );
  XORCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_xor<8>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<7>_429 ),
    .LI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<8>_446 ),
    .O(\XLXI_4/KernelBL[3].KB/value_add0000<8> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<9>  (
    .I0(\XLXI_4/KernelBL[3].KB/test<9> ),
    .I1(\XLXI_4/KernelBL[3].KB/_old_tmp_2<13> ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<9>_447 )
  );
  MUXCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<9>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<8>_430 ),
    .DI(\XLXI_4/KernelBL[3].KB/_old_tmp_2<13> ),
    .S(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<9>_447 ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<9>_431 )
  );
  XORCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_xor<9>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<8>_430 ),
    .LI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<9>_447 ),
    .O(\XLXI_4/KernelBL[3].KB/value_add0000<9> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<10>  (
    .I0(\XLXI_4/KernelBL[3].KB/test<10> ),
    .I1(\XLXI_4/KernelBL[3].KB/_old_tmp_2<14> ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<10>_433 )
  );
  MUXCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<10>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<9>_431 ),
    .DI(\XLXI_4/KernelBL[3].KB/_old_tmp_2<14> ),
    .S(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<10>_433 ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<10>_418 )
  );
  XORCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_xor<10>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<9>_431 ),
    .LI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<10>_433 ),
    .O(\XLXI_4/KernelBL[3].KB/value_add0000<10> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<11>  (
    .I0(\XLXI_4/KernelBL[3].KB/test<11> ),
    .I1(\XLXI_4/KernelBL[3].KB/_old_tmp_2<15> ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<11>_434 )
  );
  MUXCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<11>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<10>_418 ),
    .DI(\XLXI_4/KernelBL[3].KB/_old_tmp_2<15> ),
    .S(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<11>_434 ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<11>_419 )
  );
  XORCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_xor<11>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<10>_418 ),
    .LI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<11>_434 ),
    .O(\XLXI_4/KernelBL[3].KB/value_add0000<11> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<12>  (
    .I0(\XLXI_4/KernelBL[3].KB/test<12> ),
    .I1(\XLXI_4/KernelBL[3].KB/_old_tmp_2<16> ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<12>_435 )
  );
  MUXCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<12>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<11>_419 ),
    .DI(\XLXI_4/KernelBL[3].KB/_old_tmp_2<16> ),
    .S(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<12>_435 ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<12>_420 )
  );
  XORCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_xor<12>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<11>_419 ),
    .LI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<12>_435 ),
    .O(\XLXI_4/KernelBL[3].KB/value_add0000<12> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<13>  (
    .I0(\XLXI_4/KernelBL[3].KB/test<13> ),
    .I1(\XLXI_4/KernelBL[3].KB/_old_tmp_2<17> ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<13>_436 )
  );
  MUXCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<13>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<12>_420 ),
    .DI(\XLXI_4/KernelBL[3].KB/_old_tmp_2<17> ),
    .S(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<13>_436 ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<13>_421 )
  );
  XORCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_xor<13>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<12>_420 ),
    .LI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<13>_436 ),
    .O(\XLXI_4/KernelBL[3].KB/value_add0000<13> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<14>  (
    .I0(\XLXI_4/KernelBL[3].KB/test<14> ),
    .I1(\XLXI_4/KernelBL[3].KB/_old_tmp_2<18> ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<14>_437 )
  );
  MUXCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<14>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<13>_421 ),
    .DI(\XLXI_4/KernelBL[3].KB/_old_tmp_2<18> ),
    .S(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<14>_437 ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<14>_422 )
  );
  XORCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_xor<14>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<13>_421 ),
    .LI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<14>_437 ),
    .O(\XLXI_4/KernelBL[3].KB/value_add0000<14> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<15>  (
    .I0(\XLXI_4/KernelBL[3].KB/test<15> ),
    .I1(\XLXI_4/KernelBL[3].KB/_old_tmp_2<19> ),
    .O(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<15>_438 )
  );
  XORCY   \XLXI_4/KernelBL[3].KB/Madd_value_add0000_xor<15>  (
    .CI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_cy<14>_422 ),
    .LI(\XLXI_4/KernelBL[3].KB/Madd_value_add0000_lut<15>_438 ),
    .O(\XLXI_4/KernelBL[3].KB/value_add0000<15> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/test_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value<0> ),
    .Q(\XLXI_4/KernelBL[4].KB/test<0> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/test_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value<1> ),
    .Q(\XLXI_4/KernelBL[4].KB/test<1> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/test_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value<2> ),
    .Q(\XLXI_4/KernelBL[4].KB/test<2> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/test_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value<3> ),
    .Q(\XLXI_4/KernelBL[4].KB/test<3> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/test_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value<4> ),
    .Q(\XLXI_4/KernelBL[4].KB/test<4> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/test_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value<5> ),
    .Q(\XLXI_4/KernelBL[4].KB/test<5> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/test_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value<6> ),
    .Q(\XLXI_4/KernelBL[4].KB/test<6> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/test_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value<7> ),
    .Q(\XLXI_4/KernelBL[4].KB/test<7> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/test_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value<8> ),
    .Q(\XLXI_4/KernelBL[4].KB/test<8> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/test_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value<9> ),
    .Q(\XLXI_4/KernelBL[4].KB/test<9> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/test_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value<10> ),
    .Q(\XLXI_4/KernelBL[4].KB/test<10> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/test_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value<11> ),
    .Q(\XLXI_4/KernelBL[4].KB/test<11> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/test_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value<12> ),
    .Q(\XLXI_4/KernelBL[4].KB/test<12> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/test_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value<13> ),
    .Q(\XLXI_4/KernelBL[4].KB/test<13> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/test_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value<14> ),
    .Q(\XLXI_4/KernelBL[4].KB/test<14> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/test_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[3].KB/value<15> ),
    .Q(\XLXI_4/KernelBL[4].KB/test<15> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/value_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value_add0000<0> ),
    .Q(\XLXI_4/KernelBL[4].KB/value<0> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/value_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value_add0000<1> ),
    .Q(\XLXI_4/KernelBL[4].KB/value<1> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/value_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value_add0000<2> ),
    .Q(\XLXI_4/KernelBL[4].KB/value<2> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/value_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value_add0000<3> ),
    .Q(\XLXI_4/KernelBL[4].KB/value<3> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/value_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value_add0000<4> ),
    .Q(\XLXI_4/KernelBL[4].KB/value<4> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/value_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value_add0000<5> ),
    .Q(\XLXI_4/KernelBL[4].KB/value<5> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/value_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value_add0000<6> ),
    .Q(\XLXI_4/KernelBL[4].KB/value<6> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/value_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value_add0000<7> ),
    .Q(\XLXI_4/KernelBL[4].KB/value<7> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/value_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value_add0000<8> ),
    .Q(\XLXI_4/KernelBL[4].KB/value<8> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/value_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value_add0000<9> ),
    .Q(\XLXI_4/KernelBL[4].KB/value<9> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/value_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value_add0000<10> ),
    .Q(\XLXI_4/KernelBL[4].KB/value<10> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/value_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value_add0000<11> ),
    .Q(\XLXI_4/KernelBL[4].KB/value<11> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/value_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value_add0000<12> ),
    .Q(\XLXI_4/KernelBL[4].KB/value<12> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/value_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value_add0000<13> ),
    .Q(\XLXI_4/KernelBL[4].KB/value<13> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/value_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value_add0000<14> ),
    .Q(\XLXI_4/KernelBL[4].KB/value<14> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[4].KB/value_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value_add0000<15> ),
    .Q(\XLXI_4/KernelBL[4].KB/value<15> )
  );
  MULT18X18SIO #(
    .B_INPUT ( "DIRECT" ),
    .AREG ( 0 ),
    .BREG ( 0 ),
    .PREG ( 0 ))
  \XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2  (
    .CEA(N0),
    .CEB(N0),
    .CEP(N0),
    .CLK(N0),
    .RSTA(N0),
    .RSTB(N0),
    .RSTP(N0),
    .A({N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N1}),
    .B({N0, N0, \XLXI_5/Numb [11], \XLXI_5/Numb [10], \XLXI_5/Numb [9], \XLXI_5/Numb [8], \XLXI_5/Numb [7], \XLXI_5/Numb [6], \XLXI_5/Numb [5], 
\XLXI_5/Numb [4], \XLXI_5/Numb [3], \XLXI_5/Numb [2], \XLXI_5/Numb [1], \XLXI_5/Numb [0], N0, N0, N0, N0}),
    .BCIN({\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED }),
    .P({\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED , 
\XLXI_4/KernelBL[4].KB/_old_tmp_2<19> , \XLXI_4/KernelBL[4].KB/_old_tmp_2<18> , \XLXI_4/KernelBL[4].KB/_old_tmp_2<17> , 
\XLXI_4/KernelBL[4].KB/_old_tmp_2<16> , \XLXI_4/KernelBL[4].KB/_old_tmp_2<15> , \XLXI_4/KernelBL[4].KB/_old_tmp_2<14> , 
\XLXI_4/KernelBL[4].KB/_old_tmp_2<13> , \XLXI_4/KernelBL[4].KB/_old_tmp_2<12> , \XLXI_4/KernelBL[4].KB/_old_tmp_2<11> , 
\XLXI_4/KernelBL[4].KB/_old_tmp_2<10> , \XLXI_4/KernelBL[4].KB/_old_tmp_2<9> , \XLXI_4/KernelBL[4].KB/_old_tmp_2<8> , 
\XLXI_4/KernelBL[4].KB/_old_tmp_2<7> , \XLXI_4/KernelBL[4].KB/_old_tmp_2<6> , \XLXI_4/KernelBL[4].KB/_old_tmp_2<5> , 
\XLXI_4/KernelBL[4].KB/_old_tmp_2<4> , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED }),
    .BCOUT({\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[4].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED })
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<0>  (
    .I0(\XLXI_4/KernelBL[4].KB/test<0> ),
    .I1(\XLXI_4/KernelBL[4].KB/_old_tmp_2<4> ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<0>_527 )
  );
  MUXCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<0>  (
    .CI(N0),
    .DI(\XLXI_4/KernelBL[4].KB/_old_tmp_2<4> ),
    .S(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<0>_527 ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<0>_512 )
  );
  XORCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_xor<0>  (
    .CI(N0),
    .LI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<0>_527 ),
    .O(\XLXI_4/KernelBL[4].KB/value_add0000<0> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<1>  (
    .I0(\XLXI_4/KernelBL[4].KB/test<1> ),
    .I1(\XLXI_4/KernelBL[4].KB/_old_tmp_2<5> ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<1>_534 )
  );
  MUXCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<1>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<0>_512 ),
    .DI(\XLXI_4/KernelBL[4].KB/_old_tmp_2<5> ),
    .S(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<1>_534 ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<1>_518 )
  );
  XORCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_xor<1>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<0>_512 ),
    .LI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<1>_534 ),
    .O(\XLXI_4/KernelBL[4].KB/value_add0000<1> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<2>  (
    .I0(\XLXI_4/KernelBL[4].KB/test<2> ),
    .I1(\XLXI_4/KernelBL[4].KB/_old_tmp_2<6> ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<2>_535 )
  );
  MUXCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<2>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<1>_518 ),
    .DI(\XLXI_4/KernelBL[4].KB/_old_tmp_2<6> ),
    .S(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<2>_535 ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<2>_519 )
  );
  XORCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_xor<2>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<1>_518 ),
    .LI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<2>_535 ),
    .O(\XLXI_4/KernelBL[4].KB/value_add0000<2> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<3>  (
    .I0(\XLXI_4/KernelBL[4].KB/test<3> ),
    .I1(\XLXI_4/KernelBL[4].KB/_old_tmp_2<7> ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<3>_536 )
  );
  MUXCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<3>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<2>_519 ),
    .DI(\XLXI_4/KernelBL[4].KB/_old_tmp_2<7> ),
    .S(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<3>_536 ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<3>_520 )
  );
  XORCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_xor<3>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<2>_519 ),
    .LI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<3>_536 ),
    .O(\XLXI_4/KernelBL[4].KB/value_add0000<3> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<4>  (
    .I0(\XLXI_4/KernelBL[4].KB/test<4> ),
    .I1(\XLXI_4/KernelBL[4].KB/_old_tmp_2<8> ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<4>_537 )
  );
  MUXCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<4>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<3>_520 ),
    .DI(\XLXI_4/KernelBL[4].KB/_old_tmp_2<8> ),
    .S(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<4>_537 ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<4>_521 )
  );
  XORCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_xor<4>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<3>_520 ),
    .LI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<4>_537 ),
    .O(\XLXI_4/KernelBL[4].KB/value_add0000<4> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<5>  (
    .I0(\XLXI_4/KernelBL[4].KB/test<5> ),
    .I1(\XLXI_4/KernelBL[4].KB/_old_tmp_2<9> ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<5>_538 )
  );
  MUXCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<5>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<4>_521 ),
    .DI(\XLXI_4/KernelBL[4].KB/_old_tmp_2<9> ),
    .S(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<5>_538 ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<5>_522 )
  );
  XORCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_xor<5>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<4>_521 ),
    .LI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<5>_538 ),
    .O(\XLXI_4/KernelBL[4].KB/value_add0000<5> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<6>  (
    .I0(\XLXI_4/KernelBL[4].KB/test<6> ),
    .I1(\XLXI_4/KernelBL[4].KB/_old_tmp_2<10> ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<6>_539 )
  );
  MUXCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<6>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<5>_522 ),
    .DI(\XLXI_4/KernelBL[4].KB/_old_tmp_2<10> ),
    .S(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<6>_539 ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<6>_523 )
  );
  XORCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_xor<6>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<5>_522 ),
    .LI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<6>_539 ),
    .O(\XLXI_4/KernelBL[4].KB/value_add0000<6> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<7>  (
    .I0(\XLXI_4/KernelBL[4].KB/test<7> ),
    .I1(\XLXI_4/KernelBL[4].KB/_old_tmp_2<11> ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<7>_540 )
  );
  MUXCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<7>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<6>_523 ),
    .DI(\XLXI_4/KernelBL[4].KB/_old_tmp_2<11> ),
    .S(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<7>_540 ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<7>_524 )
  );
  XORCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_xor<7>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<6>_523 ),
    .LI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<7>_540 ),
    .O(\XLXI_4/KernelBL[4].KB/value_add0000<7> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<8>  (
    .I0(\XLXI_4/KernelBL[4].KB/test<8> ),
    .I1(\XLXI_4/KernelBL[4].KB/_old_tmp_2<12> ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<8>_541 )
  );
  MUXCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<8>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<7>_524 ),
    .DI(\XLXI_4/KernelBL[4].KB/_old_tmp_2<12> ),
    .S(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<8>_541 ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<8>_525 )
  );
  XORCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_xor<8>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<7>_524 ),
    .LI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<8>_541 ),
    .O(\XLXI_4/KernelBL[4].KB/value_add0000<8> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<9>  (
    .I0(\XLXI_4/KernelBL[4].KB/test<9> ),
    .I1(\XLXI_4/KernelBL[4].KB/_old_tmp_2<13> ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<9>_542 )
  );
  MUXCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<9>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<8>_525 ),
    .DI(\XLXI_4/KernelBL[4].KB/_old_tmp_2<13> ),
    .S(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<9>_542 ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<9>_526 )
  );
  XORCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_xor<9>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<8>_525 ),
    .LI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<9>_542 ),
    .O(\XLXI_4/KernelBL[4].KB/value_add0000<9> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<10>  (
    .I0(\XLXI_4/KernelBL[4].KB/test<10> ),
    .I1(\XLXI_4/KernelBL[4].KB/_old_tmp_2<14> ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<10>_528 )
  );
  MUXCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<10>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<9>_526 ),
    .DI(\XLXI_4/KernelBL[4].KB/_old_tmp_2<14> ),
    .S(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<10>_528 ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<10>_513 )
  );
  XORCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_xor<10>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<9>_526 ),
    .LI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<10>_528 ),
    .O(\XLXI_4/KernelBL[4].KB/value_add0000<10> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<11>  (
    .I0(\XLXI_4/KernelBL[4].KB/test<11> ),
    .I1(\XLXI_4/KernelBL[4].KB/_old_tmp_2<15> ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<11>_529 )
  );
  MUXCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<11>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<10>_513 ),
    .DI(\XLXI_4/KernelBL[4].KB/_old_tmp_2<15> ),
    .S(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<11>_529 ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<11>_514 )
  );
  XORCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_xor<11>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<10>_513 ),
    .LI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<11>_529 ),
    .O(\XLXI_4/KernelBL[4].KB/value_add0000<11> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<12>  (
    .I0(\XLXI_4/KernelBL[4].KB/test<12> ),
    .I1(\XLXI_4/KernelBL[4].KB/_old_tmp_2<16> ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<12>_530 )
  );
  MUXCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<12>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<11>_514 ),
    .DI(\XLXI_4/KernelBL[4].KB/_old_tmp_2<16> ),
    .S(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<12>_530 ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<12>_515 )
  );
  XORCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_xor<12>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<11>_514 ),
    .LI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<12>_530 ),
    .O(\XLXI_4/KernelBL[4].KB/value_add0000<12> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<13>  (
    .I0(\XLXI_4/KernelBL[4].KB/test<13> ),
    .I1(\XLXI_4/KernelBL[4].KB/_old_tmp_2<17> ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<13>_531 )
  );
  MUXCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<13>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<12>_515 ),
    .DI(\XLXI_4/KernelBL[4].KB/_old_tmp_2<17> ),
    .S(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<13>_531 ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<13>_516 )
  );
  XORCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_xor<13>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<12>_515 ),
    .LI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<13>_531 ),
    .O(\XLXI_4/KernelBL[4].KB/value_add0000<13> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<14>  (
    .I0(\XLXI_4/KernelBL[4].KB/test<14> ),
    .I1(\XLXI_4/KernelBL[4].KB/_old_tmp_2<18> ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<14>_532 )
  );
  MUXCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<14>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<13>_516 ),
    .DI(\XLXI_4/KernelBL[4].KB/_old_tmp_2<18> ),
    .S(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<14>_532 ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<14>_517 )
  );
  XORCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_xor<14>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<13>_516 ),
    .LI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<14>_532 ),
    .O(\XLXI_4/KernelBL[4].KB/value_add0000<14> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<15>  (
    .I0(\XLXI_4/KernelBL[4].KB/test<15> ),
    .I1(\XLXI_4/KernelBL[4].KB/_old_tmp_2<19> ),
    .O(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<15>_533 )
  );
  XORCY   \XLXI_4/KernelBL[4].KB/Madd_value_add0000_xor<15>  (
    .CI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_cy<14>_517 ),
    .LI(\XLXI_4/KernelBL[4].KB/Madd_value_add0000_lut<15>_533 ),
    .O(\XLXI_4/KernelBL[4].KB/value_add0000<15> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/test_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value<0> ),
    .Q(\XLXI_4/KernelBL[5].KB/test<0> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/test_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value<1> ),
    .Q(\XLXI_4/KernelBL[5].KB/test<1> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/test_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value<2> ),
    .Q(\XLXI_4/KernelBL[5].KB/test<2> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/test_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value<3> ),
    .Q(\XLXI_4/KernelBL[5].KB/test<3> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/test_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value<4> ),
    .Q(\XLXI_4/KernelBL[5].KB/test<4> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/test_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value<5> ),
    .Q(\XLXI_4/KernelBL[5].KB/test<5> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/test_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value<6> ),
    .Q(\XLXI_4/KernelBL[5].KB/test<6> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/test_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value<7> ),
    .Q(\XLXI_4/KernelBL[5].KB/test<7> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/test_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value<8> ),
    .Q(\XLXI_4/KernelBL[5].KB/test<8> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/test_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value<9> ),
    .Q(\XLXI_4/KernelBL[5].KB/test<9> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/test_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value<10> ),
    .Q(\XLXI_4/KernelBL[5].KB/test<10> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/test_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value<11> ),
    .Q(\XLXI_4/KernelBL[5].KB/test<11> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/test_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value<12> ),
    .Q(\XLXI_4/KernelBL[5].KB/test<12> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/test_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value<13> ),
    .Q(\XLXI_4/KernelBL[5].KB/test<13> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/test_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value<14> ),
    .Q(\XLXI_4/KernelBL[5].KB/test<14> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/test_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[4].KB/value<15> ),
    .Q(\XLXI_4/KernelBL[5].KB/test<15> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/value_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value_add0000<0> ),
    .Q(\XLXI_4/KernelBL[5].KB/value<0> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/value_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value_add0000<1> ),
    .Q(\XLXI_4/KernelBL[5].KB/value<1> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/value_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value_add0000<2> ),
    .Q(\XLXI_4/KernelBL[5].KB/value<2> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/value_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value_add0000<3> ),
    .Q(\XLXI_4/KernelBL[5].KB/value<3> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/value_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value_add0000<4> ),
    .Q(\XLXI_4/KernelBL[5].KB/value<4> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/value_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value_add0000<5> ),
    .Q(\XLXI_4/KernelBL[5].KB/value<5> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/value_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value_add0000<6> ),
    .Q(\XLXI_4/KernelBL[5].KB/value<6> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/value_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value_add0000<7> ),
    .Q(\XLXI_4/KernelBL[5].KB/value<7> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/value_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value_add0000<8> ),
    .Q(\XLXI_4/KernelBL[5].KB/value<8> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/value_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value_add0000<9> ),
    .Q(\XLXI_4/KernelBL[5].KB/value<9> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/value_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value_add0000<10> ),
    .Q(\XLXI_4/KernelBL[5].KB/value<10> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/value_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value_add0000<11> ),
    .Q(\XLXI_4/KernelBL[5].KB/value<11> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/value_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value_add0000<12> ),
    .Q(\XLXI_4/KernelBL[5].KB/value<12> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/value_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value_add0000<13> ),
    .Q(\XLXI_4/KernelBL[5].KB/value<13> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/value_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value_add0000<14> ),
    .Q(\XLXI_4/KernelBL[5].KB/value<14> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[5].KB/value_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value_add0000<15> ),
    .Q(\XLXI_4/KernelBL[5].KB/value<15> )
  );
  MULT18X18SIO #(
    .B_INPUT ( "DIRECT" ),
    .AREG ( 0 ),
    .BREG ( 0 ),
    .PREG ( 0 ))
  \XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2  (
    .CEA(N0),
    .CEB(N0),
    .CEP(N0),
    .CLK(N0),
    .RSTA(N0),
    .RSTB(N0),
    .RSTP(N0),
    .A({N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N1}),
    .B({N0, N0, \XLXI_5/Numb [11], \XLXI_5/Numb [10], \XLXI_5/Numb [9], \XLXI_5/Numb [8], \XLXI_5/Numb [7], \XLXI_5/Numb [6], \XLXI_5/Numb [5], 
\XLXI_5/Numb [4], \XLXI_5/Numb [3], \XLXI_5/Numb [2], \XLXI_5/Numb [1], \XLXI_5/Numb [0], N0, N0, N0, N0}),
    .BCIN({\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED }),
    .P({\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED , 
\XLXI_4/KernelBL[5].KB/_old_tmp_2<19> , \XLXI_4/KernelBL[5].KB/_old_tmp_2<18> , \XLXI_4/KernelBL[5].KB/_old_tmp_2<17> , 
\XLXI_4/KernelBL[5].KB/_old_tmp_2<16> , \XLXI_4/KernelBL[5].KB/_old_tmp_2<15> , \XLXI_4/KernelBL[5].KB/_old_tmp_2<14> , 
\XLXI_4/KernelBL[5].KB/_old_tmp_2<13> , \XLXI_4/KernelBL[5].KB/_old_tmp_2<12> , \XLXI_4/KernelBL[5].KB/_old_tmp_2<11> , 
\XLXI_4/KernelBL[5].KB/_old_tmp_2<10> , \XLXI_4/KernelBL[5].KB/_old_tmp_2<9> , \XLXI_4/KernelBL[5].KB/_old_tmp_2<8> , 
\XLXI_4/KernelBL[5].KB/_old_tmp_2<7> , \XLXI_4/KernelBL[5].KB/_old_tmp_2<6> , \XLXI_4/KernelBL[5].KB/_old_tmp_2<5> , 
\XLXI_4/KernelBL[5].KB/_old_tmp_2<4> , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED }),
    .BCOUT({\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[5].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED })
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<0>  (
    .I0(\XLXI_4/KernelBL[5].KB/test<0> ),
    .I1(\XLXI_4/KernelBL[5].KB/_old_tmp_2<4> ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<0>_622 )
  );
  MUXCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<0>  (
    .CI(N0),
    .DI(\XLXI_4/KernelBL[5].KB/_old_tmp_2<4> ),
    .S(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<0>_622 ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<0>_607 )
  );
  XORCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_xor<0>  (
    .CI(N0),
    .LI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<0>_622 ),
    .O(\XLXI_4/KernelBL[5].KB/value_add0000<0> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<1>  (
    .I0(\XLXI_4/KernelBL[5].KB/test<1> ),
    .I1(\XLXI_4/KernelBL[5].KB/_old_tmp_2<5> ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<1>_629 )
  );
  MUXCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<1>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<0>_607 ),
    .DI(\XLXI_4/KernelBL[5].KB/_old_tmp_2<5> ),
    .S(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<1>_629 ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<1>_613 )
  );
  XORCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_xor<1>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<0>_607 ),
    .LI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<1>_629 ),
    .O(\XLXI_4/KernelBL[5].KB/value_add0000<1> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<2>  (
    .I0(\XLXI_4/KernelBL[5].KB/test<2> ),
    .I1(\XLXI_4/KernelBL[5].KB/_old_tmp_2<6> ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<2>_630 )
  );
  MUXCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<2>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<1>_613 ),
    .DI(\XLXI_4/KernelBL[5].KB/_old_tmp_2<6> ),
    .S(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<2>_630 ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<2>_614 )
  );
  XORCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_xor<2>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<1>_613 ),
    .LI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<2>_630 ),
    .O(\XLXI_4/KernelBL[5].KB/value_add0000<2> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<3>  (
    .I0(\XLXI_4/KernelBL[5].KB/test<3> ),
    .I1(\XLXI_4/KernelBL[5].KB/_old_tmp_2<7> ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<3>_631 )
  );
  MUXCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<3>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<2>_614 ),
    .DI(\XLXI_4/KernelBL[5].KB/_old_tmp_2<7> ),
    .S(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<3>_631 ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<3>_615 )
  );
  XORCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_xor<3>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<2>_614 ),
    .LI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<3>_631 ),
    .O(\XLXI_4/KernelBL[5].KB/value_add0000<3> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<4>  (
    .I0(\XLXI_4/KernelBL[5].KB/test<4> ),
    .I1(\XLXI_4/KernelBL[5].KB/_old_tmp_2<8> ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<4>_632 )
  );
  MUXCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<4>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<3>_615 ),
    .DI(\XLXI_4/KernelBL[5].KB/_old_tmp_2<8> ),
    .S(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<4>_632 ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<4>_616 )
  );
  XORCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_xor<4>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<3>_615 ),
    .LI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<4>_632 ),
    .O(\XLXI_4/KernelBL[5].KB/value_add0000<4> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<5>  (
    .I0(\XLXI_4/KernelBL[5].KB/test<5> ),
    .I1(\XLXI_4/KernelBL[5].KB/_old_tmp_2<9> ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<5>_633 )
  );
  MUXCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<5>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<4>_616 ),
    .DI(\XLXI_4/KernelBL[5].KB/_old_tmp_2<9> ),
    .S(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<5>_633 ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<5>_617 )
  );
  XORCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_xor<5>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<4>_616 ),
    .LI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<5>_633 ),
    .O(\XLXI_4/KernelBL[5].KB/value_add0000<5> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<6>  (
    .I0(\XLXI_4/KernelBL[5].KB/test<6> ),
    .I1(\XLXI_4/KernelBL[5].KB/_old_tmp_2<10> ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<6>_634 )
  );
  MUXCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<6>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<5>_617 ),
    .DI(\XLXI_4/KernelBL[5].KB/_old_tmp_2<10> ),
    .S(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<6>_634 ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<6>_618 )
  );
  XORCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_xor<6>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<5>_617 ),
    .LI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<6>_634 ),
    .O(\XLXI_4/KernelBL[5].KB/value_add0000<6> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<7>  (
    .I0(\XLXI_4/KernelBL[5].KB/test<7> ),
    .I1(\XLXI_4/KernelBL[5].KB/_old_tmp_2<11> ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<7>_635 )
  );
  MUXCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<7>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<6>_618 ),
    .DI(\XLXI_4/KernelBL[5].KB/_old_tmp_2<11> ),
    .S(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<7>_635 ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<7>_619 )
  );
  XORCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_xor<7>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<6>_618 ),
    .LI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<7>_635 ),
    .O(\XLXI_4/KernelBL[5].KB/value_add0000<7> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<8>  (
    .I0(\XLXI_4/KernelBL[5].KB/test<8> ),
    .I1(\XLXI_4/KernelBL[5].KB/_old_tmp_2<12> ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<8>_636 )
  );
  MUXCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<8>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<7>_619 ),
    .DI(\XLXI_4/KernelBL[5].KB/_old_tmp_2<12> ),
    .S(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<8>_636 ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<8>_620 )
  );
  XORCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_xor<8>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<7>_619 ),
    .LI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<8>_636 ),
    .O(\XLXI_4/KernelBL[5].KB/value_add0000<8> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<9>  (
    .I0(\XLXI_4/KernelBL[5].KB/test<9> ),
    .I1(\XLXI_4/KernelBL[5].KB/_old_tmp_2<13> ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<9>_637 )
  );
  MUXCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<9>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<8>_620 ),
    .DI(\XLXI_4/KernelBL[5].KB/_old_tmp_2<13> ),
    .S(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<9>_637 ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<9>_621 )
  );
  XORCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_xor<9>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<8>_620 ),
    .LI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<9>_637 ),
    .O(\XLXI_4/KernelBL[5].KB/value_add0000<9> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<10>  (
    .I0(\XLXI_4/KernelBL[5].KB/test<10> ),
    .I1(\XLXI_4/KernelBL[5].KB/_old_tmp_2<14> ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<10>_623 )
  );
  MUXCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<10>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<9>_621 ),
    .DI(\XLXI_4/KernelBL[5].KB/_old_tmp_2<14> ),
    .S(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<10>_623 ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<10>_608 )
  );
  XORCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_xor<10>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<9>_621 ),
    .LI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<10>_623 ),
    .O(\XLXI_4/KernelBL[5].KB/value_add0000<10> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<11>  (
    .I0(\XLXI_4/KernelBL[5].KB/test<11> ),
    .I1(\XLXI_4/KernelBL[5].KB/_old_tmp_2<15> ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<11>_624 )
  );
  MUXCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<11>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<10>_608 ),
    .DI(\XLXI_4/KernelBL[5].KB/_old_tmp_2<15> ),
    .S(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<11>_624 ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<11>_609 )
  );
  XORCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_xor<11>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<10>_608 ),
    .LI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<11>_624 ),
    .O(\XLXI_4/KernelBL[5].KB/value_add0000<11> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<12>  (
    .I0(\XLXI_4/KernelBL[5].KB/test<12> ),
    .I1(\XLXI_4/KernelBL[5].KB/_old_tmp_2<16> ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<12>_625 )
  );
  MUXCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<12>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<11>_609 ),
    .DI(\XLXI_4/KernelBL[5].KB/_old_tmp_2<16> ),
    .S(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<12>_625 ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<12>_610 )
  );
  XORCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_xor<12>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<11>_609 ),
    .LI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<12>_625 ),
    .O(\XLXI_4/KernelBL[5].KB/value_add0000<12> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<13>  (
    .I0(\XLXI_4/KernelBL[5].KB/test<13> ),
    .I1(\XLXI_4/KernelBL[5].KB/_old_tmp_2<17> ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<13>_626 )
  );
  MUXCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<13>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<12>_610 ),
    .DI(\XLXI_4/KernelBL[5].KB/_old_tmp_2<17> ),
    .S(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<13>_626 ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<13>_611 )
  );
  XORCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_xor<13>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<12>_610 ),
    .LI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<13>_626 ),
    .O(\XLXI_4/KernelBL[5].KB/value_add0000<13> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<14>  (
    .I0(\XLXI_4/KernelBL[5].KB/test<14> ),
    .I1(\XLXI_4/KernelBL[5].KB/_old_tmp_2<18> ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<14>_627 )
  );
  MUXCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<14>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<13>_611 ),
    .DI(\XLXI_4/KernelBL[5].KB/_old_tmp_2<18> ),
    .S(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<14>_627 ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<14>_612 )
  );
  XORCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_xor<14>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<13>_611 ),
    .LI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<14>_627 ),
    .O(\XLXI_4/KernelBL[5].KB/value_add0000<14> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<15>  (
    .I0(\XLXI_4/KernelBL[5].KB/test<15> ),
    .I1(\XLXI_4/KernelBL[5].KB/_old_tmp_2<19> ),
    .O(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<15>_628 )
  );
  XORCY   \XLXI_4/KernelBL[5].KB/Madd_value_add0000_xor<15>  (
    .CI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_cy<14>_612 ),
    .LI(\XLXI_4/KernelBL[5].KB/Madd_value_add0000_lut<15>_628 ),
    .O(\XLXI_4/KernelBL[5].KB/value_add0000<15> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/test_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value<0> ),
    .Q(\XLXI_4/KernelBL[6].KB/test<0> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/test_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value<1> ),
    .Q(\XLXI_4/KernelBL[6].KB/test<1> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/test_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value<2> ),
    .Q(\XLXI_4/KernelBL[6].KB/test<2> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/test_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value<3> ),
    .Q(\XLXI_4/KernelBL[6].KB/test<3> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/test_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value<4> ),
    .Q(\XLXI_4/KernelBL[6].KB/test<4> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/test_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value<5> ),
    .Q(\XLXI_4/KernelBL[6].KB/test<5> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/test_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value<6> ),
    .Q(\XLXI_4/KernelBL[6].KB/test<6> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/test_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value<7> ),
    .Q(\XLXI_4/KernelBL[6].KB/test<7> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/test_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value<8> ),
    .Q(\XLXI_4/KernelBL[6].KB/test<8> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/test_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value<9> ),
    .Q(\XLXI_4/KernelBL[6].KB/test<9> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/test_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value<10> ),
    .Q(\XLXI_4/KernelBL[6].KB/test<10> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/test_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value<11> ),
    .Q(\XLXI_4/KernelBL[6].KB/test<11> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/test_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value<12> ),
    .Q(\XLXI_4/KernelBL[6].KB/test<12> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/test_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value<13> ),
    .Q(\XLXI_4/KernelBL[6].KB/test<13> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/test_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value<14> ),
    .Q(\XLXI_4/KernelBL[6].KB/test<14> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/test_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[5].KB/value<15> ),
    .Q(\XLXI_4/KernelBL[6].KB/test<15> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/value_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value_add0000<0> ),
    .Q(\XLXI_4/KernelBL[6].KB/value<0> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/value_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value_add0000<1> ),
    .Q(\XLXI_4/KernelBL[6].KB/value<1> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/value_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value_add0000<2> ),
    .Q(\XLXI_4/KernelBL[6].KB/value<2> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/value_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value_add0000<3> ),
    .Q(\XLXI_4/KernelBL[6].KB/value<3> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/value_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value_add0000<4> ),
    .Q(\XLXI_4/KernelBL[6].KB/value<4> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/value_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value_add0000<5> ),
    .Q(\XLXI_4/KernelBL[6].KB/value<5> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/value_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value_add0000<6> ),
    .Q(\XLXI_4/KernelBL[6].KB/value<6> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/value_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value_add0000<7> ),
    .Q(\XLXI_4/KernelBL[6].KB/value<7> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/value_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value_add0000<8> ),
    .Q(\XLXI_4/KernelBL[6].KB/value<8> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/value_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value_add0000<9> ),
    .Q(\XLXI_4/KernelBL[6].KB/value<9> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/value_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value_add0000<10> ),
    .Q(\XLXI_4/KernelBL[6].KB/value<10> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/value_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value_add0000<11> ),
    .Q(\XLXI_4/KernelBL[6].KB/value<11> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/value_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value_add0000<12> ),
    .Q(\XLXI_4/KernelBL[6].KB/value<12> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/value_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value_add0000<13> ),
    .Q(\XLXI_4/KernelBL[6].KB/value<13> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/value_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value_add0000<14> ),
    .Q(\XLXI_4/KernelBL[6].KB/value<14> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[6].KB/value_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value_add0000<15> ),
    .Q(\XLXI_4/KernelBL[6].KB/value<15> )
  );
  MULT18X18SIO #(
    .B_INPUT ( "DIRECT" ),
    .AREG ( 0 ),
    .BREG ( 0 ),
    .PREG ( 0 ))
  \XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2  (
    .CEA(N0),
    .CEB(N0),
    .CEP(N0),
    .CLK(N0),
    .RSTA(N0),
    .RSTB(N0),
    .RSTP(N0),
    .A({N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N1}),
    .B({N0, N0, \XLXI_5/Numb [11], \XLXI_5/Numb [10], \XLXI_5/Numb [9], \XLXI_5/Numb [8], \XLXI_5/Numb [7], \XLXI_5/Numb [6], \XLXI_5/Numb [5], 
\XLXI_5/Numb [4], \XLXI_5/Numb [3], \XLXI_5/Numb [2], \XLXI_5/Numb [1], \XLXI_5/Numb [0], N0, N0, N0, N0}),
    .BCIN({\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED }),
    .P({\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED , 
\XLXI_4/KernelBL[6].KB/_old_tmp_2<19> , \XLXI_4/KernelBL[6].KB/_old_tmp_2<18> , \XLXI_4/KernelBL[6].KB/_old_tmp_2<17> , 
\XLXI_4/KernelBL[6].KB/_old_tmp_2<16> , \XLXI_4/KernelBL[6].KB/_old_tmp_2<15> , \XLXI_4/KernelBL[6].KB/_old_tmp_2<14> , 
\XLXI_4/KernelBL[6].KB/_old_tmp_2<13> , \XLXI_4/KernelBL[6].KB/_old_tmp_2<12> , \XLXI_4/KernelBL[6].KB/_old_tmp_2<11> , 
\XLXI_4/KernelBL[6].KB/_old_tmp_2<10> , \XLXI_4/KernelBL[6].KB/_old_tmp_2<9> , \XLXI_4/KernelBL[6].KB/_old_tmp_2<8> , 
\XLXI_4/KernelBL[6].KB/_old_tmp_2<7> , \XLXI_4/KernelBL[6].KB/_old_tmp_2<6> , \XLXI_4/KernelBL[6].KB/_old_tmp_2<5> , 
\XLXI_4/KernelBL[6].KB/_old_tmp_2<4> , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED }),
    .BCOUT({\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[6].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED })
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<0>  (
    .I0(\XLXI_4/KernelBL[6].KB/test<0> ),
    .I1(\XLXI_4/KernelBL[6].KB/_old_tmp_2<4> ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<0>_717 )
  );
  MUXCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<0>  (
    .CI(N0),
    .DI(\XLXI_4/KernelBL[6].KB/_old_tmp_2<4> ),
    .S(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<0>_717 ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<0>_702 )
  );
  XORCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_xor<0>  (
    .CI(N0),
    .LI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<0>_717 ),
    .O(\XLXI_4/KernelBL[6].KB/value_add0000<0> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<1>  (
    .I0(\XLXI_4/KernelBL[6].KB/test<1> ),
    .I1(\XLXI_4/KernelBL[6].KB/_old_tmp_2<5> ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<1>_724 )
  );
  MUXCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<1>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<0>_702 ),
    .DI(\XLXI_4/KernelBL[6].KB/_old_tmp_2<5> ),
    .S(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<1>_724 ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<1>_708 )
  );
  XORCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_xor<1>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<0>_702 ),
    .LI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<1>_724 ),
    .O(\XLXI_4/KernelBL[6].KB/value_add0000<1> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<2>  (
    .I0(\XLXI_4/KernelBL[6].KB/test<2> ),
    .I1(\XLXI_4/KernelBL[6].KB/_old_tmp_2<6> ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<2>_725 )
  );
  MUXCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<2>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<1>_708 ),
    .DI(\XLXI_4/KernelBL[6].KB/_old_tmp_2<6> ),
    .S(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<2>_725 ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<2>_709 )
  );
  XORCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_xor<2>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<1>_708 ),
    .LI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<2>_725 ),
    .O(\XLXI_4/KernelBL[6].KB/value_add0000<2> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<3>  (
    .I0(\XLXI_4/KernelBL[6].KB/test<3> ),
    .I1(\XLXI_4/KernelBL[6].KB/_old_tmp_2<7> ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<3>_726 )
  );
  MUXCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<3>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<2>_709 ),
    .DI(\XLXI_4/KernelBL[6].KB/_old_tmp_2<7> ),
    .S(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<3>_726 ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<3>_710 )
  );
  XORCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_xor<3>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<2>_709 ),
    .LI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<3>_726 ),
    .O(\XLXI_4/KernelBL[6].KB/value_add0000<3> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<4>  (
    .I0(\XLXI_4/KernelBL[6].KB/test<4> ),
    .I1(\XLXI_4/KernelBL[6].KB/_old_tmp_2<8> ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<4>_727 )
  );
  MUXCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<4>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<3>_710 ),
    .DI(\XLXI_4/KernelBL[6].KB/_old_tmp_2<8> ),
    .S(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<4>_727 ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<4>_711 )
  );
  XORCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_xor<4>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<3>_710 ),
    .LI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<4>_727 ),
    .O(\XLXI_4/KernelBL[6].KB/value_add0000<4> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<5>  (
    .I0(\XLXI_4/KernelBL[6].KB/test<5> ),
    .I1(\XLXI_4/KernelBL[6].KB/_old_tmp_2<9> ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<5>_728 )
  );
  MUXCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<5>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<4>_711 ),
    .DI(\XLXI_4/KernelBL[6].KB/_old_tmp_2<9> ),
    .S(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<5>_728 ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<5>_712 )
  );
  XORCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_xor<5>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<4>_711 ),
    .LI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<5>_728 ),
    .O(\XLXI_4/KernelBL[6].KB/value_add0000<5> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<6>  (
    .I0(\XLXI_4/KernelBL[6].KB/test<6> ),
    .I1(\XLXI_4/KernelBL[6].KB/_old_tmp_2<10> ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<6>_729 )
  );
  MUXCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<6>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<5>_712 ),
    .DI(\XLXI_4/KernelBL[6].KB/_old_tmp_2<10> ),
    .S(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<6>_729 ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<6>_713 )
  );
  XORCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_xor<6>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<5>_712 ),
    .LI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<6>_729 ),
    .O(\XLXI_4/KernelBL[6].KB/value_add0000<6> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<7>  (
    .I0(\XLXI_4/KernelBL[6].KB/test<7> ),
    .I1(\XLXI_4/KernelBL[6].KB/_old_tmp_2<11> ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<7>_730 )
  );
  MUXCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<7>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<6>_713 ),
    .DI(\XLXI_4/KernelBL[6].KB/_old_tmp_2<11> ),
    .S(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<7>_730 ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<7>_714 )
  );
  XORCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_xor<7>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<6>_713 ),
    .LI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<7>_730 ),
    .O(\XLXI_4/KernelBL[6].KB/value_add0000<7> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<8>  (
    .I0(\XLXI_4/KernelBL[6].KB/test<8> ),
    .I1(\XLXI_4/KernelBL[6].KB/_old_tmp_2<12> ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<8>_731 )
  );
  MUXCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<8>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<7>_714 ),
    .DI(\XLXI_4/KernelBL[6].KB/_old_tmp_2<12> ),
    .S(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<8>_731 ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<8>_715 )
  );
  XORCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_xor<8>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<7>_714 ),
    .LI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<8>_731 ),
    .O(\XLXI_4/KernelBL[6].KB/value_add0000<8> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<9>  (
    .I0(\XLXI_4/KernelBL[6].KB/test<9> ),
    .I1(\XLXI_4/KernelBL[6].KB/_old_tmp_2<13> ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<9>_732 )
  );
  MUXCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<9>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<8>_715 ),
    .DI(\XLXI_4/KernelBL[6].KB/_old_tmp_2<13> ),
    .S(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<9>_732 ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<9>_716 )
  );
  XORCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_xor<9>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<8>_715 ),
    .LI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<9>_732 ),
    .O(\XLXI_4/KernelBL[6].KB/value_add0000<9> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<10>  (
    .I0(\XLXI_4/KernelBL[6].KB/test<10> ),
    .I1(\XLXI_4/KernelBL[6].KB/_old_tmp_2<14> ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<10>_718 )
  );
  MUXCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<10>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<9>_716 ),
    .DI(\XLXI_4/KernelBL[6].KB/_old_tmp_2<14> ),
    .S(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<10>_718 ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<10>_703 )
  );
  XORCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_xor<10>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<9>_716 ),
    .LI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<10>_718 ),
    .O(\XLXI_4/KernelBL[6].KB/value_add0000<10> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<11>  (
    .I0(\XLXI_4/KernelBL[6].KB/test<11> ),
    .I1(\XLXI_4/KernelBL[6].KB/_old_tmp_2<15> ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<11>_719 )
  );
  MUXCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<11>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<10>_703 ),
    .DI(\XLXI_4/KernelBL[6].KB/_old_tmp_2<15> ),
    .S(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<11>_719 ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<11>_704 )
  );
  XORCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_xor<11>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<10>_703 ),
    .LI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<11>_719 ),
    .O(\XLXI_4/KernelBL[6].KB/value_add0000<11> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<12>  (
    .I0(\XLXI_4/KernelBL[6].KB/test<12> ),
    .I1(\XLXI_4/KernelBL[6].KB/_old_tmp_2<16> ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<12>_720 )
  );
  MUXCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<12>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<11>_704 ),
    .DI(\XLXI_4/KernelBL[6].KB/_old_tmp_2<16> ),
    .S(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<12>_720 ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<12>_705 )
  );
  XORCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_xor<12>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<11>_704 ),
    .LI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<12>_720 ),
    .O(\XLXI_4/KernelBL[6].KB/value_add0000<12> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<13>  (
    .I0(\XLXI_4/KernelBL[6].KB/test<13> ),
    .I1(\XLXI_4/KernelBL[6].KB/_old_tmp_2<17> ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<13>_721 )
  );
  MUXCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<13>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<12>_705 ),
    .DI(\XLXI_4/KernelBL[6].KB/_old_tmp_2<17> ),
    .S(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<13>_721 ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<13>_706 )
  );
  XORCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_xor<13>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<12>_705 ),
    .LI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<13>_721 ),
    .O(\XLXI_4/KernelBL[6].KB/value_add0000<13> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<14>  (
    .I0(\XLXI_4/KernelBL[6].KB/test<14> ),
    .I1(\XLXI_4/KernelBL[6].KB/_old_tmp_2<18> ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<14>_722 )
  );
  MUXCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<14>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<13>_706 ),
    .DI(\XLXI_4/KernelBL[6].KB/_old_tmp_2<18> ),
    .S(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<14>_722 ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<14>_707 )
  );
  XORCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_xor<14>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<13>_706 ),
    .LI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<14>_722 ),
    .O(\XLXI_4/KernelBL[6].KB/value_add0000<14> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<15>  (
    .I0(\XLXI_4/KernelBL[6].KB/test<15> ),
    .I1(\XLXI_4/KernelBL[6].KB/_old_tmp_2<19> ),
    .O(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<15>_723 )
  );
  XORCY   \XLXI_4/KernelBL[6].KB/Madd_value_add0000_xor<15>  (
    .CI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_cy<14>_707 ),
    .LI(\XLXI_4/KernelBL[6].KB/Madd_value_add0000_lut<15>_723 ),
    .O(\XLXI_4/KernelBL[6].KB/value_add0000<15> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/test_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value<0> ),
    .Q(\XLXI_4/KernelBL[7].KB/test<0> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/test_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value<1> ),
    .Q(\XLXI_4/KernelBL[7].KB/test<1> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/test_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value<2> ),
    .Q(\XLXI_4/KernelBL[7].KB/test<2> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/test_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value<3> ),
    .Q(\XLXI_4/KernelBL[7].KB/test<3> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/test_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value<4> ),
    .Q(\XLXI_4/KernelBL[7].KB/test<4> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/test_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value<5> ),
    .Q(\XLXI_4/KernelBL[7].KB/test<5> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/test_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value<6> ),
    .Q(\XLXI_4/KernelBL[7].KB/test<6> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/test_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value<7> ),
    .Q(\XLXI_4/KernelBL[7].KB/test<7> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/test_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value<8> ),
    .Q(\XLXI_4/KernelBL[7].KB/test<8> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/test_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value<9> ),
    .Q(\XLXI_4/KernelBL[7].KB/test<9> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/test_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value<10> ),
    .Q(\XLXI_4/KernelBL[7].KB/test<10> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/test_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value<11> ),
    .Q(\XLXI_4/KernelBL[7].KB/test<11> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/test_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value<12> ),
    .Q(\XLXI_4/KernelBL[7].KB/test<12> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/test_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value<13> ),
    .Q(\XLXI_4/KernelBL[7].KB/test<13> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/test_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value<14> ),
    .Q(\XLXI_4/KernelBL[7].KB/test<14> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/test_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[6].KB/value<15> ),
    .Q(\XLXI_4/KernelBL[7].KB/test<15> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/value_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value_add0000<0> ),
    .Q(\XLXI_4/KernelBL[7].KB/value<0> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/value_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value_add0000<1> ),
    .Q(\XLXI_4/KernelBL[7].KB/value<1> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/value_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value_add0000<2> ),
    .Q(\XLXI_4/KernelBL[7].KB/value<2> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/value_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value_add0000<3> ),
    .Q(\XLXI_4/KernelBL[7].KB/value<3> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/value_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value_add0000<4> ),
    .Q(\XLXI_4/KernelBL[7].KB/value<4> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/value_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value_add0000<5> ),
    .Q(\XLXI_4/KernelBL[7].KB/value<5> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/value_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value_add0000<6> ),
    .Q(\XLXI_4/KernelBL[7].KB/value<6> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/value_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value_add0000<7> ),
    .Q(\XLXI_4/KernelBL[7].KB/value<7> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/value_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value_add0000<8> ),
    .Q(\XLXI_4/KernelBL[7].KB/value<8> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/value_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value_add0000<9> ),
    .Q(\XLXI_4/KernelBL[7].KB/value<9> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/value_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value_add0000<10> ),
    .Q(\XLXI_4/KernelBL[7].KB/value<10> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/value_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value_add0000<11> ),
    .Q(\XLXI_4/KernelBL[7].KB/value<11> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/value_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value_add0000<12> ),
    .Q(\XLXI_4/KernelBL[7].KB/value<12> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/value_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value_add0000<13> ),
    .Q(\XLXI_4/KernelBL[7].KB/value<13> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/value_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value_add0000<14> ),
    .Q(\XLXI_4/KernelBL[7].KB/value<14> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[7].KB/value_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value_add0000<15> ),
    .Q(\XLXI_4/KernelBL[7].KB/value<15> )
  );
  MULT18X18SIO #(
    .B_INPUT ( "DIRECT" ),
    .AREG ( 0 ),
    .BREG ( 0 ),
    .PREG ( 0 ))
  \XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2  (
    .CEA(N0),
    .CEB(N0),
    .CEP(N0),
    .CLK(N0),
    .RSTA(N0),
    .RSTB(N0),
    .RSTP(N0),
    .A({N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N1}),
    .B({N0, N0, \XLXI_5/Numb [11], \XLXI_5/Numb [10], \XLXI_5/Numb [9], \XLXI_5/Numb [8], \XLXI_5/Numb [7], \XLXI_5/Numb [6], \XLXI_5/Numb [5], 
\XLXI_5/Numb [4], \XLXI_5/Numb [3], \XLXI_5/Numb [2], \XLXI_5/Numb [1], \XLXI_5/Numb [0], N0, N0, N0, N0}),
    .BCIN({\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED }),
    .P({\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED , 
\XLXI_4/KernelBL[7].KB/_old_tmp_2<19> , \XLXI_4/KernelBL[7].KB/_old_tmp_2<18> , \XLXI_4/KernelBL[7].KB/_old_tmp_2<17> , 
\XLXI_4/KernelBL[7].KB/_old_tmp_2<16> , \XLXI_4/KernelBL[7].KB/_old_tmp_2<15> , \XLXI_4/KernelBL[7].KB/_old_tmp_2<14> , 
\XLXI_4/KernelBL[7].KB/_old_tmp_2<13> , \XLXI_4/KernelBL[7].KB/_old_tmp_2<12> , \XLXI_4/KernelBL[7].KB/_old_tmp_2<11> , 
\XLXI_4/KernelBL[7].KB/_old_tmp_2<10> , \XLXI_4/KernelBL[7].KB/_old_tmp_2<9> , \XLXI_4/KernelBL[7].KB/_old_tmp_2<8> , 
\XLXI_4/KernelBL[7].KB/_old_tmp_2<7> , \XLXI_4/KernelBL[7].KB/_old_tmp_2<6> , \XLXI_4/KernelBL[7].KB/_old_tmp_2<5> , 
\XLXI_4/KernelBL[7].KB/_old_tmp_2<4> , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED }),
    .BCOUT({\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[7].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED })
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<0>  (
    .I0(\XLXI_4/KernelBL[7].KB/test<0> ),
    .I1(\XLXI_4/KernelBL[7].KB/_old_tmp_2<4> ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<0>_812 )
  );
  MUXCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<0>  (
    .CI(N0),
    .DI(\XLXI_4/KernelBL[7].KB/_old_tmp_2<4> ),
    .S(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<0>_812 ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<0>_797 )
  );
  XORCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_xor<0>  (
    .CI(N0),
    .LI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<0>_812 ),
    .O(\XLXI_4/KernelBL[7].KB/value_add0000<0> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<1>  (
    .I0(\XLXI_4/KernelBL[7].KB/test<1> ),
    .I1(\XLXI_4/KernelBL[7].KB/_old_tmp_2<5> ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<1>_819 )
  );
  MUXCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<1>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<0>_797 ),
    .DI(\XLXI_4/KernelBL[7].KB/_old_tmp_2<5> ),
    .S(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<1>_819 ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<1>_803 )
  );
  XORCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_xor<1>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<0>_797 ),
    .LI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<1>_819 ),
    .O(\XLXI_4/KernelBL[7].KB/value_add0000<1> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<2>  (
    .I0(\XLXI_4/KernelBL[7].KB/test<2> ),
    .I1(\XLXI_4/KernelBL[7].KB/_old_tmp_2<6> ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<2>_820 )
  );
  MUXCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<2>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<1>_803 ),
    .DI(\XLXI_4/KernelBL[7].KB/_old_tmp_2<6> ),
    .S(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<2>_820 ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<2>_804 )
  );
  XORCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_xor<2>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<1>_803 ),
    .LI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<2>_820 ),
    .O(\XLXI_4/KernelBL[7].KB/value_add0000<2> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<3>  (
    .I0(\XLXI_4/KernelBL[7].KB/test<3> ),
    .I1(\XLXI_4/KernelBL[7].KB/_old_tmp_2<7> ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<3>_821 )
  );
  MUXCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<3>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<2>_804 ),
    .DI(\XLXI_4/KernelBL[7].KB/_old_tmp_2<7> ),
    .S(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<3>_821 ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<3>_805 )
  );
  XORCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_xor<3>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<2>_804 ),
    .LI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<3>_821 ),
    .O(\XLXI_4/KernelBL[7].KB/value_add0000<3> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<4>  (
    .I0(\XLXI_4/KernelBL[7].KB/test<4> ),
    .I1(\XLXI_4/KernelBL[7].KB/_old_tmp_2<8> ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<4>_822 )
  );
  MUXCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<4>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<3>_805 ),
    .DI(\XLXI_4/KernelBL[7].KB/_old_tmp_2<8> ),
    .S(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<4>_822 ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<4>_806 )
  );
  XORCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_xor<4>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<3>_805 ),
    .LI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<4>_822 ),
    .O(\XLXI_4/KernelBL[7].KB/value_add0000<4> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<5>  (
    .I0(\XLXI_4/KernelBL[7].KB/test<5> ),
    .I1(\XLXI_4/KernelBL[7].KB/_old_tmp_2<9> ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<5>_823 )
  );
  MUXCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<5>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<4>_806 ),
    .DI(\XLXI_4/KernelBL[7].KB/_old_tmp_2<9> ),
    .S(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<5>_823 ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<5>_807 )
  );
  XORCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_xor<5>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<4>_806 ),
    .LI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<5>_823 ),
    .O(\XLXI_4/KernelBL[7].KB/value_add0000<5> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<6>  (
    .I0(\XLXI_4/KernelBL[7].KB/test<6> ),
    .I1(\XLXI_4/KernelBL[7].KB/_old_tmp_2<10> ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<6>_824 )
  );
  MUXCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<6>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<5>_807 ),
    .DI(\XLXI_4/KernelBL[7].KB/_old_tmp_2<10> ),
    .S(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<6>_824 ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<6>_808 )
  );
  XORCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_xor<6>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<5>_807 ),
    .LI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<6>_824 ),
    .O(\XLXI_4/KernelBL[7].KB/value_add0000<6> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<7>  (
    .I0(\XLXI_4/KernelBL[7].KB/test<7> ),
    .I1(\XLXI_4/KernelBL[7].KB/_old_tmp_2<11> ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<7>_825 )
  );
  MUXCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<7>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<6>_808 ),
    .DI(\XLXI_4/KernelBL[7].KB/_old_tmp_2<11> ),
    .S(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<7>_825 ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<7>_809 )
  );
  XORCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_xor<7>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<6>_808 ),
    .LI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<7>_825 ),
    .O(\XLXI_4/KernelBL[7].KB/value_add0000<7> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<8>  (
    .I0(\XLXI_4/KernelBL[7].KB/test<8> ),
    .I1(\XLXI_4/KernelBL[7].KB/_old_tmp_2<12> ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<8>_826 )
  );
  MUXCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<8>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<7>_809 ),
    .DI(\XLXI_4/KernelBL[7].KB/_old_tmp_2<12> ),
    .S(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<8>_826 ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<8>_810 )
  );
  XORCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_xor<8>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<7>_809 ),
    .LI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<8>_826 ),
    .O(\XLXI_4/KernelBL[7].KB/value_add0000<8> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<9>  (
    .I0(\XLXI_4/KernelBL[7].KB/test<9> ),
    .I1(\XLXI_4/KernelBL[7].KB/_old_tmp_2<13> ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<9>_827 )
  );
  MUXCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<9>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<8>_810 ),
    .DI(\XLXI_4/KernelBL[7].KB/_old_tmp_2<13> ),
    .S(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<9>_827 ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<9>_811 )
  );
  XORCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_xor<9>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<8>_810 ),
    .LI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<9>_827 ),
    .O(\XLXI_4/KernelBL[7].KB/value_add0000<9> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<10>  (
    .I0(\XLXI_4/KernelBL[7].KB/test<10> ),
    .I1(\XLXI_4/KernelBL[7].KB/_old_tmp_2<14> ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<10>_813 )
  );
  MUXCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<10>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<9>_811 ),
    .DI(\XLXI_4/KernelBL[7].KB/_old_tmp_2<14> ),
    .S(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<10>_813 ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<10>_798 )
  );
  XORCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_xor<10>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<9>_811 ),
    .LI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<10>_813 ),
    .O(\XLXI_4/KernelBL[7].KB/value_add0000<10> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<11>  (
    .I0(\XLXI_4/KernelBL[7].KB/test<11> ),
    .I1(\XLXI_4/KernelBL[7].KB/_old_tmp_2<15> ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<11>_814 )
  );
  MUXCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<11>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<10>_798 ),
    .DI(\XLXI_4/KernelBL[7].KB/_old_tmp_2<15> ),
    .S(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<11>_814 ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<11>_799 )
  );
  XORCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_xor<11>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<10>_798 ),
    .LI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<11>_814 ),
    .O(\XLXI_4/KernelBL[7].KB/value_add0000<11> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<12>  (
    .I0(\XLXI_4/KernelBL[7].KB/test<12> ),
    .I1(\XLXI_4/KernelBL[7].KB/_old_tmp_2<16> ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<12>_815 )
  );
  MUXCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<12>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<11>_799 ),
    .DI(\XLXI_4/KernelBL[7].KB/_old_tmp_2<16> ),
    .S(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<12>_815 ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<12>_800 )
  );
  XORCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_xor<12>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<11>_799 ),
    .LI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<12>_815 ),
    .O(\XLXI_4/KernelBL[7].KB/value_add0000<12> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<13>  (
    .I0(\XLXI_4/KernelBL[7].KB/test<13> ),
    .I1(\XLXI_4/KernelBL[7].KB/_old_tmp_2<17> ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<13>_816 )
  );
  MUXCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<13>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<12>_800 ),
    .DI(\XLXI_4/KernelBL[7].KB/_old_tmp_2<17> ),
    .S(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<13>_816 ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<13>_801 )
  );
  XORCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_xor<13>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<12>_800 ),
    .LI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<13>_816 ),
    .O(\XLXI_4/KernelBL[7].KB/value_add0000<13> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<14>  (
    .I0(\XLXI_4/KernelBL[7].KB/test<14> ),
    .I1(\XLXI_4/KernelBL[7].KB/_old_tmp_2<18> ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<14>_817 )
  );
  MUXCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<14>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<13>_801 ),
    .DI(\XLXI_4/KernelBL[7].KB/_old_tmp_2<18> ),
    .S(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<14>_817 ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<14>_802 )
  );
  XORCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_xor<14>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<13>_801 ),
    .LI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<14>_817 ),
    .O(\XLXI_4/KernelBL[7].KB/value_add0000<14> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<15>  (
    .I0(\XLXI_4/KernelBL[7].KB/test<15> ),
    .I1(\XLXI_4/KernelBL[7].KB/_old_tmp_2<19> ),
    .O(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<15>_818 )
  );
  XORCY   \XLXI_4/KernelBL[7].KB/Madd_value_add0000_xor<15>  (
    .CI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_cy<14>_802 ),
    .LI(\XLXI_4/KernelBL[7].KB/Madd_value_add0000_lut<15>_818 ),
    .O(\XLXI_4/KernelBL[7].KB/value_add0000<15> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/test_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value<0> ),
    .Q(\XLXI_4/KernelBL[8].KB/test<0> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/test_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value<1> ),
    .Q(\XLXI_4/KernelBL[8].KB/test<1> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/test_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value<2> ),
    .Q(\XLXI_4/KernelBL[8].KB/test<2> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/test_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value<3> ),
    .Q(\XLXI_4/KernelBL[8].KB/test<3> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/test_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value<4> ),
    .Q(\XLXI_4/KernelBL[8].KB/test<4> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/test_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value<5> ),
    .Q(\XLXI_4/KernelBL[8].KB/test<5> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/test_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value<6> ),
    .Q(\XLXI_4/KernelBL[8].KB/test<6> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/test_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value<7> ),
    .Q(\XLXI_4/KernelBL[8].KB/test<7> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/test_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value<8> ),
    .Q(\XLXI_4/KernelBL[8].KB/test<8> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/test_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value<9> ),
    .Q(\XLXI_4/KernelBL[8].KB/test<9> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/test_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value<10> ),
    .Q(\XLXI_4/KernelBL[8].KB/test<10> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/test_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value<11> ),
    .Q(\XLXI_4/KernelBL[8].KB/test<11> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/test_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value<12> ),
    .Q(\XLXI_4/KernelBL[8].KB/test<12> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/test_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value<13> ),
    .Q(\XLXI_4/KernelBL[8].KB/test<13> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/test_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value<14> ),
    .Q(\XLXI_4/KernelBL[8].KB/test<14> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/test_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[7].KB/value<15> ),
    .Q(\XLXI_4/KernelBL[8].KB/test<15> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/value_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value_add0000<0> ),
    .Q(\XLXI_4/KernelBL[8].KB/value<0> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/value_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value_add0000<1> ),
    .Q(\XLXI_4/KernelBL[8].KB/value<1> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/value_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value_add0000<2> ),
    .Q(\XLXI_4/KernelBL[8].KB/value<2> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/value_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value_add0000<3> ),
    .Q(\XLXI_4/KernelBL[8].KB/value<3> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/value_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value_add0000<4> ),
    .Q(\XLXI_4/KernelBL[8].KB/value<4> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/value_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value_add0000<5> ),
    .Q(\XLXI_4/KernelBL[8].KB/value<5> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/value_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value_add0000<6> ),
    .Q(\XLXI_4/KernelBL[8].KB/value<6> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/value_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value_add0000<7> ),
    .Q(\XLXI_4/KernelBL[8].KB/value<7> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/value_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value_add0000<8> ),
    .Q(\XLXI_4/KernelBL[8].KB/value<8> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/value_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value_add0000<9> ),
    .Q(\XLXI_4/KernelBL[8].KB/value<9> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/value_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value_add0000<10> ),
    .Q(\XLXI_4/KernelBL[8].KB/value<10> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/value_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value_add0000<11> ),
    .Q(\XLXI_4/KernelBL[8].KB/value<11> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/value_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value_add0000<12> ),
    .Q(\XLXI_4/KernelBL[8].KB/value<12> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/value_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value_add0000<13> ),
    .Q(\XLXI_4/KernelBL[8].KB/value<13> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/value_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value_add0000<14> ),
    .Q(\XLXI_4/KernelBL[8].KB/value<14> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[8].KB/value_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value_add0000<15> ),
    .Q(\XLXI_4/KernelBL[8].KB/value<15> )
  );
  MULT18X18SIO #(
    .B_INPUT ( "DIRECT" ),
    .AREG ( 0 ),
    .BREG ( 0 ),
    .PREG ( 0 ))
  \XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2  (
    .CEA(N0),
    .CEB(N0),
    .CEP(N0),
    .CLK(N0),
    .RSTA(N0),
    .RSTB(N0),
    .RSTP(N0),
    .A({N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N1}),
    .B({N0, N0, \XLXI_5/Numb [11], \XLXI_5/Numb [10], \XLXI_5/Numb [9], \XLXI_5/Numb [8], \XLXI_5/Numb [7], \XLXI_5/Numb [6], \XLXI_5/Numb [5], 
\XLXI_5/Numb [4], \XLXI_5/Numb [3], \XLXI_5/Numb [2], \XLXI_5/Numb [1], \XLXI_5/Numb [0], N0, N0, N0, N0}),
    .BCIN({\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED }),
    .P({\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED , 
\XLXI_4/KernelBL[8].KB/_old_tmp_2<19> , \XLXI_4/KernelBL[8].KB/_old_tmp_2<18> , \XLXI_4/KernelBL[8].KB/_old_tmp_2<17> , 
\XLXI_4/KernelBL[8].KB/_old_tmp_2<16> , \XLXI_4/KernelBL[8].KB/_old_tmp_2<15> , \XLXI_4/KernelBL[8].KB/_old_tmp_2<14> , 
\XLXI_4/KernelBL[8].KB/_old_tmp_2<13> , \XLXI_4/KernelBL[8].KB/_old_tmp_2<12> , \XLXI_4/KernelBL[8].KB/_old_tmp_2<11> , 
\XLXI_4/KernelBL[8].KB/_old_tmp_2<10> , \XLXI_4/KernelBL[8].KB/_old_tmp_2<9> , \XLXI_4/KernelBL[8].KB/_old_tmp_2<8> , 
\XLXI_4/KernelBL[8].KB/_old_tmp_2<7> , \XLXI_4/KernelBL[8].KB/_old_tmp_2<6> , \XLXI_4/KernelBL[8].KB/_old_tmp_2<5> , 
\XLXI_4/KernelBL[8].KB/_old_tmp_2<4> , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED }),
    .BCOUT({\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[8].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED })
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<0>  (
    .I0(\XLXI_4/KernelBL[8].KB/test<0> ),
    .I1(\XLXI_4/KernelBL[8].KB/_old_tmp_2<4> ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<0>_907 )
  );
  MUXCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<0>  (
    .CI(N0),
    .DI(\XLXI_4/KernelBL[8].KB/_old_tmp_2<4> ),
    .S(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<0>_907 ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<0>_892 )
  );
  XORCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_xor<0>  (
    .CI(N0),
    .LI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<0>_907 ),
    .O(\XLXI_4/KernelBL[8].KB/value_add0000<0> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<1>  (
    .I0(\XLXI_4/KernelBL[8].KB/test<1> ),
    .I1(\XLXI_4/KernelBL[8].KB/_old_tmp_2<5> ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<1>_914 )
  );
  MUXCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<1>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<0>_892 ),
    .DI(\XLXI_4/KernelBL[8].KB/_old_tmp_2<5> ),
    .S(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<1>_914 ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<1>_898 )
  );
  XORCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_xor<1>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<0>_892 ),
    .LI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<1>_914 ),
    .O(\XLXI_4/KernelBL[8].KB/value_add0000<1> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<2>  (
    .I0(\XLXI_4/KernelBL[8].KB/test<2> ),
    .I1(\XLXI_4/KernelBL[8].KB/_old_tmp_2<6> ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<2>_915 )
  );
  MUXCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<2>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<1>_898 ),
    .DI(\XLXI_4/KernelBL[8].KB/_old_tmp_2<6> ),
    .S(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<2>_915 ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<2>_899 )
  );
  XORCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_xor<2>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<1>_898 ),
    .LI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<2>_915 ),
    .O(\XLXI_4/KernelBL[8].KB/value_add0000<2> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<3>  (
    .I0(\XLXI_4/KernelBL[8].KB/test<3> ),
    .I1(\XLXI_4/KernelBL[8].KB/_old_tmp_2<7> ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<3>_916 )
  );
  MUXCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<3>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<2>_899 ),
    .DI(\XLXI_4/KernelBL[8].KB/_old_tmp_2<7> ),
    .S(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<3>_916 ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<3>_900 )
  );
  XORCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_xor<3>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<2>_899 ),
    .LI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<3>_916 ),
    .O(\XLXI_4/KernelBL[8].KB/value_add0000<3> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<4>  (
    .I0(\XLXI_4/KernelBL[8].KB/test<4> ),
    .I1(\XLXI_4/KernelBL[8].KB/_old_tmp_2<8> ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<4>_917 )
  );
  MUXCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<4>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<3>_900 ),
    .DI(\XLXI_4/KernelBL[8].KB/_old_tmp_2<8> ),
    .S(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<4>_917 ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<4>_901 )
  );
  XORCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_xor<4>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<3>_900 ),
    .LI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<4>_917 ),
    .O(\XLXI_4/KernelBL[8].KB/value_add0000<4> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<5>  (
    .I0(\XLXI_4/KernelBL[8].KB/test<5> ),
    .I1(\XLXI_4/KernelBL[8].KB/_old_tmp_2<9> ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<5>_918 )
  );
  MUXCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<5>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<4>_901 ),
    .DI(\XLXI_4/KernelBL[8].KB/_old_tmp_2<9> ),
    .S(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<5>_918 ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<5>_902 )
  );
  XORCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_xor<5>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<4>_901 ),
    .LI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<5>_918 ),
    .O(\XLXI_4/KernelBL[8].KB/value_add0000<5> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<6>  (
    .I0(\XLXI_4/KernelBL[8].KB/test<6> ),
    .I1(\XLXI_4/KernelBL[8].KB/_old_tmp_2<10> ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<6>_919 )
  );
  MUXCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<6>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<5>_902 ),
    .DI(\XLXI_4/KernelBL[8].KB/_old_tmp_2<10> ),
    .S(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<6>_919 ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<6>_903 )
  );
  XORCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_xor<6>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<5>_902 ),
    .LI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<6>_919 ),
    .O(\XLXI_4/KernelBL[8].KB/value_add0000<6> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<7>  (
    .I0(\XLXI_4/KernelBL[8].KB/test<7> ),
    .I1(\XLXI_4/KernelBL[8].KB/_old_tmp_2<11> ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<7>_920 )
  );
  MUXCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<7>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<6>_903 ),
    .DI(\XLXI_4/KernelBL[8].KB/_old_tmp_2<11> ),
    .S(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<7>_920 ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<7>_904 )
  );
  XORCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_xor<7>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<6>_903 ),
    .LI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<7>_920 ),
    .O(\XLXI_4/KernelBL[8].KB/value_add0000<7> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<8>  (
    .I0(\XLXI_4/KernelBL[8].KB/test<8> ),
    .I1(\XLXI_4/KernelBL[8].KB/_old_tmp_2<12> ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<8>_921 )
  );
  MUXCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<8>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<7>_904 ),
    .DI(\XLXI_4/KernelBL[8].KB/_old_tmp_2<12> ),
    .S(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<8>_921 ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<8>_905 )
  );
  XORCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_xor<8>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<7>_904 ),
    .LI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<8>_921 ),
    .O(\XLXI_4/KernelBL[8].KB/value_add0000<8> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<9>  (
    .I0(\XLXI_4/KernelBL[8].KB/test<9> ),
    .I1(\XLXI_4/KernelBL[8].KB/_old_tmp_2<13> ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<9>_922 )
  );
  MUXCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<9>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<8>_905 ),
    .DI(\XLXI_4/KernelBL[8].KB/_old_tmp_2<13> ),
    .S(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<9>_922 ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<9>_906 )
  );
  XORCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_xor<9>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<8>_905 ),
    .LI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<9>_922 ),
    .O(\XLXI_4/KernelBL[8].KB/value_add0000<9> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<10>  (
    .I0(\XLXI_4/KernelBL[8].KB/test<10> ),
    .I1(\XLXI_4/KernelBL[8].KB/_old_tmp_2<14> ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<10>_908 )
  );
  MUXCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<10>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<9>_906 ),
    .DI(\XLXI_4/KernelBL[8].KB/_old_tmp_2<14> ),
    .S(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<10>_908 ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<10>_893 )
  );
  XORCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_xor<10>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<9>_906 ),
    .LI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<10>_908 ),
    .O(\XLXI_4/KernelBL[8].KB/value_add0000<10> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<11>  (
    .I0(\XLXI_4/KernelBL[8].KB/test<11> ),
    .I1(\XLXI_4/KernelBL[8].KB/_old_tmp_2<15> ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<11>_909 )
  );
  MUXCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<11>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<10>_893 ),
    .DI(\XLXI_4/KernelBL[8].KB/_old_tmp_2<15> ),
    .S(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<11>_909 ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<11>_894 )
  );
  XORCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_xor<11>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<10>_893 ),
    .LI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<11>_909 ),
    .O(\XLXI_4/KernelBL[8].KB/value_add0000<11> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<12>  (
    .I0(\XLXI_4/KernelBL[8].KB/test<12> ),
    .I1(\XLXI_4/KernelBL[8].KB/_old_tmp_2<16> ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<12>_910 )
  );
  MUXCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<12>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<11>_894 ),
    .DI(\XLXI_4/KernelBL[8].KB/_old_tmp_2<16> ),
    .S(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<12>_910 ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<12>_895 )
  );
  XORCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_xor<12>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<11>_894 ),
    .LI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<12>_910 ),
    .O(\XLXI_4/KernelBL[8].KB/value_add0000<12> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<13>  (
    .I0(\XLXI_4/KernelBL[8].KB/test<13> ),
    .I1(\XLXI_4/KernelBL[8].KB/_old_tmp_2<17> ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<13>_911 )
  );
  MUXCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<13>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<12>_895 ),
    .DI(\XLXI_4/KernelBL[8].KB/_old_tmp_2<17> ),
    .S(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<13>_911 ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<13>_896 )
  );
  XORCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_xor<13>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<12>_895 ),
    .LI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<13>_911 ),
    .O(\XLXI_4/KernelBL[8].KB/value_add0000<13> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<14>  (
    .I0(\XLXI_4/KernelBL[8].KB/test<14> ),
    .I1(\XLXI_4/KernelBL[8].KB/_old_tmp_2<18> ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<14>_912 )
  );
  MUXCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<14>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<13>_896 ),
    .DI(\XLXI_4/KernelBL[8].KB/_old_tmp_2<18> ),
    .S(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<14>_912 ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<14>_897 )
  );
  XORCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_xor<14>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<13>_896 ),
    .LI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<14>_912 ),
    .O(\XLXI_4/KernelBL[8].KB/value_add0000<14> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<15>  (
    .I0(\XLXI_4/KernelBL[8].KB/test<15> ),
    .I1(\XLXI_4/KernelBL[8].KB/_old_tmp_2<19> ),
    .O(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<15>_913 )
  );
  XORCY   \XLXI_4/KernelBL[8].KB/Madd_value_add0000_xor<15>  (
    .CI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_cy<14>_897 ),
    .LI(\XLXI_4/KernelBL[8].KB/Madd_value_add0000_lut<15>_913 ),
    .O(\XLXI_4/KernelBL[8].KB/value_add0000<15> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/test_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value<0> ),
    .Q(\XLXI_4/KernelBL[9].KB/test<0> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/test_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value<1> ),
    .Q(\XLXI_4/KernelBL[9].KB/test<1> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/test_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value<2> ),
    .Q(\XLXI_4/KernelBL[9].KB/test<2> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/test_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value<3> ),
    .Q(\XLXI_4/KernelBL[9].KB/test<3> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/test_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value<4> ),
    .Q(\XLXI_4/KernelBL[9].KB/test<4> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/test_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value<5> ),
    .Q(\XLXI_4/KernelBL[9].KB/test<5> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/test_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value<6> ),
    .Q(\XLXI_4/KernelBL[9].KB/test<6> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/test_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value<7> ),
    .Q(\XLXI_4/KernelBL[9].KB/test<7> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/test_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value<8> ),
    .Q(\XLXI_4/KernelBL[9].KB/test<8> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/test_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value<9> ),
    .Q(\XLXI_4/KernelBL[9].KB/test<9> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/test_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value<10> ),
    .Q(\XLXI_4/KernelBL[9].KB/test<10> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/test_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value<11> ),
    .Q(\XLXI_4/KernelBL[9].KB/test<11> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/test_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value<12> ),
    .Q(\XLXI_4/KernelBL[9].KB/test<12> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/test_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value<13> ),
    .Q(\XLXI_4/KernelBL[9].KB/test<13> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/test_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value<14> ),
    .Q(\XLXI_4/KernelBL[9].KB/test<14> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/test_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[8].KB/value<15> ),
    .Q(\XLXI_4/KernelBL[9].KB/test<15> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/value_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value_add0000<0> ),
    .Q(\XLXI_4/KernelBL[9].KB/value<0> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/value_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value_add0000<1> ),
    .Q(\XLXI_4/KernelBL[9].KB/value<1> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/value_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value_add0000<2> ),
    .Q(\XLXI_4/KernelBL[9].KB/value<2> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/value_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value_add0000<3> ),
    .Q(\XLXI_4/KernelBL[9].KB/value<3> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/value_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value_add0000<4> ),
    .Q(\XLXI_4/KernelBL[9].KB/value<4> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/value_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value_add0000<5> ),
    .Q(\XLXI_4/KernelBL[9].KB/value<5> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/value_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value_add0000<6> ),
    .Q(\XLXI_4/KernelBL[9].KB/value<6> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/value_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value_add0000<7> ),
    .Q(\XLXI_4/KernelBL[9].KB/value<7> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/value_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value_add0000<8> ),
    .Q(\XLXI_4/KernelBL[9].KB/value<8> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/value_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value_add0000<9> ),
    .Q(\XLXI_4/KernelBL[9].KB/value<9> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/value_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value_add0000<10> ),
    .Q(\XLXI_4/KernelBL[9].KB/value<10> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/value_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value_add0000<11> ),
    .Q(\XLXI_4/KernelBL[9].KB/value<11> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/value_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value_add0000<12> ),
    .Q(\XLXI_4/KernelBL[9].KB/value<12> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/value_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value_add0000<13> ),
    .Q(\XLXI_4/KernelBL[9].KB/value<13> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/value_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value_add0000<14> ),
    .Q(\XLXI_4/KernelBL[9].KB/value<14> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[9].KB/value_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value_add0000<15> ),
    .Q(\XLXI_4/KernelBL[9].KB/value<15> )
  );
  MULT18X18SIO #(
    .B_INPUT ( "DIRECT" ),
    .AREG ( 0 ),
    .BREG ( 0 ),
    .PREG ( 0 ))
  \XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2  (
    .CEA(N0),
    .CEB(N0),
    .CEP(N0),
    .CLK(N0),
    .RSTA(N0),
    .RSTB(N0),
    .RSTP(N0),
    .A({N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N1}),
    .B({N0, N0, \XLXI_5/Numb [11], \XLXI_5/Numb [10], \XLXI_5/Numb [9], \XLXI_5/Numb [8], \XLXI_5/Numb [7], \XLXI_5/Numb [6], \XLXI_5/Numb [5], 
\XLXI_5/Numb [4], \XLXI_5/Numb [3], \XLXI_5/Numb [2], \XLXI_5/Numb [1], \XLXI_5/Numb [0], N0, N0, N0, N0}),
    .BCIN({\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED }),
    .P({\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED , 
\XLXI_4/KernelBL[9].KB/_old_tmp_2<19> , \XLXI_4/KernelBL[9].KB/_old_tmp_2<18> , \XLXI_4/KernelBL[9].KB/_old_tmp_2<17> , 
\XLXI_4/KernelBL[9].KB/_old_tmp_2<16> , \XLXI_4/KernelBL[9].KB/_old_tmp_2<15> , \XLXI_4/KernelBL[9].KB/_old_tmp_2<14> , 
\XLXI_4/KernelBL[9].KB/_old_tmp_2<13> , \XLXI_4/KernelBL[9].KB/_old_tmp_2<12> , \XLXI_4/KernelBL[9].KB/_old_tmp_2<11> , 
\XLXI_4/KernelBL[9].KB/_old_tmp_2<10> , \XLXI_4/KernelBL[9].KB/_old_tmp_2<9> , \XLXI_4/KernelBL[9].KB/_old_tmp_2<8> , 
\XLXI_4/KernelBL[9].KB/_old_tmp_2<7> , \XLXI_4/KernelBL[9].KB/_old_tmp_2<6> , \XLXI_4/KernelBL[9].KB/_old_tmp_2<5> , 
\XLXI_4/KernelBL[9].KB/_old_tmp_2<4> , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED }),
    .BCOUT({\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[9].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED })
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<0>  (
    .I0(\XLXI_4/KernelBL[9].KB/test<0> ),
    .I1(\XLXI_4/KernelBL[9].KB/_old_tmp_2<4> ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<0>_1002 )
  );
  MUXCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<0>  (
    .CI(N0),
    .DI(\XLXI_4/KernelBL[9].KB/_old_tmp_2<4> ),
    .S(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<0>_1002 ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<0>_987 )
  );
  XORCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_xor<0>  (
    .CI(N0),
    .LI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<0>_1002 ),
    .O(\XLXI_4/KernelBL[9].KB/value_add0000<0> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<1>  (
    .I0(\XLXI_4/KernelBL[9].KB/test<1> ),
    .I1(\XLXI_4/KernelBL[9].KB/_old_tmp_2<5> ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<1>_1009 )
  );
  MUXCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<1>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<0>_987 ),
    .DI(\XLXI_4/KernelBL[9].KB/_old_tmp_2<5> ),
    .S(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<1>_1009 ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<1>_993 )
  );
  XORCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_xor<1>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<0>_987 ),
    .LI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<1>_1009 ),
    .O(\XLXI_4/KernelBL[9].KB/value_add0000<1> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<2>  (
    .I0(\XLXI_4/KernelBL[9].KB/test<2> ),
    .I1(\XLXI_4/KernelBL[9].KB/_old_tmp_2<6> ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<2>_1010 )
  );
  MUXCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<2>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<1>_993 ),
    .DI(\XLXI_4/KernelBL[9].KB/_old_tmp_2<6> ),
    .S(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<2>_1010 ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<2>_994 )
  );
  XORCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_xor<2>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<1>_993 ),
    .LI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<2>_1010 ),
    .O(\XLXI_4/KernelBL[9].KB/value_add0000<2> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<3>  (
    .I0(\XLXI_4/KernelBL[9].KB/test<3> ),
    .I1(\XLXI_4/KernelBL[9].KB/_old_tmp_2<7> ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<3>_1011 )
  );
  MUXCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<3>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<2>_994 ),
    .DI(\XLXI_4/KernelBL[9].KB/_old_tmp_2<7> ),
    .S(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<3>_1011 ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<3>_995 )
  );
  XORCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_xor<3>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<2>_994 ),
    .LI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<3>_1011 ),
    .O(\XLXI_4/KernelBL[9].KB/value_add0000<3> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<4>  (
    .I0(\XLXI_4/KernelBL[9].KB/test<4> ),
    .I1(\XLXI_4/KernelBL[9].KB/_old_tmp_2<8> ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<4>_1012 )
  );
  MUXCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<4>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<3>_995 ),
    .DI(\XLXI_4/KernelBL[9].KB/_old_tmp_2<8> ),
    .S(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<4>_1012 ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<4>_996 )
  );
  XORCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_xor<4>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<3>_995 ),
    .LI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<4>_1012 ),
    .O(\XLXI_4/KernelBL[9].KB/value_add0000<4> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<5>  (
    .I0(\XLXI_4/KernelBL[9].KB/test<5> ),
    .I1(\XLXI_4/KernelBL[9].KB/_old_tmp_2<9> ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<5>_1013 )
  );
  MUXCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<5>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<4>_996 ),
    .DI(\XLXI_4/KernelBL[9].KB/_old_tmp_2<9> ),
    .S(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<5>_1013 ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<5>_997 )
  );
  XORCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_xor<5>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<4>_996 ),
    .LI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<5>_1013 ),
    .O(\XLXI_4/KernelBL[9].KB/value_add0000<5> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<6>  (
    .I0(\XLXI_4/KernelBL[9].KB/test<6> ),
    .I1(\XLXI_4/KernelBL[9].KB/_old_tmp_2<10> ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<6>_1014 )
  );
  MUXCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<6>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<5>_997 ),
    .DI(\XLXI_4/KernelBL[9].KB/_old_tmp_2<10> ),
    .S(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<6>_1014 ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<6>_998 )
  );
  XORCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_xor<6>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<5>_997 ),
    .LI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<6>_1014 ),
    .O(\XLXI_4/KernelBL[9].KB/value_add0000<6> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<7>  (
    .I0(\XLXI_4/KernelBL[9].KB/test<7> ),
    .I1(\XLXI_4/KernelBL[9].KB/_old_tmp_2<11> ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<7>_1015 )
  );
  MUXCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<7>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<6>_998 ),
    .DI(\XLXI_4/KernelBL[9].KB/_old_tmp_2<11> ),
    .S(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<7>_1015 ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<7>_999 )
  );
  XORCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_xor<7>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<6>_998 ),
    .LI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<7>_1015 ),
    .O(\XLXI_4/KernelBL[9].KB/value_add0000<7> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<8>  (
    .I0(\XLXI_4/KernelBL[9].KB/test<8> ),
    .I1(\XLXI_4/KernelBL[9].KB/_old_tmp_2<12> ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<8>_1016 )
  );
  MUXCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<8>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<7>_999 ),
    .DI(\XLXI_4/KernelBL[9].KB/_old_tmp_2<12> ),
    .S(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<8>_1016 ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<8>_1000 )
  );
  XORCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_xor<8>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<7>_999 ),
    .LI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<8>_1016 ),
    .O(\XLXI_4/KernelBL[9].KB/value_add0000<8> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<9>  (
    .I0(\XLXI_4/KernelBL[9].KB/test<9> ),
    .I1(\XLXI_4/KernelBL[9].KB/_old_tmp_2<13> ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<9>_1017 )
  );
  MUXCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<9>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<8>_1000 ),
    .DI(\XLXI_4/KernelBL[9].KB/_old_tmp_2<13> ),
    .S(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<9>_1017 ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<9>_1001 )
  );
  XORCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_xor<9>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<8>_1000 ),
    .LI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<9>_1017 ),
    .O(\XLXI_4/KernelBL[9].KB/value_add0000<9> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<10>  (
    .I0(\XLXI_4/KernelBL[9].KB/test<10> ),
    .I1(\XLXI_4/KernelBL[9].KB/_old_tmp_2<14> ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<10>_1003 )
  );
  MUXCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<10>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<9>_1001 ),
    .DI(\XLXI_4/KernelBL[9].KB/_old_tmp_2<14> ),
    .S(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<10>_1003 ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<10>_988 )
  );
  XORCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_xor<10>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<9>_1001 ),
    .LI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<10>_1003 ),
    .O(\XLXI_4/KernelBL[9].KB/value_add0000<10> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<11>  (
    .I0(\XLXI_4/KernelBL[9].KB/test<11> ),
    .I1(\XLXI_4/KernelBL[9].KB/_old_tmp_2<15> ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<11>_1004 )
  );
  MUXCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<11>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<10>_988 ),
    .DI(\XLXI_4/KernelBL[9].KB/_old_tmp_2<15> ),
    .S(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<11>_1004 ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<11>_989 )
  );
  XORCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_xor<11>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<10>_988 ),
    .LI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<11>_1004 ),
    .O(\XLXI_4/KernelBL[9].KB/value_add0000<11> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<12>  (
    .I0(\XLXI_4/KernelBL[9].KB/test<12> ),
    .I1(\XLXI_4/KernelBL[9].KB/_old_tmp_2<16> ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<12>_1005 )
  );
  MUXCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<12>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<11>_989 ),
    .DI(\XLXI_4/KernelBL[9].KB/_old_tmp_2<16> ),
    .S(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<12>_1005 ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<12>_990 )
  );
  XORCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_xor<12>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<11>_989 ),
    .LI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<12>_1005 ),
    .O(\XLXI_4/KernelBL[9].KB/value_add0000<12> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<13>  (
    .I0(\XLXI_4/KernelBL[9].KB/test<13> ),
    .I1(\XLXI_4/KernelBL[9].KB/_old_tmp_2<17> ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<13>_1006 )
  );
  MUXCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<13>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<12>_990 ),
    .DI(\XLXI_4/KernelBL[9].KB/_old_tmp_2<17> ),
    .S(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<13>_1006 ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<13>_991 )
  );
  XORCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_xor<13>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<12>_990 ),
    .LI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<13>_1006 ),
    .O(\XLXI_4/KernelBL[9].KB/value_add0000<13> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<14>  (
    .I0(\XLXI_4/KernelBL[9].KB/test<14> ),
    .I1(\XLXI_4/KernelBL[9].KB/_old_tmp_2<18> ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<14>_1007 )
  );
  MUXCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<14>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<13>_991 ),
    .DI(\XLXI_4/KernelBL[9].KB/_old_tmp_2<18> ),
    .S(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<14>_1007 ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<14>_992 )
  );
  XORCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_xor<14>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<13>_991 ),
    .LI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<14>_1007 ),
    .O(\XLXI_4/KernelBL[9].KB/value_add0000<14> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<15>  (
    .I0(\XLXI_4/KernelBL[9].KB/test<15> ),
    .I1(\XLXI_4/KernelBL[9].KB/_old_tmp_2<19> ),
    .O(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<15>_1008 )
  );
  XORCY   \XLXI_4/KernelBL[9].KB/Madd_value_add0000_xor<15>  (
    .CI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_cy<14>_992 ),
    .LI(\XLXI_4/KernelBL[9].KB/Madd_value_add0000_lut<15>_1008 ),
    .O(\XLXI_4/KernelBL[9].KB/value_add0000<15> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/test_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value<0> ),
    .Q(\XLXI_4/KernelBL[10].KB/test<0> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/test_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value<1> ),
    .Q(\XLXI_4/KernelBL[10].KB/test<1> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/test_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value<2> ),
    .Q(\XLXI_4/KernelBL[10].KB/test<2> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/test_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value<3> ),
    .Q(\XLXI_4/KernelBL[10].KB/test<3> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/test_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value<4> ),
    .Q(\XLXI_4/KernelBL[10].KB/test<4> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/test_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value<5> ),
    .Q(\XLXI_4/KernelBL[10].KB/test<5> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/test_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value<6> ),
    .Q(\XLXI_4/KernelBL[10].KB/test<6> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/test_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value<7> ),
    .Q(\XLXI_4/KernelBL[10].KB/test<7> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/test_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value<8> ),
    .Q(\XLXI_4/KernelBL[10].KB/test<8> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/test_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value<9> ),
    .Q(\XLXI_4/KernelBL[10].KB/test<9> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/test_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value<10> ),
    .Q(\XLXI_4/KernelBL[10].KB/test<10> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/test_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value<11> ),
    .Q(\XLXI_4/KernelBL[10].KB/test<11> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/test_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value<12> ),
    .Q(\XLXI_4/KernelBL[10].KB/test<12> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/test_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value<13> ),
    .Q(\XLXI_4/KernelBL[10].KB/test<13> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/test_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value<14> ),
    .Q(\XLXI_4/KernelBL[10].KB/test<14> )
  );
  FD_1 #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/test_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[9].KB/value<15> ),
    .Q(\XLXI_4/KernelBL[10].KB/test<15> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/value_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value_add0000<4> ),
    .Q(\XLXI_4/KernelBL[10].KB/value<4> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/value_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value_add0000<5> ),
    .Q(\XLXI_4/KernelBL[10].KB/value<5> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/value_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value_add0000<6> ),
    .Q(\XLXI_4/KernelBL[10].KB/value<6> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/value_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value_add0000<7> ),
    .Q(\XLXI_4/KernelBL[10].KB/value<7> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/value_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value_add0000<8> ),
    .Q(\XLXI_4/KernelBL[10].KB/value<8> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/value_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value_add0000<9> ),
    .Q(\XLXI_4/KernelBL[10].KB/value<9> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/value_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value_add0000<10> ),
    .Q(\XLXI_4/KernelBL[10].KB/value<10> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/value_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value_add0000<11> ),
    .Q(\XLXI_4/KernelBL[10].KB/value<11> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/value_12  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value_add0000<12> ),
    .Q(\XLXI_4/KernelBL[10].KB/value<12> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/value_13  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value_add0000<13> ),
    .Q(\XLXI_4/KernelBL[10].KB/value<13> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/value_14  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value_add0000<14> ),
    .Q(\XLXI_4/KernelBL[10].KB/value<14> )
  );
  FD #(
    .INIT ( 1'b0 ))
  \XLXI_4/KernelBL[10].KB/value_15  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value_add0000<15> ),
    .Q(\XLXI_4/KernelBL[10].KB/value<15> )
  );
  MULT18X18SIO #(
    .B_INPUT ( "DIRECT" ),
    .AREG ( 0 ),
    .BREG ( 0 ),
    .PREG ( 0 ))
  \XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2  (
    .CEA(N0),
    .CEB(N0),
    .CEP(N0),
    .CLK(N0),
    .RSTA(N0),
    .RSTB(N0),
    .RSTP(N0),
    .A({N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N0, N1}),
    .B({N0, N0, \XLXI_5/Numb [11], \XLXI_5/Numb [10], \XLXI_5/Numb [9], \XLXI_5/Numb [8], \XLXI_5/Numb [7], \XLXI_5/Numb [6], \XLXI_5/Numb [5], 
\XLXI_5/Numb [4], \XLXI_5/Numb [3], \XLXI_5/Numb [2], \XLXI_5/Numb [1], \XLXI_5/Numb [0], N0, N0, N0, N0}),
    .BCIN({\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCIN<0>_UNCONNECTED }),
    .P({\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<35>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<34>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<33>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<32>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<31>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<30>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<29>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<28>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<27>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<26>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<25>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<24>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<23>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<22>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<21>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<20>_UNCONNECTED , 
\XLXI_4/KernelBL[10].KB/_old_tmp_2<19> , \XLXI_4/KernelBL[10].KB/_old_tmp_2<18> , \XLXI_4/KernelBL[10].KB/_old_tmp_2<17> , 
\XLXI_4/KernelBL[10].KB/_old_tmp_2<16> , \XLXI_4/KernelBL[10].KB/_old_tmp_2<15> , \XLXI_4/KernelBL[10].KB/_old_tmp_2<14> , 
\XLXI_4/KernelBL[10].KB/_old_tmp_2<13> , \XLXI_4/KernelBL[10].KB/_old_tmp_2<12> , \XLXI_4/KernelBL[10].KB/_old_tmp_2<11> , 
\XLXI_4/KernelBL[10].KB/_old_tmp_2<10> , \XLXI_4/KernelBL[10].KB/_old_tmp_2<9> , \XLXI_4/KernelBL[10].KB/_old_tmp_2<8> , 
\XLXI_4/KernelBL[10].KB/_old_tmp_2<7> , \XLXI_4/KernelBL[10].KB/_old_tmp_2<6> , \XLXI_4/KernelBL[10].KB/_old_tmp_2<5> , 
\XLXI_4/KernelBL[10].KB/_old_tmp_2<4> , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<3>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<2>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<1>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_P<0>_UNCONNECTED }),
    .BCOUT({\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<17>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<16>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<15>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<14>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<13>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<12>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<11>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<10>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<9>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<8>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<7>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<6>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<5>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<4>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<3>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<2>_UNCONNECTED , 
\NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<1>_UNCONNECTED , \NLW_XLXI_4/KernelBL[10].KB/Mmult__old_tmp_2_BCOUT<0>_UNCONNECTED })
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<0>  (
    .I0(\XLXI_4/KernelBL[10].KB/test<0> ),
    .I1(\XLXI_4/KernelBL[10].KB/_old_tmp_2<4> ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<0>_155 )
  );
  MUXCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<0>  (
    .CI(N0),
    .DI(\XLXI_4/KernelBL[10].KB/_old_tmp_2<4> ),
    .S(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<0>_155 ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<0>_140 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<1>  (
    .I0(\XLXI_4/KernelBL[10].KB/test<1> ),
    .I1(\XLXI_4/KernelBL[10].KB/_old_tmp_2<5> ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<1>_162 )
  );
  MUXCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<1>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<0>_140 ),
    .DI(\XLXI_4/KernelBL[10].KB/_old_tmp_2<5> ),
    .S(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<1>_162 ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<1>_146 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<2>  (
    .I0(\XLXI_4/KernelBL[10].KB/test<2> ),
    .I1(\XLXI_4/KernelBL[10].KB/_old_tmp_2<6> ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<2>_163 )
  );
  MUXCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<2>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<1>_146 ),
    .DI(\XLXI_4/KernelBL[10].KB/_old_tmp_2<6> ),
    .S(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<2>_163 ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<2>_147 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<3>  (
    .I0(\XLXI_4/KernelBL[10].KB/test<3> ),
    .I1(\XLXI_4/KernelBL[10].KB/_old_tmp_2<7> ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<3>_164 )
  );
  MUXCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<3>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<2>_147 ),
    .DI(\XLXI_4/KernelBL[10].KB/_old_tmp_2<7> ),
    .S(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<3>_164 ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<3>_148 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<4>  (
    .I0(\XLXI_4/KernelBL[10].KB/test<4> ),
    .I1(\XLXI_4/KernelBL[10].KB/_old_tmp_2<8> ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<4>_165 )
  );
  MUXCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<4>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<3>_148 ),
    .DI(\XLXI_4/KernelBL[10].KB/_old_tmp_2<8> ),
    .S(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<4>_165 ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<4>_149 )
  );
  XORCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_xor<4>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<3>_148 ),
    .LI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<4>_165 ),
    .O(\XLXI_4/KernelBL[10].KB/value_add0000<4> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<5>  (
    .I0(\XLXI_4/KernelBL[10].KB/test<5> ),
    .I1(\XLXI_4/KernelBL[10].KB/_old_tmp_2<9> ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<5>_166 )
  );
  MUXCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<5>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<4>_149 ),
    .DI(\XLXI_4/KernelBL[10].KB/_old_tmp_2<9> ),
    .S(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<5>_166 ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<5>_150 )
  );
  XORCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_xor<5>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<4>_149 ),
    .LI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<5>_166 ),
    .O(\XLXI_4/KernelBL[10].KB/value_add0000<5> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<6>  (
    .I0(\XLXI_4/KernelBL[10].KB/test<6> ),
    .I1(\XLXI_4/KernelBL[10].KB/_old_tmp_2<10> ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<6>_167 )
  );
  MUXCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<6>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<5>_150 ),
    .DI(\XLXI_4/KernelBL[10].KB/_old_tmp_2<10> ),
    .S(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<6>_167 ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<6>_151 )
  );
  XORCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_xor<6>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<5>_150 ),
    .LI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<6>_167 ),
    .O(\XLXI_4/KernelBL[10].KB/value_add0000<6> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<7>  (
    .I0(\XLXI_4/KernelBL[10].KB/test<7> ),
    .I1(\XLXI_4/KernelBL[10].KB/_old_tmp_2<11> ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<7>_168 )
  );
  MUXCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<7>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<6>_151 ),
    .DI(\XLXI_4/KernelBL[10].KB/_old_tmp_2<11> ),
    .S(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<7>_168 ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<7>_152 )
  );
  XORCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_xor<7>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<6>_151 ),
    .LI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<7>_168 ),
    .O(\XLXI_4/KernelBL[10].KB/value_add0000<7> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<8>  (
    .I0(\XLXI_4/KernelBL[10].KB/test<8> ),
    .I1(\XLXI_4/KernelBL[10].KB/_old_tmp_2<12> ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<8>_169 )
  );
  MUXCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<8>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<7>_152 ),
    .DI(\XLXI_4/KernelBL[10].KB/_old_tmp_2<12> ),
    .S(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<8>_169 ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<8>_153 )
  );
  XORCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_xor<8>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<7>_152 ),
    .LI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<8>_169 ),
    .O(\XLXI_4/KernelBL[10].KB/value_add0000<8> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<9>  (
    .I0(\XLXI_4/KernelBL[10].KB/test<9> ),
    .I1(\XLXI_4/KernelBL[10].KB/_old_tmp_2<13> ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<9>_170 )
  );
  MUXCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<9>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<8>_153 ),
    .DI(\XLXI_4/KernelBL[10].KB/_old_tmp_2<13> ),
    .S(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<9>_170 ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<9>_154 )
  );
  XORCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_xor<9>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<8>_153 ),
    .LI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<9>_170 ),
    .O(\XLXI_4/KernelBL[10].KB/value_add0000<9> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<10>  (
    .I0(\XLXI_4/KernelBL[10].KB/test<10> ),
    .I1(\XLXI_4/KernelBL[10].KB/_old_tmp_2<14> ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<10>_156 )
  );
  MUXCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<10>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<9>_154 ),
    .DI(\XLXI_4/KernelBL[10].KB/_old_tmp_2<14> ),
    .S(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<10>_156 ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<10>_141 )
  );
  XORCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_xor<10>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<9>_154 ),
    .LI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<10>_156 ),
    .O(\XLXI_4/KernelBL[10].KB/value_add0000<10> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<11>  (
    .I0(\XLXI_4/KernelBL[10].KB/test<11> ),
    .I1(\XLXI_4/KernelBL[10].KB/_old_tmp_2<15> ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<11>_157 )
  );
  MUXCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<11>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<10>_141 ),
    .DI(\XLXI_4/KernelBL[10].KB/_old_tmp_2<15> ),
    .S(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<11>_157 ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<11>_142 )
  );
  XORCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_xor<11>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<10>_141 ),
    .LI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<11>_157 ),
    .O(\XLXI_4/KernelBL[10].KB/value_add0000<11> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<12>  (
    .I0(\XLXI_4/KernelBL[10].KB/test<12> ),
    .I1(\XLXI_4/KernelBL[10].KB/_old_tmp_2<16> ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<12>_158 )
  );
  MUXCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<12>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<11>_142 ),
    .DI(\XLXI_4/KernelBL[10].KB/_old_tmp_2<16> ),
    .S(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<12>_158 ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<12>_143 )
  );
  XORCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_xor<12>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<11>_142 ),
    .LI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<12>_158 ),
    .O(\XLXI_4/KernelBL[10].KB/value_add0000<12> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<13>  (
    .I0(\XLXI_4/KernelBL[10].KB/test<13> ),
    .I1(\XLXI_4/KernelBL[10].KB/_old_tmp_2<17> ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<13>_159 )
  );
  MUXCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<13>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<12>_143 ),
    .DI(\XLXI_4/KernelBL[10].KB/_old_tmp_2<17> ),
    .S(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<13>_159 ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<13>_144 )
  );
  XORCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_xor<13>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<12>_143 ),
    .LI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<13>_159 ),
    .O(\XLXI_4/KernelBL[10].KB/value_add0000<13> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<14>  (
    .I0(\XLXI_4/KernelBL[10].KB/test<14> ),
    .I1(\XLXI_4/KernelBL[10].KB/_old_tmp_2<18> ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<14>_160 )
  );
  MUXCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<14>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<13>_144 ),
    .DI(\XLXI_4/KernelBL[10].KB/_old_tmp_2<18> ),
    .S(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<14>_160 ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<14>_145 )
  );
  XORCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_xor<14>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<13>_144 ),
    .LI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<14>_160 ),
    .O(\XLXI_4/KernelBL[10].KB/value_add0000<14> )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<15>  (
    .I0(\XLXI_4/KernelBL[10].KB/test<15> ),
    .I1(\XLXI_4/KernelBL[10].KB/_old_tmp_2<19> ),
    .O(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<15>_161 )
  );
  XORCY   \XLXI_4/KernelBL[10].KB/Madd_value_add0000_xor<15>  (
    .CI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_cy<14>_145 ),
    .LI(\XLXI_4/KernelBL[10].KB/Madd_value_add0000_lut<15>_161 ),
    .O(\XLXI_4/KernelBL[10].KB/value_add0000<15> )
  );
  FD_1   \XLXI_4/dataOut_11  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value<15> ),
    .Q(\XLXI_4/dataOut [11])
  );
  FD_1   \XLXI_4/dataOut_10  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value<14> ),
    .Q(\XLXI_4/dataOut [10])
  );
  FD_1   \XLXI_4/dataOut_9  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value<13> ),
    .Q(\XLXI_4/dataOut [9])
  );
  FD_1   \XLXI_4/dataOut_8  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value<12> ),
    .Q(\XLXI_4/dataOut [8])
  );
  FD_1   \XLXI_4/dataOut_7  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value<11> ),
    .Q(\XLXI_4/dataOut [7])
  );
  FD_1   \XLXI_4/dataOut_6  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value<10> ),
    .Q(\XLXI_4/dataOut [6])
  );
  FD_1   \XLXI_4/dataOut_5  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value<9> ),
    .Q(\XLXI_4/dataOut [5])
  );
  FD_1   \XLXI_4/dataOut_4  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value<8> ),
    .Q(\XLXI_4/dataOut [4])
  );
  FD_1   \XLXI_4/dataOut_3  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value<7> ),
    .Q(\XLXI_4/dataOut [3])
  );
  FD_1   \XLXI_4/dataOut_2  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value<6> ),
    .Q(\XLXI_4/dataOut [2])
  );
  FD_1   \XLXI_4/dataOut_1  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value<5> ),
    .Q(\XLXI_4/dataOut [1])
  );
  FD_1   \XLXI_4/dataOut_0  (
    .C(CLK_BUFGP_1),
    .D(\XLXI_4/KernelBL[10].KB/value<4> ),
    .Q(\XLXI_4/dataOut [0])
  );
  OBUF   L0_OBUF (
    .I(N1),
    .O(L0)
  );
  OBUF   L1_OBUF (
    .I(\XLXI_3/LED1_21 ),
    .O(L1)
  );
  OBUF   L2_OBUF (
    .I(\XLXI_3/LED2_22 ),
    .O(L2)
  );
  OBUF   L3_OBUF (
    .I(\XLXI_3/LED3_23 ),
    .O(L3)
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_5/Mcompar_Numb_cmp_gt0000_cy<1>_rt  (
    .I0(\XLXI_5/Numb [4]),
    .O(\XLXI_5/Mcompar_Numb_cmp_gt0000_cy<1>_rt_1096 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_5/Mcompar_Numb_cmp_gt0000_cy<4>_rt  (
    .I0(\XLXI_5/Numb [10]),
    .O(\XLXI_5/Mcompar_Numb_cmp_gt0000_cy<4>_rt_1100 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_5/Mcount_Numb_cy<1>_rt  (
    .I0(\XLXI_5/Numb [1]),
    .O(\XLXI_5/Mcount_Numb_cy<1>_rt_1110 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_5/Mcount_Numb_cy<2>_rt  (
    .I0(\XLXI_5/Numb [2]),
    .O(\XLXI_5/Mcount_Numb_cy<2>_rt_1112 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_5/Mcount_Numb_cy<3>_rt  (
    .I0(\XLXI_5/Numb [3]),
    .O(\XLXI_5/Mcount_Numb_cy<3>_rt_1114 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_5/Mcount_Numb_cy<4>_rt  (
    .I0(\XLXI_5/Numb [4]),
    .O(\XLXI_5/Mcount_Numb_cy<4>_rt_1116 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_5/Mcount_Numb_cy<5>_rt  (
    .I0(\XLXI_5/Numb [5]),
    .O(\XLXI_5/Mcount_Numb_cy<5>_rt_1118 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_5/Mcount_Numb_cy<6>_rt  (
    .I0(\XLXI_5/Numb [6]),
    .O(\XLXI_5/Mcount_Numb_cy<6>_rt_1120 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_5/Mcount_Numb_cy<7>_rt  (
    .I0(\XLXI_5/Numb [7]),
    .O(\XLXI_5/Mcount_Numb_cy<7>_rt_1122 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_5/Mcount_Numb_cy<8>_rt  (
    .I0(\XLXI_5/Numb [8]),
    .O(\XLXI_5/Mcount_Numb_cy<8>_rt_1124 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_5/Mcount_Numb_cy<9>_rt  (
    .I0(\XLXI_5/Numb [9]),
    .O(\XLXI_5/Mcount_Numb_cy<9>_rt_1126 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_5/Mcount_Numb_cy<10>_rt  (
    .I0(\XLXI_5/Numb [10]),
    .O(\XLXI_5/Mcount_Numb_cy<10>_rt_1108 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<6>_rt  (
    .I0(\XLXI_4/dataOut [11]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<6>_rt_47 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<4>_1_rt  (
    .I0(\XLXI_4/dataOut [8]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<4>_1_rt_41 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<1>_0_rt  (
    .I0(\XLXI_4/dataOut [2]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<1>_0_rt_30 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<5>_rt  (
    .I0(\XLXI_4/dataOut [11]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<5>_rt_45 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_3/Mcompar_LED3_cmp_gt0000_cy<2>_rt  (
    .I0(\XLXI_4/dataOut [5]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_cy<2>_rt_34 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<0>_rt  (
    .I0(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<0> ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<0>_rt_63 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<1>_rt  (
    .I0(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<1> ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<1>_rt_75 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<2>_rt  (
    .I0(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<2> ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<2>_rt_77 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<3>_rt  (
    .I0(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<3> ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<3>_rt_79 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<4>_rt  (
    .I0(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<4> ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<4>_rt_81 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<5>_rt  (
    .I0(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<5> ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<5>_rt_83 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<6>_rt  (
    .I0(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<6> ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<6>_rt_85 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<7>_rt  (
    .I0(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<7> ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<7>_rt_87 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<8>_rt  (
    .I0(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<8> ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<8>_rt_89 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<9>_rt  (
    .I0(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<9> ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<9>_rt_91 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<10>_rt  (
    .I0(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<10> ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<10>_rt_65 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<11>_rt  (
    .I0(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<11> ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<11>_rt_67 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<12>_rt  (
    .I0(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<12> ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<12>_rt_69 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<13>_rt  (
    .I0(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<13> ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<13>_rt_71 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<14>_rt  (
    .I0(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_lut<14> ),
    .O(\XLXI_4/KernelBL[0].KB/Madd_value_add0000_cy<14>_rt_73 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \XLXI_5/Mcount_Numb_xor<11>_rt  (
    .I0(\XLXI_5/Numb [11]),
    .O(\XLXI_5/Mcount_Numb_xor<11>_rt_1128 )
  );
  BUFGP   CLK_BUFGP (
    .I(CLK),
    .O(CLK_BUFGP_1)
  );
  INV   \XLXI_5/Mcompar_Numb_cmp_gt0000_lut<2>_INV_0  (
    .I(\XLXI_5/Numb [5]),
    .O(\XLXI_5/Mcompar_Numb_cmp_gt0000_lut[2] )
  );
  INV   \XLXI_5/Mcompar_Numb_cmp_gt0000_lut<5>_INV_0  (
    .I(\XLXI_5/Numb [11]),
    .O(\XLXI_5/Mcompar_Numb_cmp_gt0000_lut[5] )
  );
  INV   \XLXI_5/Mcompar_Numb_cmp_gt0000_cy<5>_inv_INV_0  (
    .I(\XLXI_5/Mcompar_Numb_cmp_gt0000_cy [5]),
    .O(\XLXI_5/Numb_cmp_gt0000 )
  );
  INV   \XLXI_5/Mcount_Numb_lut<0>_INV_0  (
    .I(\XLXI_5/Numb [0]),
    .O(\XLXI_5/Mcount_Numb_lut [0])
  );
  INV   \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<3>2_INV_0  (
    .I(\XLXI_4/dataOut [7]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut<3>2 )
  );
  INV   \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<1>1_INV_0  (
    .I(\XLXI_4/dataOut [4]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut<1>1 )
  );
  INV   \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<5>_INV_0  (
    .I(\XLXI_4/dataOut [11]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut [5])
  );
  INV   \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<3>_INV_0  (
    .I(\XLXI_4/dataOut [6]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut [3])
  );
  INV   \XLXI_3/Mcompar_LED3_cmp_gt0000_lut<1>_INV_0  (
    .I(\XLXI_4/dataOut [4]),
    .O(\XLXI_3/Mcompar_LED3_cmp_gt0000_lut [1])
  );
  INV   CLK_inv1_INV_0 (
    .I(CLK_BUFGP_1),
    .O(CLK_inv)
  );
endmodule


`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

