`timescale 1ns / 1ps

module kernBL;

	// Inputs
	reg [7:0] data;
	reg [11:0] ldata;
	reg [11:0] filter;
	reg add_clk;

	reg set;

	// Outputs
	wire [11:0] value;


	// Instantiate the Unit Under Test (UUT)
	kernelBlock uut (
		.data(data), 
		.ldata(ldata), 
		.filter(filter),
		.clk(add_clk),
		.setFilter(set),
		.value(value)
	);

	initial begin
		// Initialize Inputs
		data = 0;
		add_clk = 0;
		set = 0;
      ldata = 'b10000;
		filter = 'b1000;

		// Wait 100 ns for global reset to finish
		#100;
		  set = 1;
		#100
		  set = 0;
		  data = 'b10000011;
		  add_clk = 1;
		#100
		  add_clk = 0;
		#100
		  data = 'b111;
		  add_clk = 1;
  		#100
		  add_clk = 0;


	end
      
endmodule

