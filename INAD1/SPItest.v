`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:10:12 01/15/2020
// Design Name:   SPIAD
// Module Name:   /home/raimonds/Downloads/INAD1/SPItest.v
// Project Name:  INAD1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: SPIAD
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module SPItest;

	// Inputs
	wire CLK2;
	reg MISO_L;
	reg MISO_R;
	
	reg clkIn;

	// Outputs
	wire [11:0] dataOut_R;
	wire [11:0] dataOut_L;
	wire SCLK;
	wire CS;
	
	wire L0;
	wire L1;
	wire L2;
	wire L3;

	reg reset;
	// Instantiate the Unit Under Test (UUT)
	SPIAD uut (
		

		.clkIN(clkIn), 
		.MISO_L(MISO_L), 
		.MISO_R(MISO_R), 
		.dataOut_R(dataOut_R), 
		.dataOut_L(dataOut_L), 

		
		.SCLK(SCLK), //clock for the chip

		.CS(CS),
		
		.CLK(CLK2)
				
		
	);
	
	LedCheck lch(
		.clk(CLK2),
		.L(dataOut_L),
		.R(dataOut_R),
		.LED0(L0),
		.LED1(L1),
		.LED2(L2),
		.LED3(L3)
	);


	reg [15:0] L;
	reg [15:0] R;
	reg [15:0] Li2;
	reg [15:0] Ri2;


	integer i ; 
	
	always #10 clkIn = ~clkIn;
	
	initial begin
		// Initialize Inputs
		clkIn = 0;
		reset = 0;
		MISO_L = 0;
		MISO_R = 0;
	
		L='b0000011001101101;
		R='b0000111111111111;
		
		Li2='b0000010101010101;
		Ri2='b0000000111000111;
		i = 0;
		// Wait 100 ns for global reset to finish
		#10;
		reset = 1;
		
		
		@(negedge CS) begin
				MISO_L = L[15-i];
				MISO_R = R[15-i];
				i = i + 1;
		repeat (15) begin
			@(negedge(SCLK)) begin
				MISO_L = L[15-i];
				MISO_R = R[15-i];
				i = i + 1;
			end
		end
		
		
		
		i = 0;
		repeat (16) begin
			@(negedge(SCLK)) begin
				MISO_L = Li2[15-i];
				MISO_R = Ri2[15-i];
				i = i + 1;
			end
		end
		end
		MISO_L = 0;
		MISO_R = 0;

	end
      
endmodule

