`timescale 1ns / 1ps

module kernelBlock(
		data,
		ldata,
		filterData,
		add_clk,
		write_clk,
		setFilter,
		value
    );
	 parameter WIDTH = 8;
	 parameter MANTISA = 4;
	
	
	input [WIDTH -1:0] data;
	input [WIDTH + MANTISA -1:0] ldata;
	input [WIDTH + MANTISA -1:0] filterData;
	input add_clk;
	input write_clk;
	input setFilter;
	output reg [WIDTH + MANTISA -1:0] value;
	 
	 reg [WIDTH + MANTISA -1 : 0] filter;
	 reg clk;
	 wire [WIDTH + MANTISA -1: 0]calcRes;
	 reg [WIDTH + MANTISA -1: 0] temp;

	 


	multTemp #(.WIDTH(WIDTH), .MANTISSA(MANTISA)) uut (
		.NumbIn(temp),
		.NumbIn2(filter),
		.clk(clk),
		.NumbOut(calcRes)
	);

initial begin
		value = 0;
		filter = 0;
		temp = 0;
		
end

always@(posedge setFilter) begin 
		filter = filterData;
	end
	
	always@(posedge write_clk) begin 
		value = ldata;
	end

always@(posedge add_clk) begin 
	
		temp = data << 4;
		clk = 1;
		#5
		value = calcRes + value;
		clk = 0;
	end
	

endmodule
